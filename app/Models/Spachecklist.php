<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Spachecklist
 *
 * @property string $TaskCode
 * @property string $ManualCode
 * @property string $Description
 * @property int $TimeEstimate
 * @property string|null $ReferToDate
 * @property string|null $Category
 * @property int|null $CategoryID
 * @property string|null $Remark
 * @property int|null $enteredBy
 * @property Carbon|null $dateEntered
 * @property int|null $updatedBy
 * @property string|null $remarks
 * @property Carbon|null $dateUpdated
 *
 * @package App\Models
 */
class Spachecklist extends Model
{
    protected $table = 'spachecklist';
    protected $primaryKey = 'TaskCode';
    public $incrementing = false;
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'TimeEstimate' => 'int',
        'CategoryID' => 'int',
        'enteredBy' => 'int',
        'updatedBy' => 'int'
    ];

    protected $dates = [
        'dateEntered',
        'dateUpdated'
    ];

    protected $fillable = [
        'ManualCode',
        'Description',
        'TimeEstimate',
        'ReferToDate',
        'Category',
        'CategoryID',
        'Remark',
        'enteredBy',
        'dateEntered',
        'updatedBy',
        'remarks',
        'dateUpdated'
    ];

    public function getCodeNameAttribute()
    {
        return $this->TaskCode;
    }
}
