<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\ProgramOwner;
use Illuminate\Http\Request;

class FirmController extends Controller
{
    public function index(Request $req)
    {
        $data = ProgramOwner::orderBy('FirmCode', 'asc')->whereNotNull('FirmCode')->first();
        $formActionURL = route('setup.firm.update', ['id' => $data->FirmCode]);
        $label = 'Firm Profile (HQ)';
        $method = 'edit';
        return view('setup.firm.index', compact('data', 'formActionURL', 'label', 'method'));
    }

    public function store(Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'Name' => 'required',
            'BranchName' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $model = new ProgramOwner;
        $input = $req->all();
        $input['FirmCode'] = ProgramOwner::orderBy('FirmCode', 'desc')->first()->FirmCode + 1;
        $model->fill($input)->save();


        return back()
        ->with('success', 'Firm has been created.')
        ->with('name', $req->name);
    }

    public function update(int $id, Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'Name' => 'required',
            'BranchName' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $model = ProgramOwner::find($id);
        $input = $req->all();
        $input['Note 1'] = $req['Note_1'];
        $model->fill($input)->save();

        return back()
        ->with('success', 'Firm has been updated.');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {
        $model = ProgramOwner::find($id);
        $model->delete();

        return back()
        ->with('success', 'Firm has been deleted.');
    }
}
