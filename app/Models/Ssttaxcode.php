<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Ssttaxcode
 * 
 * @property int $Ranking
 * @property string $TaxCode
 * @property float $TaxRate
 * @property string $Status
 * @property string $Description
 * @property int|null $enteredBy
 * @property Carbon|null $dateEntered
 * @property int|null $updatedBy
 * @property Carbon|null $dateUpdated
 * @property string|null $remarks
 *
 * @package App\Models
 */
class Ssttaxcode extends Model
{
	protected $table = 'ssttaxcode';
	protected $primaryKey = 'TaxCode';
	public $incrementing = false;
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'Ranking' => 'int',
		'TaxRate' => 'float',
		'enteredBy' => 'int',
		'updatedBy' => 'int'
	];

	protected $dates = [
		'dateEntered',
		'dateUpdated'
	];

	protected $fillable = [
		'Ranking',
		'TaxRate',
		'Status',
		'Description',
		'enteredBy',
		'dateEntered',
		'updatedBy',
		'dateUpdated',
		'remarks'
	];
}
