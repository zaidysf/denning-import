<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Cbomattercategory
 * 
 * @property int $ID
 * @property string $Category
 * @property string|null $GroupC1
 * @property string|null $GroupC2
 * @property string|null $GroupC3
 * @property string|null $GroupC4
 * @property string|null $GroupC5
 * @property string|null $GroupC6
 * @property string|null $GroupL1
 * @property string|null $GroupL2
 * @property string|null $GroupL3
 * @property string|null $GroupL4
 * @property string|null $GroupB1
 * @property string|null $GroupB2
 * @property Carbon|null $dateEntered
 * @property int|null $enteredBy
 * @property Carbon|null $dateUpdated
 * @property int|null $updatedBy
 * @property string|null $remarks
 *
 * @package App\Models
 */
class Cbomattercategory extends Model
{
	protected $table = 'cbomattercategory';
	protected $primaryKey = 'ID';
	public $incrementing = false;
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'ID' => 'int',
		'enteredBy' => 'int',
		'updatedBy' => 'int'
	];

	protected $dates = [
		'dateEntered',
		'dateUpdated'
	];

	protected $fillable = [
		'Category',
		'GroupC1',
		'GroupC2',
		'GroupC3',
		'GroupC4',
		'GroupC5',
		'GroupC6',
		'GroupL1',
		'GroupL2',
		'GroupL3',
		'GroupL4',
		'GroupB1',
		'GroupB2',
		'dateEntered',
		'enteredBy',
		'dateUpdated',
		'updatedBy',
		'remarks'
	];
}
