<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * Class Filestatus
 *
 * @property int $statusCode
 * @property string|null $status
 * @property Carbon|null $dateEntered
 * @property int|null $enteredBy
 * @property Carbon|null $dateUpdated
 * @property int|null $updatedBy
 * @property string|null $remarks
 *
 * @package App\Models
 */
class Filestatus extends Model
{


    protected $table = 'filestatus';
    protected $primaryKey = 'statusCode';
    public $incrementing = false;
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'statusCode' => 'int',
        'enteredBy' => 'int',
        'updatedBy' => 'int'
    ];

    protected $dates = [
        'dateEntered',
        'dateUpdated'
    ];

    protected $fillable = [
        'status',
        'dateEntered',
        'enteredBy',
        'dateUpdated',
        'updatedBy',
        'remarks'
    ];
}
