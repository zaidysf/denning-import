<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\File;
use App\Models\MatterCode;
use App\Models\Project;
use Illuminate\Http\Request;

class FileController extends Controller
{
    public function index(Request $req)
    {
        $page = 10;
        if ($req->has('per_page')) {
            $page = $req->get('per_page');
        }
        if ($req->has('key')) {
            switch ($req->get('key')) {
                case 'FileNo2':
                    $data = File::where('FileNo2', 'LIKE', '%'.$req->get('value').'%')->whereNotNull('FileStatus')->orderBy('FileNo1', 'desc')->paginate($page);
                    break;

                case 'client':
                    $client = Customer::where('name', 'LIKE', '%'.$req->get('value').'%')->get()->pluck('customerCode');
                    $data = File::whereIn('C0', $client)->whereNotNull('FileStatus')->orderBy('FileNo1', 'desc')->paginate($page);
                    break;

                case 'matter':
                    $matter = MatterCode::where('Matter', 'LIKE', '%'.$req->get('value').'%')->get()->pluck('MatterCode');
                    $data = File::whereIn('MatterCode', $matter)->whereNotNull('FileStatus')->orderBy('FileNo1', 'desc')->paginate($page);
                    break;

                case 'BatchNo':
                    $data = File::where('BatchNo', 'LIKE', '%'.$req->get('value').'%')->whereNotNull('FileStatus')->orderBy('FileNo1', 'desc')->paginate($page);
                    break;

                case 'MatterCode':
                    $data = File::where('MatterCode', 'LIKE', '%'.$req->get('value').'%')->whereNotNull('FileStatus')->orderBy('FileNo1', 'desc')->paginate($page);
                    break;

                case 'Project':
                    $project = Project::where('projectCode', 'LIKE', '%'.$req->get('value').'%')->orWhere('projectnode', 'LIKE', '%'.$req->get('value').'%')->get()->pluck('projectCode');
                    $data = File::whereIn('P1', $project)->whereNotNull('FileStatus')->orderBy('FileNo1', 'desc')->paginate($page);
                    break;

                default:
                    $data = File::where('FileNo1', 'LIKE', '%'.$req->get('value').'%')->whereNotNull('FileStatus')->orderBy('FileNo1', 'desc')->paginate($page);
                    break;
            }
        } else {
            $data = File::whereNotNull('FileStatus')->orderBy('FileNo1', 'desc')->paginate($page);
        }
        $selectColumn = [
            'client' => 'Client',
            'FileNo1' => 'FileNo1',
            'FileNo2' => 'FileNo2',
            'BatchNo' => 'Batch No',
            'MatterCode' => 'Matter Code',
            'matter' => 'Matter Type',
            'Project' => 'Project Code/Name',
        ];
        $selectPagination = [10 => 10, 25 => 25, 50 => 50, 100 => 100];
        return view('file.index', compact('data', 'selectColumn', 'selectPagination'));
    }
}
