<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\MailMerge;
use Illuminate\Http\Request;

class MailMergeController extends Controller
{
    public function __construct()
    {
        //
    }

    public function find($id)
    {
        $data = MailMerge::with('createdBy', 'updatedBy')->find($id);
        return ['data' => $data];
    }

    public function search(Request $request)
    {
        // $mailMerge = MailMerge::selectRaw("CONCAT_WS(' - ', template_id, mail_merges.name, category.name) AS text, id")
        // ->leftJoin('category', 'mail_merges.category', '=', 'Cbomattercategory.ID')
        // ->with(['cat:Category'])
        // ->where('template_id', 'LIKE', '%'.$request->input('term', '').'%')
        // ->orWhere('mail_merges.name', 'LIKE', '%'.$request->input('term', '').'%')
        // ->orWhereHas('cat', function ($query) use ($request) {
        //     $query->where('name', 'like', '%' .$request->input('term', '') . '%');
        // })

        $mailMerge = MailMerge::selectRaw("CONCAT_WS(' - ', template_id, mail_merges.name, category.name) AS text, id")
        ->leftJoin('category', 'mail_merges.category', '=', 'Cbomattercategory.ID')
        ->where('template_id', 'LIKE', '%'.$request->input('term', '').'%')
        ->orWhere('mail_merges.name', 'LIKE', '%'.$request->input('term', '').'%')
        ->get();
        // ->simplePaginate(10);
        // dd($mailMerge->toJson());
        $mailMerge->appends(['term' => $request->input('term', '')]);
        return ['results' => $mailMerge];
    }
}
