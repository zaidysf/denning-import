<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * Class Idtype
 *
 * @property int $typeCode
 * @property string|null $label
 * @property string|null $typeE
 * @property string|null $typeM
 * @property string|null $oldTypeE
 * @property string|null $oldTypeM
 * @property string|null $Citizenship(E)
 * @property string|null $Citizenship(BM)
 * @property string|null $Add(E)
 * @property string|null $Add(BM)
 * @property string|null $IncorpType(E)
 * @property string|null $IncorpType(BM)
 * @property string|null $openbracket
 * @property string|null $closebracket
 * @property string|null $coma
 * @property string|null $hyphen
 * @property string|null $openbracket2
 * @property string|null $coma2
 * @property string|null $closebracket2
 * @property string|null $comaAdd
 * @property Carbon|null $dateEntered
 * @property int|null $enteredBy
 * @property Carbon|null $dateUpdated
 * @property int|null $updatedBy
 * @property string|null $remarks
 *
 * @package App\Models
 */
class Idtype extends Model
{


    protected $table = 'idtype';
    protected $primaryKey = 'typeCode';
    public $incrementing = false;
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'typeCode' => 'int',
        'enteredBy' => 'int',
        'updatedBy' => 'int'
    ];

    protected $dates = [
        'dateEntered',
        'dateUpdated'
    ];

    protected $fillable = [
        'label',
        'typeE',
        'typeM',
        'oldTypeE',
        'oldTypeM',
        'Citizenship(E)',
        'Citizenship(BM)',
        'Add(E)',
        'Add(BM)',
        'IncorpType(E)',
        'IncorpType(BM)',
        'openbracket',
        'closebracket',
        'coma',
        'hyphen',
        'openbracket2',
        'coma2',
        'closebracket2',
        'comaAdd',
        'dateEntered',
        'enteredBy',
        'dateUpdated',
        'updatedBy',
        'remarks'
    ];
}
