<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Project;
// use App\Models\Property;
use Illuminate\Http\Request;

class PropertyController extends Controller
{
    public function __construct()
    {
        //
    }

    public function search(Request $request)
    {
        // $property = Property::selectRaw("PropertyCode as id, CONCAT_WS(' - ', PropertyCode, ProjectCode, ProjectName) AS text")
        //     ->where('ProjectCode', 'LIKE', '%'.$request->input('term', '').'%')
        //     ->orWhere('ProjectName', 'LIKE', '%'.$request->input('term', '').'%')
        //     ->simplePaginate(10);
        $property = Project::selectRaw("projectCode as id, projectName AS text")
            ->orWhere('projectName', 'LIKE', '%'.$request->input('term', '').'%')
            ->simplePaginate(10);
        $property->appends(['term' => $request->input('term', '')]);
        return ['results' => $property];
    }
}
