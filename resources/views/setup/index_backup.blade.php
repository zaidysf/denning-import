@extends('layout')
@section('content')
<div class="mb-3">
    {{-- <h1 class="h3 d-inline align-middle">Setup</h1> --}}
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="container-fluid">
                    <div class="row">
                        {{-- <div class="col-12 col-lg-6 d-none d-lg-flex col-img">
                        </div> --}}
                        <div class="col-12 col-lg-12 align-self-center">
                            <div class="m-3 m-lg-5">

                                <div class="text-center mt-4 mb-3">
                                    <h1 class="h3">Denning Initial Setup</h1>
                                    <p class="lead">
                                        Navigate between steps in this setup wizard.
                                    </p>
                                </div>

                                <ul id="steps-native" class="nav nav-pills justify-content-center"></ul>

                                <form id="wizard" class="my-2 py-2">
                                    <section data-step="Account">
                                        <div class="mb-3">
                                            <label class="form-label" for="email">Email</label>
                                            <input class="form-control" type="email" name="email" placeholder="Email.."
                                                required>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label" for="password">Password</label>
                                            <input class="form-control" type="password" name="password"
                                                placeholder="Password.." required>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 text-right">
                                                <button class="btn btn-primary" data-next>Next</button>
                                            </div>
                                        </div>
                                    </section>

                                    <section data-step="Firm Profile">
                                        <div class="mb-3">
                                            <label class="form-label" for="first-name"></label>
                                            <input class="form-control" type="text" name="first-name"
                                                placeholder="First name.." required>
                                        </div>

                                        <div class="mb-3">
                                            <label class="form-label" for="last-name">Last name</label>
                                            <input class="form-control" type="text" name="last-name"
                                                placeholder="Last name.." required>
                                        </div>

                                        <div class="row">
                                            <div class="col-6 text-left">
                                                <button class="btn btn-outline-primary" data-prev>Previous</button>
                                            </div>
                                            <div class="col-6 text-right">
                                                <button class="btn btn-primary" data-next>Next</button>
                                            </div>
                                        </div>
                                    </section>

                                    <section data-step="Staff or Users">
                                        <div class="mb-3">
                                            <label class="form-label" for="first-name"></label>
                                            <input class="form-control" type="text" name="first-name"
                                                placeholder="First name.." required>
                                        </div>

                                        <div class="mb-3">
                                            <label class="form-label" for="last-name">Last name</label>
                                            <input class="form-control" type="text" name="last-name"
                                                placeholder="Last name.." required>
                                        </div>

                                        <div class="row">
                                            <div class="col-6 text-left">
                                                <button class="btn btn-outline-primary" data-prev>Previous</button>
                                            </div>
                                            <div class="col-6 text-right">
                                                <button class="btn btn-primary" data-next>Next</button>
                                            </div>
                                        </div>
                                    </section>

                                    <section data-step="Company Letterhead">
                                        <div class="mb-3">
                                            <label class="form-label" for="first-name"></label>
                                            <input class="form-control" type="text" name="first-name"
                                                placeholder="First name.." required>
                                        </div>

                                        <div class="mb-3">
                                            <label class="form-label" for="last-name">Last name</label>
                                            <input class="form-control" type="text" name="last-name"
                                                placeholder="Last name.." required>
                                        </div>

                                        <div class="row">
                                            <div class="col-6 text-left">
                                                <button class="btn btn-outline-primary" data-prev>Previous</button>
                                            </div>
                                            <div class="col-6 text-right">
                                                <button class="btn btn-primary" data-next>Next</button>
                                            </div>
                                        </div>
                                    </section>

                                    <section data-step="Currency Settings">
                                        <div class="mb-3">
                                            <label class="form-label" for="first-name"></label>
                                            <input class="form-control" type="text" name="first-name"
                                                placeholder="First name.." required>
                                        </div>

                                        <div class="mb-3">
                                            <label class="form-label" for="last-name">Last name</label>
                                            <input class="form-control" type="text" name="last-name"
                                                placeholder="Last name.." required>
                                        </div>

                                        <div class="row">
                                            <div class="col-6 text-left">
                                                <button class="btn btn-outline-primary" data-prev>Previous</button>
                                            </div>
                                            <div class="col-6 text-right">
                                                <button class="btn btn-primary" data-next>Next</button>
                                            </div>
                                        </div>
                                    </section>

                                    <section data-step="Bank Accounts">
                                        <div class="mb-3">
                                            <label class="form-label" for="first-name"></label>
                                            <input class="form-control" type="text" name="first-name"
                                                placeholder="First name.." required>
                                        </div>

                                        <div class="mb-3">
                                            <label class="form-label" for="last-name">Last name</label>
                                            <input class="form-control" type="text" name="last-name"
                                                placeholder="Last name.." required>
                                        </div>

                                        <div class="row">
                                            <div class="col-6 text-left">
                                                <button class="btn btn-outline-primary" data-prev>Previous</button>
                                            </div>
                                            <div class="col-6 text-right">
                                                <button class="btn btn-primary" data-next>Next</button>
                                            </div>
                                        </div>
                                    </section>

                                    <section data-step="Opening Balances">
                                        <div class="mb-3">
                                            <label class="form-label" for="first-name"></label>
                                            <input class="form-control" type="text" name="first-name"
                                                placeholder="First name.." required>
                                        </div>

                                        <div class="mb-3">
                                            <label class="form-label" for="last-name">Last name</label>
                                            <input class="form-control" type="text" name="last-name"
                                                placeholder="Last name.." required>
                                        </div>

                                        <div class="row">
                                            <div class="col-6 text-left">
                                                <button class="btn btn-outline-primary" data-prev>Previous</button>
                                            </div>
                                            <div class="col-6 text-right">
                                                <button class="btn btn-primary" data-next>Next</button>
                                            </div>
                                        </div>
                                    </section>

                                    <section data-step="Privileges and Restrictions">
                                        <div class="mb-3">
                                            <label class="form-label" for="first-name"></label>
                                            <input class="form-control" type="text" name="first-name"
                                                placeholder="First name.." required>
                                        </div>

                                        <div class="mb-3">
                                            <label class="form-label" for="last-name">Last name</label>
                                            <input class="form-control" type="text" name="last-name"
                                                placeholder="Last name.." required>
                                        </div>

                                        <div class="row">
                                            <div class="col-6 text-left">
                                                <button class="btn btn-outline-primary" data-prev>Previous</button>
                                            </div>
                                            <div class="col-6 text-right">
                                                <button class="btn btn-primary" data-next>Next</button>
                                            </div>
                                        </div>
                                    </section>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('style')
<style>
.zandgar__wizard {
    display: block;
    height: 100%;
    position: relative;
}

.zandgar__wizard .zandgar__step {
    display: none;
}
.zandgar__wizard .zandgar__step.zandgar__step__active {
    display: block;
}

.col-img {
    /* https://unsplash.com/photos/yWwob8kwOCk */
    background: scroll center url('https://images.unsplash.com/photo-1497366754035-f200968a6e72?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80');
    background-size: cover;
    min-height: 100vh;
}
</style>
@endsection
@section('script')
<script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
<script src="{{ asset('js/zangdar.min.js') }}"></script>
<script>
    function buildStepsBreadcrumb (wizard, element, steps) {
    const $steps = document.getElementById(element)
        $steps.innerHTML = ''
        for (let label in steps) {
            if (steps.hasOwnProperty(label)) {
                const $li = document.createElement('li')
                const $a = document.createElement('a')
                $li.classList.add('nav-item')
                $a.classList.add('nav-link')
                if (steps[label].active) {
                    $a.classList.add('active')
                }
                $a.setAttribute('href', '#')
                $a.innerText = label
                $a.addEventListener('click', e => {
                    e.preventDefault()
                    wizard.revealStep(label)
                })
                $li.appendChild($a)
                $steps.appendChild($li)
            }
        }
    }

    function onStepChange(wizard, selector) {
        const steps = wizard.getBreadcrumb()
        buildStepsBreadcrumb(wizard, selector, steps)
    }

    const wizard = new window.Zangdar('#wizard', {
    onStepChange: () => {
        onStepChange(wizard, 'steps-native')
    },
    onSubmit(e) {
        e.preventDefault()
        console.log(e.target.elements)
        return false
    }
    })

    onStepChange(wizard, 'steps-native')
</script>
@endsection
