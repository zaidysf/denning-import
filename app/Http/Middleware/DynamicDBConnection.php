<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class DynamicDBConnection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // abort_if(!$request->session()->has('denningonline') ||
        //     !$request->session()->has('DBServer') ||
        //     !$request->session()->has('DBPort'), 404);
        if (Auth::user()->email == 'omargill@test.com' || Auth::user()->email == 'megat@test.com') {
            if (Auth::user()->email == 'omargill@test.com') {
                $allowed = [
                    'dashboard',
                    'export.index',
                    'export.post',
                    'logout'
                ];
                if (!in_array($request->route()->getName(), $allowed)) {
                    abort(401);
                }
            }
            return $next($request);
        }

        DB::disconnect('mysql');
        Config::set("database.connections.mysql.host", Auth::user()->db_server);
        Config::set("database.connections.mysql.port", Auth::user()->db_port);
        Config::set("database.connections.mysql.database", 'des');
        DB::purge('mysql');
        DB::reconnect('mysql');

        Config::set("filesystems.disks.ftp_user.host", Auth::user()->ftp_server);
        Config::set("filesystems.disks.ftp_user.port", Auth::user()->ftp_port);
        Config::set("filesystems.disks.ftp_user.root", Auth::user()->ftp_dir);

        return $next($request);
    }
}
