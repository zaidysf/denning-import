<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * Class File
 *
 * @property string $FileNo1
 * @property string|null $FileNo2
 * @property Carbon|null $DateOpenFile
 * @property int|null $PartnerInCharge
 * @property int|null $LAInCharge
 * @property int|null $ClerkInCharge
 * @property string|null $MatterCode
 * @property int|null $FileStatus
 * @property string|null $FileRef
 * @property string|null $RelatedFile
 * @property string|null $PocketFile
 * @property string|null $FileLoc
 * @property string|null $Box
 * @property string|null $Note1
 * @property string|null $Note2
 * @property string|null $Note3
 * @property int|null $enteredBy
 * @property Carbon|null $dateEntered
 * @property int|null $updatedBy
 * @property string|null $remarks
 * @property Carbon|null $dateUpdated
 * @property int|null $C0
 * @property int|null $C1
 * @property int|null $C2
 * @property int|null $C3
 * @property int|null $C4
 * @property int|null $C5
 * @property int|null $C6
 * @property int|null $C7
 * @property int|null $C8
 * @property int|null $C9
 * @property int|null $C10
 * @property int|null $C11
 * @property int|null $C12
 * @property int|null $C13
 * @property int|null $C14
 * @property int|null $C15
 * @property int|null $C16
 * @property int|null $C17
 * @property int|null $C18
 * @property int|null $C19
 * @property int|null $C20
 * @property int|null $C21
 * @property int|null $C22
 * @property int|null $C23
 * @property int|null $C24
 * @property int|null $C25
 * @property int|null $C26
 * @property int|null $C27
 * @property int|null $C28
 * @property int|null $C29
 * @property int|null $C30
 * @property int|null $EAC1
 * @property int|null $EAC2
 * @property float|null $EAFee1
 * @property Carbon|null $EADatePaid1
 * @property string|null $EAChqNo1
 * @property float|null $EAFee2
 * @property Carbon|null $EADatePaid2
 * @property string|null $EAChqNo2
 * @property float|null $EAFee3
 * @property Carbon|null $EADatePaid3
 * @property string|null $EAChqNo3
 * @property string|null $EANote
 * @property int|null $P1
 * @property int|null $P2
 * @property int|null $P3
 * @property int|null $P4
 * @property int|null $P5
 * @property int|null $Bank1
 * @property string|null $Bank1Ref
 * @property string|null $Bank1PANo
 * @property int|null $Bank2
 * @property string|null $Bank2Ref
 * @property string|null $Bank2PANo
 * @property int|null $Lawyer1
 * @property int|null $Lawyer2
 * @property int|null $Lawyer3
 * @property int|null $Lawyer4
 * @property string|null $Lawyer1Ref
 * @property string|null $Lawyer2Ref
 * @property string|null $Lawyer3Ref
 * @property string|null $Lawyer4Ref
 * @property float|null $$1
 * @property float|null $$2
 * @property float|null $$3
 * @property float|null $$4
 * @property float|null $$5
 * @property float|null $$6
 * @property float|null $$7
 * @property float|null $$8
 * @property float|null $$9
 * @property float|null $$10
 * @property float|null $$11
 * @property float|null $$12
 * @property float|null $$13
 * @property float|null $$14
 * @property float|null $$15
 * @property float|null $$16
 * @property float|null $$17
 * @property float|null $$18
 * @property float|null $$19
 * @property float|null $$20
 * @property float|null $$21
 * @property float|null $$22
 * @property float|null $$23
 * @property float|null $$24
 * @property float|null $$25
 * @property float|null $$26
 * @property float|null $$27
 * @property float|null $$28
 * @property float|null $$29
 * @property float|null $$30
 * @property float|null $$31
 * @property float|null $$32
 * @property float|null $$33
 * @property float|null $$34
 * @property float|null $$35
 * @property float|null $$36
 * @property float|null $$37
 * @property float|null $$38
 * @property float|null $$39
 * @property float|null $$40
 * @property float|null $$41
 * @property float|null $$42
 * @property float|null $$43
 * @property float|null $$44
 * @property float|null $$45
 * @property Carbon|null $Date1
 * @property Carbon|null $Date2
 * @property Carbon|null $Date3
 * @property Carbon|null $Date4
 * @property Carbon|null $Date5
 * @property Carbon|null $Date6
 * @property Carbon|null $Date7
 * @property Carbon|null $Date8
 * @property Carbon|null $Date9
 * @property Carbon|null $Date10
 * @property Carbon|null $Date11
 * @property Carbon|null $Date12
 * @property Carbon|null $Date13
 * @property Carbon|null $Date14
 * @property Carbon|null $Date15
 * @property Carbon|null $Date16
 * @property Carbon|null $Date17
 * @property Carbon|null $Date18
 * @property Carbon|null $Date19
 * @property Carbon|null $Date20
 * @property Carbon|null $Date21
 * @property Carbon|null $Date22
 * @property Carbon|null $Date23
 * @property Carbon|null $Date24
 * @property Carbon|null $Date25
 * @property Carbon|null $Date26
 * @property Carbon|null $Date27
 * @property Carbon|null $Date28
 * @property Carbon|null $Date29
 * @property Carbon|null $Date30
 * @property Carbon|null $Date31
 * @property Carbon|null $Date32
 * @property Carbon|null $Date33
 * @property Carbon|null $Date34
 * @property Carbon|null $Date35
 * @property Carbon|null $Date36
 * @property Carbon|null $Date37
 * @property Carbon|null $Date38
 * @property Carbon|null $Date39
 * @property Carbon|null $Date40
 * @property Carbon|null $Date41
 * @property Carbon|null $Date42
 * @property Carbon|null $Date43
 * @property Carbon|null $Date44
 * @property Carbon|null $Date45
 * @property Carbon|null $Date46
 * @property Carbon|null $Date47
 * @property Carbon|null $Date48
 * @property Carbon|null $Date49
 * @property Carbon|null $Date50
 * @property string|null $Pn1
 * @property string|null $Pn2
 * @property string|null $Pn3
 * @property string|null $Pn4
 * @property string|null $Pn5
 * @property string|null $Pn6
 * @property string|null $Pn7
 * @property string|null $Pn8
 * @property string|null $Pn9
 * @property string|null $Pn10
 * @property string|null $Pn11
 * @property string|null $Pn12
 * @property string|null $Pn13
 * @property string|null $Pn14
 * @property string|null $Pn15
 * @property string|null $PartyNo1
 * @property string|null $PartyNo2
 * @property string|null $PartyNo3
 * @property string|null $14AOption
 * @property string|null $16AOption
 * @property string|null $Relationship
 * @property string|null $F1
 * @property string|null $F2
 * @property string|null $F3
 * @property string|null $F4
 * @property string|null $F5
 * @property string|null $F6
 * @property string|null $F7
 * @property string|null $F8
 * @property string|null $F9
 * @property string|null $F10
 * @property string|null $F11
 * @property string|null $F12
 * @property string|null $F13
 * @property string|null $F14
 * @property string|null $F15
 * @property string|null $F16
 * @property string|null $F17
 * @property string|null $F18
 * @property string|null $F19
 * @property string|null $F20
 * @property string|null $F21
 * @property string|null $F22
 * @property string|null $F23
 * @property string|null $F24
 * @property string|null $F25
 * @property string|null $F26
 * @property string|null $F27
 * @property string|null $F28
 * @property string|null $F29
 * @property string|null $F30
 * @property string|null $F31
 * @property string|null $F32
 * @property string|null $F33
 * @property string|null $F34
 * @property string|null $F35
 * @property string|null $F36
 * @property string|null $F37
 * @property string|null $F38
 * @property string|null $F39
 * @property string|null $F40
 * @property string|null $F41
 * @property bool|null $BankruptCheck
 * @property string|null $Licensee
 * @property string|null $Share1
 * @property string|null $Share2
 * @property string|null $Share3
 * @property string|null $Share4
 * @property string|null $Share5
 * @property string|null $Portion1
 * @property bool|null $chkVendor
 * @property bool|null $chkPurchaser
 * @property bool|null $chkBank
 * @property bool|null $chkStakeholder
 * @property bool|null $chkParty
 * @property bool|null $chkSPA
 * @property bool|null $chkCPFulfilled
 * @property int|null $radCD
 * @property int|null $radECD
 * @property int|null $CDDays
 * @property int|null $ECDDays
 * @property bool $ChkCompleted
 * @property bool|null $Checklist1
 * @property bool|null $Checklist2
 * @property bool|null $Checklist3
 * @property bool|null $Checklist4
 * @property bool|null $Checklist5
 * @property bool|null $Checklist6
 * @property bool|null $Checklist7
 * @property bool|null $Checklist8
 * @property int|null $Turnaround
 * @property Carbon|null $DateCloseFile
 * @property bool|null $NonBillable
 * @property string|null $Attest1
 * @property string|null $Attest2
 * @property string|null $Attest3
 * @property int|null $landoffice
 * @property bool|null $IsBank
 * @property int|null $LitigationType
 * @property string|null $PChecklist
 * @property string|null $PBilling
 * @property string|null $PostingMode
 * @property bool|null $ChkScheduled
 * @property bool|null $IsChanged
 * @property string|null $ShareC1
 * @property string|null $ShareC2
 * @property string|null $ShareC3
 * @property string|null $ShareC4
 * @property string|null $ShareC5
 * @property string|null $ShareC6
 * @property string|null $ShareC7
 * @property string|null $ShareC8
 * @property string|null $ShareC9
 * @property string|null $ShareC10
 * @property int|null $radPriceGst
 * @property float|null $PriceGst
 * @property string|null $AllowStaffList
 * @property string|null $FixPwd
 * @property string|null $TempPwd
 * @property Carbon|null $TempPwdExpiry
 * @property int $AccessRestriction
 * @property string|null $Team
 * @property string|null $CustAllowStaffList
 * @property string $PurchasePriceSymbol
 * @property string $LoanPriceSymbol
 * @property string|null $CurrencySymbols
 *
 * @package App\Models
 */
class File extends Model
{


    protected $table = 'file';
    protected $primaryKey = 'FileNo1';
    public $incrementing = false;
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'PartnerInCharge' => 'int',
        'LAInCharge' => 'int',
        'ClerkInCharge' => 'int',
        'FileStatus' => 'int',
        'enteredBy' => 'int',
        'updatedBy' => 'int',
        'C0' => 'int',
        'C1' => 'int',
        'C2' => 'int',
        'C3' => 'int',
        'C4' => 'int',
        'C5' => 'int',
        'C6' => 'int',
        'C7' => 'int',
        'C8' => 'int',
        'C9' => 'int',
        'C10' => 'int',
        'C11' => 'int',
        'C12' => 'int',
        'C13' => 'int',
        'C14' => 'int',
        'C15' => 'int',
        'C16' => 'int',
        'C17' => 'int',
        'C18' => 'int',
        'C19' => 'int',
        'C20' => 'int',
        'C21' => 'int',
        'C22' => 'int',
        'C23' => 'int',
        'C24' => 'int',
        'C25' => 'int',
        'C26' => 'int',
        'C27' => 'int',
        'C28' => 'int',
        'C29' => 'int',
        'C30' => 'int',
        'EAC1' => 'int',
        'EAC2' => 'int',
        'EAFee1' => 'float',
        'EAFee2' => 'float',
        'EAFee3' => 'float',
        'P1' => 'int',
        'P2' => 'int',
        'P3' => 'int',
        'P4' => 'int',
        'P5' => 'int',
        'Bank1' => 'int',
        'Bank2' => 'int',
        'Lawyer1' => 'int',
        'Lawyer2' => 'int',
        'Lawyer3' => 'int',
        'Lawyer4' => 'int',
        '$1' => 'float',
        '$2' => 'float',
        '$3' => 'float',
        '$4' => 'float',
        '$5' => 'float',
        '$6' => 'float',
        '$7' => 'float',
        '$8' => 'float',
        '$9' => 'float',
        '$10' => 'float',
        '$11' => 'float',
        '$12' => 'float',
        '$13' => 'float',
        '$14' => 'float',
        '$15' => 'float',
        '$16' => 'float',
        '$17' => 'float',
        '$18' => 'float',
        '$19' => 'float',
        '$20' => 'float',
        '$21' => 'float',
        '$22' => 'float',
        '$23' => 'float',
        '$24' => 'float',
        '$25' => 'float',
        '$26' => 'float',
        '$27' => 'float',
        '$28' => 'float',
        '$29' => 'float',
        '$30' => 'float',
        '$31' => 'float',
        '$32' => 'float',
        '$33' => 'float',
        '$34' => 'float',
        '$35' => 'float',
        '$36' => 'float',
        '$37' => 'float',
        '$38' => 'float',
        '$39' => 'float',
        '$40' => 'float',
        '$41' => 'float',
        '$42' => 'float',
        '$43' => 'float',
        '$44' => 'float',
        '$45' => 'float',
        'BankruptCheck' => 'bool',
        'chkVendor' => 'bool',
        'chkPurchaser' => 'bool',
        'chkBank' => 'bool',
        'chkStakeholder' => 'bool',
        'chkParty' => 'bool',
        'chkSPA' => 'bool',
        'chkCPFulfilled' => 'bool',
        'radCD' => 'int',
        'radECD' => 'int',
        'CDDays' => 'int',
        'ECDDays' => 'int',
        'ChkCompleted' => 'bool',
        'Checklist1' => 'bool',
        'Checklist2' => 'bool',
        'Checklist3' => 'bool',
        'Checklist4' => 'bool',
        'Checklist5' => 'bool',
        'Checklist6' => 'bool',
        'Checklist7' => 'bool',
        'Checklist8' => 'bool',
        'Turnaround' => 'int',
        'NonBillable' => 'bool',
        'landoffice' => 'int',
        'IsBank' => 'bool',
        'LitigationType' => 'int',
        'ChkScheduled' => 'bool',
        'IsChanged' => 'bool',
        'radPriceGst' => 'int',
        'PriceGst' => 'float',
        'AccessRestriction' => 'int'
    ];

    protected $dates = [
        'DateOpenFile',
        'dateEntered',
        'dateUpdated',
        'EADatePaid1',
        'EADatePaid2',
        'EADatePaid3',
        'Date1',
        'Date2',
        'Date3',
        'Date4',
        'Date5',
        'Date6',
        'Date7',
        'Date8',
        'Date9',
        'Date10',
        'Date11',
        'Date12',
        'Date13',
        'Date14',
        'Date15',
        'Date16',
        'Date17',
        'Date18',
        'Date19',
        'Date20',
        'Date21',
        'Date22',
        'Date23',
        'Date24',
        'Date25',
        'Date26',
        'Date27',
        'Date28',
        'Date29',
        'Date30',
        'Date31',
        'Date32',
        'Date33',
        'Date34',
        'Date35',
        'Date36',
        'Date37',
        'Date38',
        'Date39',
        'Date40',
        'Date41',
        'Date42',
        'Date43',
        'Date44',
        'Date45',
        'Date46',
        'Date47',
        'Date48',
        'Date49',
        'Date50',
        'DateCloseFile',
        'TempPwdExpiry'
    ];

    protected $fillable = [
        'FileNo1',
        'FileNo2',
        'DateOpenFile',
        'PartnerInCharge',
        'LAInCharge',
        'ClerkInCharge',
        'MatterCode',
        'BatchNo',
        'FileStatus',
        'FileRef',
        'RelatedFile',
        'PocketFile',
        'FileLoc',
        'Box',
        'Note1',
        'Note2',
        'Note3',
        'enteredBy',
        'dateEntered',
        'updatedBy',
        'remarks',
        'dateUpdated',
        'C0',
        'C1',
        'C2',
        'C3',
        'C4',
        'C5',
        'C6',
        'C7',
        'C8',
        'C9',
        'C10',
        'C11',
        'C12',
        'C13',
        'C14',
        'C15',
        'C16',
        'C17',
        'C18',
        'C19',
        'C20',
        'C21',
        'C22',
        'C23',
        'C24',
        'C25',
        'C26',
        'C27',
        'C28',
        'C29',
        'C30',
        'EAC1',
        'EAC2',
        'EAFee1',
        'EADatePaid1',
        'EAChqNo1',
        'EAFee2',
        'EADatePaid2',
        'EAChqNo2',
        'EAFee3',
        'EADatePaid3',
        'EAChqNo3',
        'EANote',
        'P1',
        'P2',
        'P3',
        'P4',
        'P5',
        'Bank1',
        'Bank1Ref',
        'Bank1PANo',
        'Bank2',
        'Bank2Ref',
        'Bank2PANo',
        'Lawyer1',
        'Lawyer2',
        'Lawyer3',
        'Lawyer4',
        'Lawyer1Ref',
        'Lawyer2Ref',
        'Lawyer3Ref',
        'Lawyer4Ref',
        '$1',
        '$2',
        '$3',
        '$4',
        '$5',
        '$6',
        '$7',
        '$8',
        '$9',
        '$10',
        '$11',
        '$12',
        '$13',
        '$14',
        '$15',
        '$16',
        '$17',
        '$18',
        '$19',
        '$20',
        '$21',
        '$22',
        '$23',
        '$24',
        '$25',
        '$26',
        '$27',
        '$28',
        '$29',
        '$30',
        '$31',
        '$32',
        '$33',
        '$34',
        '$35',
        '$36',
        '$37',
        '$38',
        '$39',
        '$40',
        '$41',
        '$42',
        '$43',
        '$44',
        '$45',
        'Date1',
        'Date2',
        'Date3',
        'Date4',
        'Date5',
        'Date6',
        'Date7',
        'Date8',
        'Date9',
        'Date10',
        'Date11',
        'Date12',
        'Date13',
        'Date14',
        'Date15',
        'Date16',
        'Date17',
        'Date18',
        'Date19',
        'Date20',
        'Date21',
        'Date22',
        'Date23',
        'Date24',
        'Date25',
        'Date26',
        'Date27',
        'Date28',
        'Date29',
        'Date30',
        'Date31',
        'Date32',
        'Date33',
        'Date34',
        'Date35',
        'Date36',
        'Date37',
        'Date38',
        'Date39',
        'Date40',
        'Date41',
        'Date42',
        'Date43',
        'Date44',
        'Date45',
        'Date46',
        'Date47',
        'Date48',
        'Date49',
        'Date50',
        'Pn1',
        'Pn2',
        'Pn3',
        'Pn4',
        'Pn5',
        'Pn6',
        'Pn7',
        'Pn8',
        'Pn9',
        'Pn10',
        'Pn11',
        'Pn12',
        'Pn13',
        'Pn14',
        'Pn15',
        'PartyNo1',
        'PartyNo2',
        'PartyNo3',
        '14AOption',
        '16AOption',
        'Relationship',
        'F1',
        'F2',
        'F3',
        'F4',
        'F5',
        'F6',
        'F7',
        'F8',
        'F9',
        'F10',
        'F11',
        'F12',
        'F13',
        'F14',
        'F15',
        'F16',
        'F17',
        'F18',
        'F19',
        'F20',
        'F21',
        'F22',
        'F23',
        'F24',
        'F25',
        'F26',
        'F27',
        'F28',
        'F29',
        'F30',
        'F31',
        'F32',
        'F33',
        'F34',
        'F35',
        'F36',
        'F37',
        'F38',
        'F39',
        'F40',
        'F41',
        'BankruptCheck',
        'Licensee',
        'Share1',
        'Share2',
        'Share3',
        'Share4',
        'Share5',
        'Portion1',
        'chkVendor',
        'chkPurchaser',
        'chkBank',
        'chkStakeholder',
        'chkParty',
        'chkSPA',
        'chkCPFulfilled',
        'radCD',
        'radECD',
        'CDDays',
        'ECDDays',
        'ChkCompleted',
        'Checklist1',
        'Checklist2',
        'Checklist3',
        'Checklist4',
        'Checklist5',
        'Checklist6',
        'Checklist7',
        'Checklist8',
        'Turnaround',
        'DateCloseFile',
        'NonBillable',
        'Attest1',
        'Attest2',
        'Attest3',
        'landoffice',
        'IsBank',
        'LitigationType',
        'PChecklist',
        'PBilling',
        'PostingMode',
        'ChkScheduled',
        'IsChanged',
        'ShareC1',
        'ShareC2',
        'ShareC3',
        'ShareC4',
        'ShareC5',
        'ShareC6',
        'ShareC7',
        'ShareC8',
        'ShareC9',
        'ShareC10',
        'radPriceGst',
        'PriceGst',
        'AllowStaffList',
        'FixPwd',
        'TempPwd',
        'TempPwdExpiry',
        'AccessRestriction',
        'Team',
        'CustAllowStaffList',
        'PurchasePriceSymbol',
        'LoanPriceSymbol',
        'CurrencySymbols'
    ];

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function matter()
    {
        return $this->belongsTo('App\Models\MatterCode', 'MatterCode', 'MatterCode');
    }

    public function lAInCharge()
    {
        return $this->belongsTo('App\Models\Staff', 'LAInCharge', 'staffCode');
    }

    public function clerkInCharge()
    {
        return $this->belongsTo('App\Models\Staff', 'ClerkInCharge', 'staffCode');
    }

    public function fileStatusRef()
    {
        return $this->belongsTo('App\Models\Filestatus', 'FileStatus', 'statusCode');
    }

    public function cust0()
    {
        return $this->belongsTo('App\Models\Customer', 'C0', 'customerCode');
    }

    public function cust1()
    {
        return $this->belongsTo('App\Models\Customer', 'C1', 'customerCode');
    }

    public function cust2()
    {
        return $this->belongsTo('App\Models\Customer', 'C2', 'customerCode');
    }

    public function cust3()
    {
        return $this->belongsTo('App\Models\Customer', 'C3', 'customerCode');
    }

    public function cust4()
    {
        return $this->belongsTo('App\Models\Customer', 'C4', 'customerCode');
    }

    public function cust5()
    {
        return $this->belongsTo('App\Models\Customer', 'C5', 'customerCode');
    }

    public function cust6()
    {
        return $this->belongsTo('App\Models\Customer', 'C6', 'customerCode');
    }

    public function cust7()
    {
        return $this->belongsTo('App\Models\Customer', 'C7', 'customerCode');
    }

    public function cust8()
    {
        return $this->belongsTo('App\Models\Customer', 'C8', 'customerCode');
    }

    public function cust9()
    {
        return $this->belongsTo('App\Models\Customer', 'C9', 'customerCode');
    }

    public function cust10()
    {
        return $this->belongsTo('App\Models\Customer', 'C10', 'customerCode');
    }

    public function cust11()
    {
        return $this->belongsTo('App\Models\Customer', 'C11', 'customerCode');
    }

    public function cust12()
    {
        return $this->belongsTo('App\Models\Customer', 'C12', 'customerCode');
    }

    public function cust13()
    {
        return $this->belongsTo('App\Models\Customer', 'C13', 'customerCode');
    }

    public function cust14()
    {
        return $this->belongsTo('App\Models\Customer', 'C14', 'customerCode');
    }

    public function cust15()
    {
        return $this->belongsTo('App\Models\Customer', 'C15', 'customerCode');
    }

    public function cust16()
    {
        return $this->belongsTo('App\Models\Customer', 'C16', 'customerCode');
    }

    public function cust17()
    {
        return $this->belongsTo('App\Models\Customer', 'C17', 'customerCode');
    }

    public function cust18()
    {
        return $this->belongsTo('App\Models\Customer', 'C18', 'customerCode');
    }

    public function cust19()
    {
        return $this->belongsTo('App\Models\Customer', 'C19', 'customerCode');
    }

    public function cust20()
    {
        return $this->belongsTo('App\Models\Customer', 'C20', 'customerCode');
    }

    public function cust21()
    {
        return $this->belongsTo('App\Models\Customer', 'C21', 'customerCode');
    }

    public function cust22()
    {
        return $this->belongsTo('App\Models\Customer', 'C22', 'customerCode');
    }

    public function cust23()
    {
        return $this->belongsTo('App\Models\Customer', 'C23', 'customerCode');
    }

    public function cust24()
    {
        return $this->belongsTo('App\Models\Customer', 'C24', 'customerCode');
    }

    public function cust25()
    {
        return $this->belongsTo('App\Models\Customer', 'C25', 'customerCode');
    }

    public function cust26()
    {
        return $this->belongsTo('App\Models\Customer', 'C26', 'customerCode');
    }

    public function cust27()
    {
        return $this->belongsTo('App\Models\Customer', 'C27', 'customerCode');
    }

    public function cust28()
    {
        return $this->belongsTo('App\Models\Customer', 'C28', 'customerCode');
    }

    public function cust29()
    {
        return $this->belongsTo('App\Models\Customer', 'C29', 'customerCode');
    }

    public function cust30()
    {
        return $this->belongsTo('App\Models\Customer', 'C30', 'customerCode');
    }

    public function proj1()
    {
        return $this->belongsTo('App\Models\Project', 'P1', 'projectCode');
    }

    public function prop1()
    {
        return $this->belongsTo('App\Models\Property', 'P1', 'PropertyCode');
    }

    public function prop2()
    {
        return $this->belongsTo('App\Models\Property', 'P2', 'PropertyCode');
    }

    public function prop3()
    {
        return $this->belongsTo('App\Models\Property', 'P3', 'PropertyCode');
    }

    public function prop4()
    {
        return $this->belongsTo('App\Models\Property', 'P4', 'PropertyCode');
    }

    public function prop5()
    {
        return $this->belongsTo('App\Models\Property', 'P5', 'PropertyCode');
    }

    public function lawyer1()
    {
        return $this->belongsTo('App\Models\Lawyer', 'Lawyer1', 'LawyerCode');
    }

    public function lawyer2()
    {
        return $this->belongsTo('App\Models\Lawyer', 'Lawyer2', 'LawyerCode');
    }

    public function bank1()
    {
        return $this->belongsTo('App\Models\Bankbranchcode', 'Bank1', 'BranchCode');
    }

    public function bank2()
    {
        return $this->belongsTo('App\Models\Bankbranchcode', 'Bank2', 'BranchCode');
    }

    public function presetChecklistRef()
    {
        return $this->belongsTo('App\Models\Spapresetchecklist', 'PChecklist', 'PresetCode');
    }

    public function getChecklistRefAttribute()
    {
        $checklist = Litigationassignedstage::where('Code', $this->FileNo1)->get();
        if (count($checklist) < 1) {
            return Spaassignedchecklist::where('FileNo', $this->FileNo1)->get();
        }
        return $checklist;
    }
}
