<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function __construct()
    {
        //
    }

    public function search(Request $request)
    {
        $project = Project::selectRaw("CONCAT_WS(' - ', projectCode, projectName) as text,
        projectCode as id")
            ->where('projectCode', 'LIKE', '%'.$request->input('term', '').'%')
            ->orWhere('projectName', 'LIKE', '%'.$request->input('term', '').'%')
            ->orderBy('projectCode', 'ASC')
            ->simplePaginate(10);
        $project->appends(['term' => $request->input('term', '')]);
        return ['results' => $project];
    }
}
