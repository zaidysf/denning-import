<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * Class Property
 *
 * @property int $PropertyCode
 * @property int $propertyFormat
 * @property string|null $PropertyID
 * @property string $country
 * @property string $state
 * @property string|null $TitleType
 * @property string|null $TitleNo
 * @property string|null $LotType
 * @property string|null $LotPTNo
 * @property string|null $MukimType
 * @property int|null $MukimCode
 * @property string|null $Mukim
 * @property string|null $DaerahType
 * @property string|null $Daerah
 * @property string|null $NegeriType
 * @property string|null $Negeri
 * @property string|null $Area
 * @property string|null $AreaDescription
 * @property string|null $LandTenure
 * @property Carbon|null $LeaseExpDate
 * @property Carbon|null $LeaseStrDate
 * @property string|null $ClassificationLand
 * @property bool $RestrictionInInterest
 * @property string|null $RestrictionAgainst
 * @property string|null $ApprovingAuthority
 * @property string|null $OtherRestriction
 * @property string|null $PropertyUse
 * @property string|null $ExpressCondition
 * @property string|null $Building/CultivationType
 * @property string|null $BuildingStoryNo
 * @property string|null $AgeOfTree
 * @property string|null $PropertyAdr
 * @property string|null $SectionBlockLabel
 * @property string|null $SectionBlockNo
 * @property string|null $UnitParcelLabel
 * @property string|null $Unit/ParcelNo
 * @property string|null $UnitFloorNo
 * @property string|null $UnitFloorLabel
 * @property string|null $UnitBuilding/Block
 * @property string|null $UnitBuildingLabel
 * @property string|null $UnitArea
 * @property string|null $UnitAreaDecription
 * @property int|null $ProjectCode
 * @property string|null $ProjectName
 * @property int|null $Developer
 * @property int|null $Proprietor
 * @property int|null $BlockMasterTitle
 * @property string|null $BlockMasterTitleAdd
 * @property string|null $BlockMasterTitle2Add
 * @property string|null $ParcelNo
 * @property string|null $FloorNo
 * @property string|null $BuildingNo
 * @property string|null $AccessoryParcelNo
 * @property string|null $UnitAccessoryParcelNo
 * @property string|null $AccessoryFloorNo
 * @property string|null $AccessoryBuildingNo
 * @property string|null $Shares
 * @property string|null $Totalshares
 * @property string|null $FloorArea
 * @property int|null $RegisteredOwner
 * @property string|null $Chargee
 * @property string|null $Branch
 * @property string|null $Remark
 * @property Carbon|null $dateEntered
 * @property int|null $enteredBy
 * @property Carbon|null $dateUpdated
 * @property int|null $updatedBy
 * @property string|null $remarks
 * @property string|null $blank1
 * @property string|null $blank2
 * @property string|null $propertyType
 * @property bool|null $titleComeOut
 * @property bool|null $HousingProject
 * @property string|null $Address
 * @property float|null $AnnualQuit
 * @property Carbon|null $TitleRegistrationDate
 * @property string|null $PreviousTitle
 * @property string|null $extra1
 * @property string|null $extra2
 * @property string|null $extra3
 * @property string|null $extra4
 * @property string|null $extra5
 * @property string|null $extra6
 * @property bool|null $ShowInList
 * @property string|null $BuildingModel
 * @property bool|null $IsReserveLand
 * @property bool|null $IsBumiLot
 *
 * @package App\Models
 */
class Property extends Model
{


    protected $table = 'property';
    protected $primaryKey = 'PropertyCode';
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'propertyFormat' => 'int',
        'MukimCode' => 'int',
        'RestrictionInInterest' => 'bool',
        'ProjectCode' => 'int',
        'Developer' => 'int',
        'Proprietor' => 'int',
        'BlockMasterTitle' => 'int',
        'RegisteredOwner' => 'int',
        'enteredBy' => 'int',
        'updatedBy' => 'int',
        'titleComeOut' => 'bool',
        'HousingProject' => 'bool',
        'AnnualQuit' => 'float',
        'ShowInList' => 'bool',
        'IsReserveLand' => 'bool',
        'IsBumiLot' => 'bool'
    ];

    protected $dates = [
        'LeaseExpDate',
        'LeaseStrDate',
        'dateEntered',
        'dateUpdated',
        'TitleRegistrationDate'
    ];

    protected $fillable = [
        'propertyFormat',
        'PropertyID',
        'country',
        'state',
        'TitleType',
        'TitleNo',
        'LotType',
        'LotPTNo',
        'MukimType',
        'MukimCode',
        'Mukim',
        'DaerahType',
        'Daerah',
        'NegeriType',
        'Negeri',
        'Area',
        'AreaDescription',
        'LandTenure',
        'LeaseExpDate',
        'LeaseStrDate',
        'ClassificationLand',
        'RestrictionInInterest',
        'RestrictionAgainst',
        'ApprovingAuthority',
        'OtherRestriction',
        'PropertyUse',
        'ExpressCondition',
        'Building/CultivationType',
        'BuildingStoryNo',
        'AgeOfTree',
        'PropertyAdr',
        'SectionBlockLabel',
        'SectionBlockNo',
        'UnitParcelLabel',
        'Unit/ParcelNo',
        'UnitFloorNo',
        'UnitFloorLabel',
        'UnitBuilding/Block',
        'UnitBuildingLabel',
        'UnitArea',
        'UnitAreaDecription',
        'ProjectCode',
        'ProjectName',
        'Developer',
        'Proprietor',
        'BlockMasterTitle',
        'BlockMasterTitleAdd',
        'BlockMasterTitle2Add',
        'ParcelNo',
        'FloorNo',
        'BuildingNo',
        'AccessoryParcelNo',
        'UnitAccessoryParcelNo',
        'AccessoryFloorNo',
        'AccessoryBuildingNo',
        'Shares',
        'Totalshares',
        'FloorArea',
        'RegisteredOwner',
        'Chargee',
        'Branch',
        'Remark',
        'dateEntered',
        'enteredBy',
        'dateUpdated',
        'updatedBy',
        'remarks',
        'blank1',
        'blank2',
        'propertyType',
        'titleComeOut',
        'HousingProject',
        'Address',
        'AnnualQuit',
        'TitleRegistrationDate',
        'PreviousTitle',
        'extra1',
        'extra2',
        'extra3',
        'extra4',
        'extra5',
        'extra6',
        'ShowInList',
        'BuildingModel',
        'IsReserveLand',
        'IsBumiLot'
    ];
}
