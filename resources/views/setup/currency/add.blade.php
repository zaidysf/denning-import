@extends('layout')
@section('content')
    <div class="mb-3">
        <a href="{{ route('setup.index') }}" class="title-link"><h1 class="h5 d-inline align-middle">&larr; Back to Setup</h1></a><br/>
        <h1 class="h3 d-inline align-middle">{{ $label }}</h1>

        @if ($method == 'add')
        <a href="#" id="btn-save" class="btn btn-outline-success float-end">Save</a>
        <a href="{{ url()->previous() }}" class="btn btn-outline-danger float-end" style="margin-right: 5px;">Cancel</a>
        @else
        <a href="#" id="btn-edit" class="btn btn-warning float-end">Edit</a>
        <a href="#" id="btn-save" class="btn btn-outline-success float-end" style="display: none;">Save</a>
        <a href="#" id="btn-cancel" class="btn btn-outline-danger float-end" style="display: none; margin-right: 5px;">Cancel</a>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form
                        action="{{ $formActionURL }}"
                        method="POST"
                        enctype="multipart/form-data"
                        id="form-firm"
                    >
                        @if ($method == 'edit')
                        <input type="hidden" name="_method" value="PUT" />
                        @endif
                        @csrf
                        <div class="card card-body">
                            <div class="mb-3">
                                <label class="form-label">Firm Name</label>
                                <input id="Name" name="Name" type="text" class="form-control" value="{{ isset($data->Name) ? $data->Name : "" }}" readonly/>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Branch Name</label>
                                <input id="BranchName" name="BranchName" type="text" class="form-control" value="{{ isset($data->BranchName) ? $data->BranchName : "" }}" readonly/>
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Default Currency Symbol</label>
                                <select name="DefaultCurrencySymbol" class="form-control form-flexible">
                                    @foreach ($currencies as $v)
                                        <option value="{{ $v->Symbol }}"{{ isset($data->DefaultCurrencySymbol) && $data->DefaultCurrencySymbol == $v->Symbol ? " selected=selected" : "" }}>{{ $v->Symbol }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Tax Label</label>
                                <select id="DefaultTaxLabel" name="DefaultTaxLabel" class="form-control form-flexible">
                                    <option value="Tax"{{ isset($data->DefaultTaxLabel) && $data->DefaultTaxLabel == 'Tax'  ? " selected=selected" : "" }}>Tax</option>
                                    <option value="GST"{{ isset($data->DefaultTaxLabel) && $data->DefaultTaxLabel == 'GST'  ? " selected=selected" : "" }}>GST</option>
                                    <option value="SST"{{ isset($data->DefaultTaxLabel) && $data->DefaultTaxLabel == 'SST'  ? " selected=selected" : "" }}>SST</option>
                                    <option value="VAT"{{ isset($data->DefaultTaxLabel) && $data->DefaultTaxLabel == 'VAT'  ? " selected=selected" : "" }}>VAT</option>
                                </select>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form1" role="button" aria-expanded="false" aria-controls="form1">
                                GST Section <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form1">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">GST Registered</label>
                                    <select id="GSTRegistered" name="GSTRegistered" class="form-control form-flexible">
                                        <option value="No"{{ isset($data->GSTRegistered) && $data->GSTRegistered == 'No'  ? " selected=selected" : "" }}>No</option>
                                        <option value="Yes"{{ isset($data->GSTRegistered) && $data->GSTRegistered == 'Yes'  ? " selected=selected" : "" }}>Yes</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">GST Registration No</label>
                                    <input id="GSTRegNo" name="GSTRegNo" type="text" class="form-control form-flexible" value="{{ isset($data->GSTRegNo) ? $data->GSTRegNo : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">GST Registration Date</label>
                                    <input id="GSTRegisteredDate" name="GSTRegisteredDate" type="date" class="form-control form-flexible" value="{{ isset($data->GSTRegisteredDate) ? date('Y-m-d', strtotime($data->GSTRegisteredDate)) : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Commencement Date</label>
                                    <input id="CommenceDate" name="CommenceDate" type="date" class="form-control form-flexible" value="{{ isset($data->CommenceDate) ? date('Y-m-d', strtotime($data->CommenceDate)) : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">GST Taxable Period</label>
                                    <select id="GSTTaxablePeriod" name="GSTTaxablePeriod" class="form-control form-flexible">
                                        <option value="1"{{ isset($data->GSTTaxablePeriod) && $data->GSTTaxablePeriod == '1'  ? " selected=selected" : "" }}>1</option>
                                        <option value="2"{{ isset($data->GSTTaxablePeriod) && $data->GSTTaxablePeriod == '2'  ? " selected=selected" : "" }}>2</option>
                                        <option value="3"{{ isset($data->GSTTaxablePeriod) && $data->GSTTaxablePeriod == '3'  ? " selected=selected" : "" }}>3</option>
                                        <option value="4"{{ isset($data->GSTTaxablePeriod) && $data->GSTTaxablePeriod == '4'  ? " selected=selected" : "" }}>4</option>
                                        <option value="5"{{ isset($data->GSTTaxablePeriod) && $data->GSTTaxablePeriod == '5'  ? " selected=selected" : "" }}>5</option>
                                        <option value="6"{{ isset($data->GSTTaxablePeriod) && $data->GSTTaxablePeriod == '6'  ? " selected=selected" : "" }}>6</option>
                                        <option value="12"{{ isset($data->GSTTaxablePeriod) && $data->GSTTaxablePeriod == '12'  ? " selected=selected" : "" }}>12</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">GST Effective Date</label>
                                    <input id="GSTDateEffective" name="GSTDATEEffective" type="date" class="form-control form-flexible" value="{{ isset($data->GSTDateEffective) ? date('Y-m-d', strtotime($data->GSTDateEffective)) : "" }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form2" role="button" aria-expanded="false" aria-controls="form2">
                                SST Section (for Malaysia only) <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form2">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">SST Registered (MY only)</label>
                                    <select id="SSTRegistered" name="SSTRegistered" class="form-control form-flexible">
                                        <option value="No"{{ isset($data->SSTRegistered) && $data->SSTRegistered == 'No'  ? " selected=selected" : "" }}>No</option>
                                        <option value="Yes"{{ isset($data->SSTRegistered) && $data->SSTRegistered == 'Yes'  ? " selected=selected" : "" }}>Yes</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">SST Registration No</label>
                                    <input id="SSTRegNo" name="SSTRegNo" type="text" class="form-control form-flexible" value="{{ isset($data->SSTRegNo) ? $data->SSTRegNo : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">SST Effective Date</label>
                                    <input id="SSTDateEffective" name="SSTDateEffective" type="date" class="form-control form-flexible" value="{{ isset($data->SSTDateEffective) ? date('Y-m-d', strtotime($data->SSTDateEffective)) : "" }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form3" role="button" aria-expanded="false" aria-controls="form3">
                                Tax Rate <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form3">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">GST / Service Tax Rate</label>
                                    <select id="ServiceTax" name="ServiceTax" class="form-control form-flexible">
                                        @for ($i=1;$i<16;$i++)
                                            <option value="{{ '0.'.str_pad($i,2,'0',STR_PAD_LEFT) }}"{{ isset($data->ServiceTax) && $data->ServiceTax == '0.'.str_pad($i,2,'0',STR_PAD_LEFT) ? " selected=selected" : "" }}>{{ '0.'.str_pad($i,2,'0',STR_PAD_LEFT) }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Sale Tax Rate (for Malaysia)</label>
                                    <select id="SaleTax" name="SaleTax" class="form-control form-flexible">
                                        @for ($i=1;$i<16;$i++)
                                            <option value="{{ '0.'.str_pad($i,2,'0',STR_PAD_LEFT) }}"{{ isset($data->SaleTax) && $data->SaleTax == '0.'.str_pad($i,2,'0',STR_PAD_LEFT) ? " selected=selected" : "" }}>{{ '0.'.str_pad($i,2,'0',STR_PAD_LEFT) }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
<style>
.title-link h1 {
    color: blue;
}
.text-left {
  text-align: left !important;
}
</style>
@endsection

@section('script')
<script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>>
<script>
    @if ($method != 'add')
    $(document).ready(function(){
        $(".form-flexible").prop("disabled", true);
    });
    $('#btn-edit').click( function(e) {
        $(".form-flexible").prop("disabled", false);
        $('#btn-save').toggle();
        $('#btn-edit').toggle();
        $('#btn-cancel').toggle();
    });
    $('#btn-cancel').click( function(e) {
        $(".form-flexible").prop("disabled", true);
        $('#btn-save').toggle();
        $('#btn-edit').toggle();
        $('#btn-cancel').toggle();
    });
    @endif
    $('#btn-save').click( function(e) {
        e.preventDefault();
        $('form#form-firm').submit();
    });
    $('#select-page').change(function(e){
        if (this.value != null && this.value != "") {
            let searchParams = new URLSearchParams(window.location.search)
            searchParams.delete('per_page');
            searchParams.append('per_page', this.value);
            window.location='{{ route('setup.staff.index') }}?'+searchParams.toString();
        }
    });
    $("#searchForm").submit( function(eventObj) {
        let searchParams = new URLSearchParams(window.location.search)
        searchParams.delete("_token");
        searchParams.delete("key");
        searchParams.delete("value");
        searchParams.forEach(function(value, key) {
            $("<input />").attr("type", "hidden")
                .attr("name", key)
                .attr("value", value)
                .appendTo("#searchForm");
        });
        return true;
    });
</script>
@endsection
