@extends('layout')
@section('content')
    <div class="mb-3">
        <h1 class="h3 d-inline align-middle" style="margin-right: 20px">Bulk Templates List</h1>
        @if (!\Request::has('generate'))
        <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#addModal"> + Upload Template </button>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Bulk Template</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form
                                        action="#"
                                        id="form-modal"
                                        method="POST"
                                        enctype="multipart/form-data"
                                    >
                                        <input type="hidden" name="_method" value="PUT"/>
                                        @csrf
                                        <div class="mb-3">
                                            <label class="form-label">Template ID</label>
                                            <input id="form-modal-templateid" name="template_id" type="text" class="form-control" readonly/>
                                        </div>
                                        <div class="mb-3">
                                            Existing file : <span id="form-modal-file"></span>
                                            <input name="file" type="file" class="form-control" accept=".doc,.docx"/>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">File Type</label>
                                            <input type="text" value="docx" class="form-control" readonly/>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Rename Template</label>
                                            <input id="form-modal-name" name="name" type="text" class="form-control"/>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Type</label>
                                            <select id="form-modal-type" name="type" class="form-select">
                                                @foreach ($selectType as $k => $v)
                                                    <option value="{{ $k }}">{{ $v }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Bank</label>
                                            <select id="form-modal-bank" name="bank" class="form-select">
                                                @foreach ($selectBank as $k => $v)
                                                    <option value="{{ $k }}">{{ $v }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Category</label>
                                            <select id="form-modal-category" name="category" class="form-select">
                                                @foreach ($selectCategory as $k => $v)
                                                    <option value="{{ $k }}">{{ $v }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label for="favourite" class="inline-flex items-center">
                                                <input id="form-modal-favourite" name="favourite" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
                                                <span class="ml-2 text-sm text-gray-600">Favourite</span>
                                            </label>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Status</label>
                                            <select id="form-modal-status" name="status" class="form-select">
                                                @foreach ($selectStatus as $k => $v)
                                                    <option value="{{ $k }}">{{ $v }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <div class="row">
                                                <div class="col">
                                                    <label class="form-label">Uploaded By</label>
                                                    <input id="form-modal-createdby" type="text" class="form-control" readonly/>
                                                </div>
                                                <div class="col">
                                                    <label class="form-label">Date Uploaded</label>
                                                    <input id="form-modal-createdat" type="text" class="form-control" readonly/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <div class="row">
                                                <div class="col">
                                                    <label class="form-label">Updated By</label>
                                                    <input id="form-modal-updatedby" type="text" class="form-control" readonly/>
                                                </div>
                                                <div class="col">
                                                    <label class="form-label">Date Updated</label>
                                                    <input id="form-modal-updatedat" type="text" class="form-control" readonly/>
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <button type="submit" class="btn btn-success" style="float:left">Update</button>
                                    </form>
                                    <form
                                        id="form-mailmerge-delete"
                                        method="POST"
                                        class="pull-right"
                                        style="float:right"
                                    >
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE"/>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="addModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="addModalLabel">Add Bulk Template</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form
                                        action="{{ route('mail-merge.template.upload') }}"
                                        method="POST"
                                        enctype="multipart/form-data"
                                    >
                                        @csrf
                                        <div class="mb-3">
                                            <label class="form-label">Template ID</label>
                                            {{-- <input id="template_id" name="template_id" type="text" class="form-control"/> --}}
                                            <input type="text" class="form-control" value="System Generated" readonly/>
                                        </div>
                                        <div class="mb-3">
                                            <input name="file" type="file" class="form-control" accept=".doc,.docx"/>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Rename Template</label>
                                            <input id="filename" name="name" type="text" class="form-control"/>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Type</label>
                                            <select name="type" class="form-select">
                                                @foreach ($selectType as $k => $v)
                                                    <option value="{{ $k }}">{{ $v }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Bank</label>
                                            <select name="bank" class="form-select">
                                                @foreach ($selectBank as $k => $v)
                                                    <option value="{{ $k }}">{{ $v }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Category</label>
                                            <select name="category" class="form-select">
                                                @foreach ($selectCategory as $k => $v)
                                                    <option value="{{ $k }}">{{ $v }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label for="favourite" class="inline-flex items-center">
                                                <input name="favourite" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
                                                <span class="ml-2 text-sm text-gray-600">Favourite</span>
                                            </label>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Status</label>
                                            <select name="status" class="form-select">
                                                @foreach ($selectStatus as $k => $v)
                                                    <option value="{{ $k }}">{{ $v }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <hr/>
                                        <button type="submit" class="btn btn-success">Save</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form method="GET" action="{{ route('mail-merge.template.index') }}" id="searchForm">
                    @csrf
                    <div class="row">
                        <div class="col mb-2">
                            <select name="key" class="form-control">
                                @foreach ($selectColumn as $k => $v)
                                    <option {{ isset($_GET['key']) ? ($_GET['key'] == $k ? "selected" : "") : "" }} value="{{ $k }}">{{ $v }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <input
                                placeholder="Enter keywords..."
                                name="value"
                                type="text"
                                class="form-control"
                                value="{{ isset($_GET['value']) ? $_GET['value'] : "" }}"/>
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                        <div class="col">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="status" id="statusCheck" {{ isset($_GET['status']) ? ($_GET['status'] == 2 ? "checked" : "") : "" }}>
                                <label class="form-check-label" for="statusCheck">
                                    Outdated
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="favourite" id="favouriteCheck" {{ isset($_GET['favourite']) ? ($_GET['favourite'] == 1 ? "checked" : "") : "" }}>
                                <label class="form-check-label" for="favouriteCheck">
                                    Favourite
                                </label>
                            </div>
                        </div>
                        <div class="col pull-right text-end mt-2">
                            Per Page
                        </div>
                        <div class="col-1 pull-right text-end">
                            <select id="select-page" name="per_page" class="form-control">
                                @foreach ($selectPagination as $k => $v)
                                    <option {{ isset($_GET['per_page']) ? ($_GET['per_page'] == $k ? "selected" : "") : "" }} value="{{ $k }}">
                                        {{ $v }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    </form>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">&nbsp;</th>
                                <th scope="col">ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">File</th>
                                <th scope="col">Type</th>
                                <th scope="col">Bank</th>
                                <th scope="col">Category</th>
                                <th scope="col">Status</th>
                                <th scope="col">Favourite</th>
                                <th scope="col">Uploaded By</th>
                                <th scope="col">Date Uploaded</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $v)
                                <tr>
                                    <td>
                                        @if (\Request::has('generate'))
                                        <form action="{{ route('export.post') }}" method="POST" id="exportForm">
                                            @csrf
                                            <input type="hidden" name="templateId" value="{{ $v->id }}" />
                                            <button type="submit" class="btn btn-success btn-sm" id="exportFormBtn">
                                                Generate
                                            </button>
                                        </form>
                                        @else
                                        <button type="button" class="btn btn-warning btn-sm" onclick="passEditData({{ $v->id }})" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            Edit
                                        </button>
                                        @endif
                                    </td>
                                    <td>{{ $v->template_id }}</td>
                                    <td>{{ $v->name }}</td>
                                    <td><a href="{{ route('mail-merge.template.download', ['id' => $v->id]) }}">.docx</a></td>
                                    <td>{{ $v->typeRef->name }}</td>
                                    <td>{{ $v->bankRef->BankCode }}</td>
                                    <td>{{ $v->cat->Category }}</td>
                                    <td>{{ $v->status_name }}</td>
                                    <td>{{ $v->favourite_name }}</td>
                                    <td>{{ $v->staff->name }}</td>
                                    <td>{{ date('Y-m-d', strtotime($v->created_at)) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @include('pagination', ['paginator' => $data])
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('script')
<script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>>
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    @if (\Request::has('generate'))
        $("#exportForm").submit( function(eventObj) {
            let searchParams = new URLSearchParams(window.location.search)
            searchParams.delete("_token");
            searchParams.delete("key");
            searchParams.delete("value");
            searchParams.delete("per_page");
            searchParams.delete("favourite");
            searchParams.delete("status");
            searchParams.delete("templateId");
            searchParams.set("mailMerge", 1);
            searchParams.forEach(function(value, key) {
                $("<input />").attr("type", "hidden")
                    .attr("name", key)
                    .attr("value", value)
                    .appendTo("#exportForm");
            });
            return true;
        });
    @endif
    function generate(id) {
        let searchParams = new URLSearchParams(window.location.search)
        window.location='{{ route('mail-merge.template.generate') }}?'+searchParams.toString();
    }
    function passEditData(id) {
        $.ajax({url: "/api/v1/mail-merge/"+id, success: function(result){
            let updatedAt = new Date(result.data.updated_at);
            let createdAt = new Date(result.data.created_at);
            let updatedAtFormatted = updatedAt.getFullYear() + "-" + (updatedAt.getMonth() + 1) + "-" + updatedAt.getDate() + " " + updatedAt.getHours() + ":" + updatedAt.getMinutes() + ":" + updatedAt.getSeconds();
            let createdAtFormatted = createdAt.getFullYear() + "-" + (createdAt.getMonth() + 1) + "-" + createdAt.getDate() + " " + createdAt.getHours() + ":" + createdAt.getMinutes() + ":" + createdAt.getSeconds();

            $('#form-modal').attr('action', '/mail-merge/template/'+id);
            $('#form-modal-templateid').val(result.data.template_id);
            $('#form-modal-file').html('<a href="/storage/'+result.data.file_path+'">.docx</a>');
            $('#form-modal-name').val(result.data.name);
            $('#form-modal-type').val(result.data.type).change();
            $('#form-modal-bank').val(result.data.bank).change();
            $('#form-modal-category').val(result.data.category).change();
            if (result.data.favourite == 1) {
                $('#form-modal-favourite').attr('checked', 'checked');
            } else {
                $('#form-modal-favourite').removeAttr('checked');
            }
            $('#form-modal-status').val(result.data.status).change();
            $('#form-modal-createdby').val(result.data.created_by.name).change();
            $('#form-modal-createdat').val(createdAtFormatted).change();
            $('#form-modal-updatedby').val(result.data.updated_by.name).change();
            $('#form-modal-updatedat').val(updatedAtFormatted).change();
            $('#form-mailmerge-delete').attr('action', '/mail-merge/template/'+id);
        }});
    }
    $('input[type=file]').change(function(e){
        var fileWithExt = e.target.files[0].name
        var name = fileWithExt.substring(0, fileWithExt.lastIndexOf('.')) || fileWithExt
        $('#filename').val(name);
    });
    $('#select-page').change(function(e){
        if (this.value != null && this.value != "") {
            let searchParams = new URLSearchParams(window.location.search)
            searchParams.delete('per_page');
            searchParams.append('per_page', this.value);
            window.location='{{ route('mail-merge.template.index') }}?'+searchParams.toString();
        }
    });
    $("#searchForm").submit( function(eventObj) {
        let searchParams = new URLSearchParams(window.location.search)
        searchParams.delete("_token");
        searchParams.delete("key");
        searchParams.delete("value");
        searchParams.forEach(function(value, key) {
            $("<input />").attr("type", "hidden")
                .attr("name", key)
                .attr("value", value)
                .appendTo("#searchForm");
        });
        return true;
    });
    $('#favouriteCheck').click(function() {
        let searchParams = new URLSearchParams(window.location.search)
        searchParams.delete('favourite');
        if ($(this).prop('checked')) {
            searchParams.append('favourite', 1);
        }
        window.location='{{ route('mail-merge.template.index') }}?'+searchParams.toString();
    });
    $('#statusCheck').click(function() {
        let searchParams = new URLSearchParams(window.location.search)
        searchParams.delete('status');
        if ($(this).prop('checked')) {
            searchParams.append('status', 2);
        }
        window.location='{{ route('mail-merge.template.index') }}?'+searchParams.toString();
    });
</script>
@endsection
