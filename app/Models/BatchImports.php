<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * Class BatchImports
 *
 * @property int $id
 * @property string $batch_import_no
 * @property int $matter
 * @property int $c0
 * @property string $file_path
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class BatchImports extends Model
{


    protected $table = 'batch_imports';
    public static $snakeAttributes = false;

    protected $casts = [
        'matter' => 'int',
        'c0' => 'int',
        'created_by' => 'int'
    ];

    protected $fillable = [
        'BatchNo',
        'staff_id',
        'matter',
        'c0',
        'file_path',
        'count',
        'created_at',
        'status'
    ];

    public function mattercode()
    {
        return $this->belongsTo('App\Models\MatterCode', 'matter', 'MatterCode');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'c0', 'customerCode');
    }

    public function staff()
    {
        return $this->belongsTo('App\Models\Staff', 'staff_id', 'staffCode');
    }

    public function getStatusNameAttribute()
    {
        switch ($this->status) {
            case 1:
                return "Completed";
                break;
            case 2:
                return "Failed";
                break;
            default:
                return "In Progress";
                break;
        }
    }
}
