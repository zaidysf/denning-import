<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * Class Customer
 *
 * @property int $customerCode
 * @property int|null $idType
 * @property string|null $idNo
 * @property string|null $idMerge
 * @property string|null $idNoOld
 * @property string|null $name
 * @property string|null $initials
 * @property string|null $race
 * @property int|null $raceCode
 * @property string|null $religion
 * @property int|null $religionCode
 * @property string|null $title
 * @property string|null $sex
 * @property string|null $sexCode
 * @property string|null $address1
 * @property string|null $address2
 * @property string|null $address3
 * @property string|null $postcode
 * @property string|null $city
 * @property string|null $state
 * @property string|null $country
 * @property string|null $correspondingAdd
 * @property string|null $extraAdd
 * @property string|null $extraAdd2
 * @property string|null $extraAdd3
 * @property string|null $phoneHome
 * @property string|null $phoneOffice
 * @property string|null $fax
 * @property string|null $phoneMobile
 * @property string|null $emailAdd
 * @property string $qbID
 * @property string|null $webSite
 * @property string|null $contactPerson
 * @property string|null $mailList
 * @property int|null $newsletter
 * @property string|null $citizenship
 * @property Carbon|null $dateBirth
 * @property string|null $placeBirth
 * @property int|null $age
 * @property string|null $marital
 * @property string|null $maritalCode
 * @property string|null $spouseName
 * @property string|null $spouseOccupation
 * @property int|null $spouseOccupationCode
 * @property int|null $childrenNo
 * @property string|null $occupation
 * @property int|null $occupationCode
 * @property string|null $placeofWork
 * @property float|null $monthlyIncome
 * @property string|null $taxFileNo
 * @property int|null $irdBranch
 * @property string|null $customerType
 * @property string|null $RegOff
 * @property int|null $director1
 * @property int|null $director2
 * @property int|null $secretary
 * @property string|null $govtDept
 * @property bool|null $deceased
 * @property Carbon|null $dateDeceased
 * @property bool|null $IsBankrupt
 * @property Carbon|null $dateBankruptCheck
 * @property bool|null $InviteToApp
 * @property Carbon|null $dateEntered
 * @property int|null $enteredBy
 * @property Carbon|null $dateUpdated
 * @property int|null $updatedBy
 * @property string|null $remarks
 * @property string|null $note1
 * @property string|null $note2
 * @property string|null $note3
 * @property string|null $note4
 * @property string|null $kp
 * @property string|null $lama
 * @property string|null $ic
 * @property string|null $old
 * @property string|null $idOpenBracket
 * @property string|null $idComa
 * @property string|null $idCloseBracket
 * @property string|null $maritalOpenBracket
 * @property string|null $maritalComa
 * @property string|null $maritalCloseBracket
 * @property string|null $addressComa
 * @property string|null $extra1
 * @property string|null $extra2
 * @property string|null $extra3
 * @property string|null $extra4
 * @property string|null $extra5
 * @property string $BankAccountName
 * @property string $BankAccountNo
 * @property bool|null $ShowInList
 * @property bool $GSTStatusVerified
 * @property Carbon|null $GSTStatusVerifiedDate
 * @property bool $IsGSTRegistered
 * @property string|null $GSTNo
 * @property string|null $GSTRMCDNo
 * @property string|null $SupplierAccNo
 * @property string|null $CustomerAccNo
 * @property string|null $DefAllowStaffList
 * @property string|null $Phone1Ctr
 * @property string|null $Phone1No
 * @property string|null $Phone2Ctr
 * @property string|null $Phone2No
 * @property string|null $Phone3Ctr
 * @property string|null $Phone3No
 * @property string|null $Fax1Ctr
 * @property string|null $Fax1No
 * @property bool|null $IsFavouritePayee
 *
 * @package App\Models
 */
class Customer extends Model
{


    protected $table = 'customer';
    protected $primaryKey = 'customerCode';
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'idType' => 'int',
        'raceCode' => 'int',
        'religionCode' => 'int',
        'newsletter' => 'int',
        'age' => 'int',
        'spouseOccupationCode' => 'int',
        'childrenNo' => 'int',
        'occupationCode' => 'int',
        'monthlyIncome' => 'float',
        'irdBranch' => 'int',
        'director1' => 'int',
        'director2' => 'int',
        'secretary' => 'int',
        'deceased' => 'bool',
        'IsBankrupt' => 'bool',
        'InviteToApp' => 'bool',
        'enteredBy' => 'int',
        'updatedBy' => 'int',
        'ShowInList' => 'bool',
        'GSTStatusVerified' => 'bool',
        'IsGSTRegistered' => 'bool',
        'IsFavouritePayee' => 'bool'
    ];

    protected $dates = [
        'dateBirth',
        'dateDeceased',
        'dateBankruptCheck',
        'dateEntered',
        'dateUpdated',
        'GSTStatusVerifiedDate'
    ];

    protected $hidden = [
        'secretary'
    ];

    protected $fillable = [
        'idType',
        'idNo',
        'idMerge',
        'idNoOld',
        'name',
        'initials',
        'race',
        'raceCode',
        'religion',
        'religionCode',
        'title',
        'sex',
        'sexCode',
        'address1',
        'address2',
        'address3',
        'postcode',
        'city',
        'state',
        'country',
        'correspondingAdd',
        'extraAdd',
        'extraAdd2',
        'extraAdd3',
        'phoneHome',
        'phoneOffice',
        'fax',
        'phoneMobile',
        'emailAdd',
        'qbID',
        'webSite',
        'contactPerson',
        'mailList',
        'newsletter',
        'citizenship',
        'dateBirth',
        'placeBirth',
        'age',
        'marital',
        'maritalCode',
        'spouseName',
        'spouseOccupation',
        'spouseOccupationCode',
        'childrenNo',
        'occupation',
        'occupationCode',
        'placeofWork',
        'monthlyIncome',
        'taxFileNo',
        'irdBranch',
        'customerType',
        'RegOff',
        'director1',
        'director2',
        'secretary',
        'govtDept',
        'deceased',
        'dateDeceased',
        'IsBankrupt',
        'dateBankruptCheck',
        'InviteToApp',
        'dateEntered',
        'enteredBy',
        'dateUpdated',
        'updatedBy',
        'remarks',
        'note1',
        'note2',
        'note3',
        'note4',
        'kp',
        'lama',
        'ic',
        'old',
        'idOpenBracket',
        'idComa',
        'idCloseBracket',
        'maritalOpenBracket',
        'maritalComa',
        'maritalCloseBracket',
        'addressComa',
        'extra1',
        'extra2',
        'extra3',
        'extra4',
        'extra5',
        'BankAccountName',
        'BankAccountNo',
        'ShowInList',
        'GSTStatusVerified',
        'GSTStatusVerifiedDate',
        'IsGSTRegistered',
        'GSTNo',
        'GSTRMCDNo',
        'SupplierAccNo',
        'CustomerAccNo',
        'DefAllowStaffList',
        'Phone1Ctr',
        'Phone1No',
        'Phone2Ctr',
        'Phone2No',
        'Phone3Ctr',
        'Phone3No',
        'Fax1Ctr',
        'Fax1No',
        'IsFavouritePayee'
    ];
}
