<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\MatterCode;
use Illuminate\Http\Request;

class MatterController extends Controller
{
    public function __construct()
    {
        //
    }

    public function search(Request $request)
    {
        $matter = MatterCode::selectRaw("CONCAT_WS(' - ', MatterCode, matter) as text, MatterCode as id")
            ->where('MatterCode', 'LIKE', '%'.$request->input('term', '').'%')
            ->orWhere('Matter', 'LIKE', '%'.$request->input('term', '').'%')
            ->orderBy('Matter', 'ASC')
            ->simplePaginate(10);
        $matter->appends(['term' => $request->input('term', '')]);
        return ['results' => $matter];
    }
}
