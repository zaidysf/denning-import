<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailMergesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_merges', function (Blueprint $table) {
            $table->id();
            $table->string('template_id')->nullable();
            $table->string('name')->nullable();
            $table->string('file_path')->nullable();
            $table->string('type');
            $table->string('bank');
            $table->string('category')->nullable();
            $table->bigInteger('staff_id');
            $table->tinyInteger('favourite')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->bigInteger('created_by');
            $table->bigInteger('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mail_merges');
    }
}
