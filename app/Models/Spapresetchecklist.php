<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Spapresetchecklist
 *
 * @property string $PresetCode
 * @property string $Description
 * @property string $Category
 * @property int|null $CategoryID
 * @property Carbon|null $dateEntered
 * @property int|null $enteredBy
 * @property Carbon|null $dateUpdated
 * @property int|null $updatedBy
 * @property string|null $remarks
 *
 * @package App\Models
 */
class Spapresetchecklist extends Model
{
    protected $table = 'spapresetchecklist';
    protected $primaryKey = 'PresetCode';
    public $incrementing = false;
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'CategoryID' => 'int',
        'enteredBy' => 'int',
        'updatedBy' => 'int'
    ];

    protected $dates = [
        'dateEntered',
        'dateUpdated'
    ];

    protected $fillable = [
        'Description',
        'Category',
        'CategoryID',
        'dateEntered',
        'enteredBy',
        'dateUpdated',
        'updatedBy',
        'remarks'
    ];
}
