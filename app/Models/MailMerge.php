<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MailMerge extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'type',
        'category',
        'file_path',
        'staff_id',
        'bank',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
        'status'
    ];

    public function staff()
    {
        return $this->belongsTo('App\Models\User', 'staff_id', 'id');
    }

    public function typeRef()
    {
        return $this->belongsTo('App\Models\BulkTemplateType', 'type', 'id');
    }

    public function bankRef()
    {
        return $this->belongsTo('App\Models\Bankcode', 'bank', 'BankCode');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\User', 'updated_by', 'id');
    }

    public function cat()
    {
        return $this->belongsTo('App\Models\Cbomattercategory', 'category', 'ID');
    }

    public function getStatusNameAttribute()
    {
        switch ($this->status) {
            case 1:
                return "Active";
                break;
            default:
                return "Outdated";
                break;
        }
    }

    public function getFavouriteNameAttribute()
    {
        switch ($this->favourite) {
            case 1:
                return "Yes";
                break;
            default:
                return "No";
                break;
        }
    }
}
