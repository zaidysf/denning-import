<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Litigationassignedstage
 *
 * @property string $Code
 * @property string $StageCode
 * @property int $Ranking
 * @property int|null $AssignedTo
 * @property Carbon|null $StartDate
 * @property Carbon|null $DueDate
 * @property int $TimeEstimate
 * @property bool $Done
 * @property Carbon|null $CompletionDate
 * @property string|null $Remark
 * @property int $ReferToItem
 * @property int $NotRequired
 * @property int $chk1
 * @property int $chk2
 * @property Carbon|null $DateChk1
 * @property Carbon|null $DateChk2
 * @property float|null $$1
 * @property float|null $$2
 * @property float|null $$3
 * @property float|null $$4
 * @property float|null $$5
 * @property Carbon|null $Date1
 * @property Carbon|null $Date2
 * @property Carbon|null $Date3
 * @property Carbon|null $Date4
 * @property Carbon|null $Date5
 * @property string|null $Text1
 * @property string|null $Text2
 * @property string|null $Text3
 * @property string|null $Text4
 * @property string|null $Text5
 * @property Carbon|null $dateEntered
 * @property int|null $enteredBy
 * @property Carbon|null $dateUpdated
 * @property int|null $updatedBy
 * @property string|null $remarks
 *
 * @package App\Models
 */
class Litigationassignedstage extends Model
{
    protected $table = 'litigationassignedstage';
    public $incrementing = false;
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'Ranking' => 'int',
        'AssignedTo' => 'int',
        'TimeEstimate' => 'int',
        'Done' => 'bool',
        'ReferToItem' => 'int',
        'NotRequired' => 'int',
        'chk1' => 'int',
        'chk2' => 'int',
        '$1' => 'float',
        '$2' => 'float',
        '$3' => 'float',
        '$4' => 'float',
        '$5' => 'float',
        'enteredBy' => 'int',
        'updatedBy' => 'int'
    ];

    protected $dates = [
        'StartDate',
        'DueDate',
        'CompletionDate',
        'DateChk1',
        'DateChk2',
        'Date1',
        'Date2',
        'Date3',
        'Date4',
        'Date5',
        'dateEntered',
        'dateUpdated'
    ];

    protected $fillable = [
        'Ranking',
        'AssignedTo',
        'StartDate',
        'DueDate',
        'TimeEstimate',
        'Done',
        'CompletionDate',
        'Remark',
        'ReferToItem',
        'NotRequired',
        'chk1',
        'chk2',
        'DateChk1',
        'DateChk2',
        '$1',
        '$2',
        '$3',
        '$4',
        '$5',
        'Date1',
        'Date2',
        'Date3',
        'Date4',
        'Date5',
        'Text1',
        'Text2',
        'Text3',
        'Text4',
        'Text5',
        'dateEntered',
        'enteredBy',
        'dateUpdated',
        'updatedBy',
        'remarks'
    ];

    public function checklistRef()
    {
        return $this->belongsTo('App\Models\Litigationstage', 'StageCode', 'StageCode');
    }

    public function getCodeNameAttribute()
    {
        return $this->Code;
    }
}
