<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\File;
use Illuminate\Http\Request;

class FileController extends Controller
{
    public function __construct()
    {
        //
    }

    public function search(Request $request)
    {
        $file = File::selectRaw("CONCAT_WS(' - ', FileNo1, customer.name) AS text, FileNo1 AS id")
        ->leftJoin('customer', 'file.C0', '=', 'customer.customerCode')
        ->with(['cust0:name'])
        ->where('FileNo1', 'LIKE', '%'.$request->input('term', '').'%')
        ->orWhereHas('cust0', function ($query) use ($request) {
            $query->where('name', 'like', '%' .$request->input('term', '') . '%');
        })
        ->simplePaginate(10);
        // dd($file->toJson());
        $file->appends(['term' => $request->input('term', '')]);
        return ['results' => $file];
    }
}
