<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * Class MatterCode
 *
 * @property string $MatterCode
 * @property string|null $Matter
 * @property string $Industry
 * @property string|null $Category
 * @property string $WorkNature
 * @property string $DesignFor
 * @property string|null $Dept
 * @property string|null $Code
 * @property int|null $TurnaroundTime
 * @property bool|null $ShowInList
 * @property string|null $SpaCheckListPresetCode
 * @property string|null $BillingPresetCode
 * @property string|null $PresetCodeG1
 * @property string|null $PresetCodeG2
 * @property bool $IsDeleted
 * @property string|null $Remark
 * @property int|null $enteredBy
 * @property Carbon|null $dateEntered
 * @property int|null $updatedBy
 * @property string|null $remarks
 * @property Carbon|null $dateUpdated
 * @property string|null $PresetCodeG3
 * @property string|null $GroupC1
 * @property string|null $GroupC2
 * @property string|null $GroupC3
 * @property string|null $GroupC4
 * @property string|null $GroupC5
 * @property string|null $GroupC6
 * @property string|null $GroupL1
 * @property string|null $GroupL2
 * @property string|null $GroupL3
 * @property string|null $GroupL4
 * @property string|null $GroupB1
 * @property string|null $GroupB2
 * @property string|null $FormName
 * @property bool|null $ChkSubsaleCD
 * @property bool|null $ChkLoanCD
 * @property string|null $FieldLabels
 * @property int|null $DefPartnerInCharge
 * @property int|null $DefLAInCharge
 * @property int|null $DefClerkInCharge
 * @property int|null $DefProject
 * @property string|null $DefAllowStaffList
 * @property string|null $DefTeam
 * @property string|null $SubFolders
 * @property string $Source
 *
 * @package App\Models
 */
class MatterCode extends Model
{


    protected $table = 'matter code';
    protected $primaryKey = 'MatterCode';
    public $incrementing = false;
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'TurnaroundTime' => 'int',
        'ShowInList' => 'bool',
        'IsDeleted' => 'bool',
        'enteredBy' => 'int',
        'updatedBy' => 'int',
        'ChkSubsaleCD' => 'bool',
        'ChkLoanCD' => 'bool',
        'DefPartnerInCharge' => 'int',
        'DefLAInCharge' => 'int',
        'DefClerkInCharge' => 'int',
        'DefProject' => 'int'
    ];

    protected $dates = [
        'dateEntered',
        'dateUpdated'
    ];

    protected $fillable = [
        'Matter',
        'Industry',
        'Category',
        'WorkNature',
        'DesignFor',
        'Dept',
        'Code',
        'TurnaroundTime',
        'ShowInList',
        'SpaCheckListPresetCode',
        'BillingPresetCode',
        'PresetCodeG1',
        'PresetCodeG2',
        'IsDeleted',
        'Remark',
        'enteredBy',
        'dateEntered',
        'updatedBy',
        'remarks',
        'dateUpdated',
        'PresetCodeG3',
        'GroupC1',
        'GroupC2',
        'GroupC3',
        'GroupC4',
        'GroupC5',
        'GroupC6',
        'GroupL1',
        'GroupL2',
        'GroupL3',
        'GroupL4',
        'GroupB1',
        'GroupB2',
        'FormName',
        'ChkSubsaleCD',
        'ChkLoanCD',
        'FieldLabels',
        'DefPartnerInCharge',
        'DefLAInCharge',
        'DefClerkInCharge',
        'DefProject',
        'DefAllowStaffList',
        'DefTeam',
        'SubFolders',
        'Source'
    ];
}
