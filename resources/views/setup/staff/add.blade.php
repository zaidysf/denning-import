@extends('layout')
@section('content')
    <div class="mb-3">
        <a href="{{ route('setup.index') }}" class="title-link"><h1 class="h5 d-inline align-middle">&larr; Back to Setup</h1></a><br/>
        <h1 class="h3 d-inline align-middle">{{ $label }}</h1>

        @if ($method == 'add')
        <a href="#" id="btn-save" class="btn btn-outline-success float-end">Save</a>
        <a href="{{ url()->previous() }}" class="btn btn-outline-danger float-end" style="margin-right: 5px;">Cancel</a>
        @else
        <a href="#" id="btn-edit" class="btn btn-warning float-end">Edit</a>
        <a href="#" id="btn-save" class="btn btn-outline-success float-end" style="display: none;">Save</a>
        <a href="#" id="btn-cancel" class="btn btn-outline-danger float-end" style="display: none; margin-right: 5px;">Cancel</a>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form
                        action="{{ $formActionURL }}"
                        method="POST"
                        enctype="multipart/form-data"
                        id="form-firm"
                    >
                        @if ($method == 'edit')
                        <input type="hidden" name="_method" value="PUT" />
                        @endif
                        @csrf
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form1" role="button" aria-expanded="false" aria-controls="form1">
                                Name &amp; Address <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form1">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">ID Type</label>
                                    <select name=idType" class="form-control">
                                        @foreach ($idTypes as $v)
                                            <option value="{{ $v->typeCode }}"{{ isset($data->idType) && $data->idType == $v->typeCode ? " selected=selected" : "" }}>{{ $v->label }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">ID No</label>
                                    <input id="idNo" name="idNo" type="text" class="form-control" value="{{ isset($data->idNo) ? $data->idNo : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">ID No (Old)</label>
                                    <input id="idNoOld" name="idNoOld" type="text" class="form-control" value="{{ isset($data->idNoOld) ? $data->idNoOld : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Name</label>
                                    <input id="name" name="name" type="text" class="form-control" value="{{ isset($data->name) ? $data->name : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Initials</label>
                                    <input id="initials" name="initials" type="text" class="form-control" value="{{ isset($data->initials) ? $data->initials : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Title</label>
                                    <select id="title" name="title" class="form-control">
                                        @foreach ($titles as $v)
                                            <option value="{{ $v->ID }}"{{ isset($data->title) && $data->title == $v->Item ? " selected=selected" : "" }}>{{ $v->Item }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="mb-3">
                                    <label class="form-label">Address 1</label>
                                    <input id="add1" name="add1" type="text" class="form-control" value="{{ isset($data->add1) ? $data->add1 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Address 2</label>
                                    <input id="add2" name="add2" type="text" class="form-control" value="{{ isset($data->add2) ? $data->add2 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Address 3</label>
                                    <input id="add3" name="add3" type="text" class="form-control" value="{{ isset($data->add3) ? $data->add3 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">PostCode</label>
                                    <input id="PostCode" name="PostCode" type="text" class="form-control" value="{{ isset($data->PostCode) ? $data->PostCode : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">City</label>
                                    <input id="City" name="City" type="text" class="form-control" value="{{ isset($data->City) ? $data->City : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">State</label>
                                    <input id="State" name="State" type="text" class="form-control" value="{{ isset($data->State) ? $data->State : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Country</label>
                                    <input id="Country" name="Country" type="text" class="form-control" value="{{ isset($data->Country) ? $data->Country : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Citizenship</label>
                                    <input id="citizenship" name="citizenship" type="text" class="form-control" value="{{ isset($data->citizenship) ? $data->citizenship : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Date Birth</label>
                                    <input id="dateBirth" name="dateBirth" type="date" class="form-control" value="{{ isset($data->dateBirth) ? $data->dateBirth : "" }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form2" role="button" aria-expanded="false" aria-controls="form2">
                                Phone &amp; Email <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form2">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">Phone Home</label>
                                    <input id="phoneHome" name="phoneHome" type="text" class="form-control" value="{{ isset($data->phoneHome) ? $data->phoneHome : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Phone Office</label>
                                    <input id="phoneOffice" name="phoneOffice" type="text" class="form-control" value="{{ isset($data->phoneOffice) ? $data->phoneOffice : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Phone Mobile</label>
                                    <input id="phoneMobile" name="phoneMobile" type="text" class="form-control" value="{{ isset($data->phoneMobile) ? $data->phoneMobile : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Phone 1 Ctr</label>
                                    <input id="Phone1ctr" name="Phone1ctr" type="text" class="form-control" value="{{ isset($data->Phone1ctr) ? $data->Phone1ctr : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Phone 1 No</label>
                                    <input id="Phone1No" name="Phone1No" type="text" class="form-control" value="{{ isset($data->Phone1No) ? $data->Phone1No : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Phone 2 Ctr</label>
                                    <input id="Phone2ctr" name="Phone2ctr" type="text" class="form-control" value="{{ isset($data->Phone2ctr) ? $data->Phone2ctr : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Phone 2 No</label>
                                    <input id="Phone2No" name="Phone2No" type="text" class="form-control" value="{{ isset($data->Phone2No) ? $data->Phone2No : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Phone 3 Ctr</label>
                                    <input id="Phone3ctr" name="Phone3ctr" type="text" class="form-control" value="{{ isset($data->Phone3ctr) ? $data->Phone3ctr : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Phone 3 No</label>
                                    <input id="Phone3No" name="Phone3No" type="text" class="form-control" value="{{ isset($data->Phone3No) ? $data->Phone3No : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Fax</label>
                                    <input id="fax" name="fax" type="text" class="form-control" value="{{ isset($data->fax) ? $data->fax : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Fax 1 Ctr</label>
                                    <input id="Fax1ctr" name="Fax1ctr" type="text" class="form-control" value="{{ isset($data->Fax1ctr) ? $data->Fax1ctr : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Fax 1 No</label>
                                    <input id="Fax1No" name="Fax1No" type="text" class="form-control" value="{{ isset($data->Fax1No) ? $data->Fax1No : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Email</label>
                                    <input id="emailAdd" name="emailAdd" type="email" class="form-control" value="{{ isset($data->emailAdd) ? $data->emailAdd : "" }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form3" role="button" aria-expanded="false" aria-controls="form3">
                                Attaching Branch (Default Branch) <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form3">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">Default Branch</label>
                                    <select name="DefaultBranch" class="form-control">
                                        @foreach ($branches as $v)
                                            <option value="{{ $v->FirmCode }}"{{ isset($data->DefaultBranch) && $data->DefaultBranch == $v->FirmCode ? " selected=selected" : "" }}>{{ $v->BranchName }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form4" role="button" aria-expanded="false" aria-controls="form4">
                                Tenure, Commencement and Cessation Date, Status <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form4">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">Tenure Employed</label>
                                    <select id="tenureEmployed" name="tenureEmployed" class="form-control">
                                        <option value="Casual"{{ isset($data->tenureEmployed) && $data->tenureEmployed == 'Casual'  ? " selected=selected" : "" }}>Casual</option>
                                        <option value="Fixed Term"{{ isset($data->tenureEmployed) && $data->tenureEmployed == 'Fixed Term'  ? " selected=selected" : "" }}>Fixed Term</option>
                                        <option value="Permanent"{{ isset($data->tenureEmployed) && $data->tenureEmployed == 'Permanent'  ? " selected=selected" : "" }}>Permanent</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Commencement Date</label>
                                    <input id="dateCommenced" name="dateCommenced" type="date" class="form-control" value="{{ isset($data->dateCommenced) ? $data->dateCommenced : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Cessation Date</label>
                                    <input id="dateCeased" name="dateCeased" type="date" class="form-control" value="{{ isset($data->dateCeased) ? $data->dateCeased : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Status</label>
                                    <select id="status" name="status" class="form-control">
                                        <option value="active"{{ isset($data->status) && $data->status == 'active'  ? " selected=selected" : "" }}>Active</option>
                                        <option value="Resigned"{{ isset($data->status) && $data->status == 'Resigned'  ? " selected=selected" : "" }}>Resigned</option>
                                        <option value="unauthorized"{{ isset($data->status) && $data->status == 'unauthorized'  ? " selected=selected" : "" }}>Unauthorized</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form5" role="button" aria-expanded="false" aria-controls="form5">
                                Position & Department <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form5">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">Position</label>
                                    <input id="positionTitle" name="positionTitle" type="text" class="form-control" value="{{ isset($data->positionTitle) ? $data->positionTitle : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Department</label>
                                    <input id="department" name="department" type="text" class="form-control" value="{{ isset($data->department) ? $data->department : "" }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form6" role="button" aria-expanded="false" aria-controls="form6">
                                Salary & Annual Leave <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form6">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">Monthly Salary</label>
                                    <input id="monthlySalary" name="monthlySalary" type="text" class="form-control" value="{{ isset($data->monthlySalary) ? $data->monthlySalary : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Previous Adjust Date</label>
                                    <input id="prevAdjustDate" name="prevAdjustDate" type="date" class="form-control" value="{{ isset($data->prevAdjustDate) ? $data->prevAdjustDate : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Annual Leave</label>
                                    <input id="annualLeave" name="annualLeave" type="text" class="form-control" value="{{ isset($data->annualLeave) ? $data->annualLeave : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Salary Account</label>
                                    <input id="SalaryAccount" name="SalaryAccount" type="text" class="form-control" value="{{ isset($data->SalaryAccount) ? $data->SalaryAccount : "" }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form7" role="button" aria-expanded="false" aria-controls="form7">
                                Qualification <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form7">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">Qualification</label>
                                    <input id="qualification" name="qualification" type="text" class="form-control" value="{{ isset($data->qualification) ? $data->qualification : "" }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form8" role="button" aria-expanded="false" aria-controls="form8">
                                Tax, Retirement Fund &amp; Social Security No <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form8">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">Tax File No</label>
                                    <input id="taxFileNo" name="taxFileNo" type="text" class="form-control" value="{{ isset($data->taxFileNo) ? $data->taxFileNo : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">IRB Branch</label>
                                    <input id="irbBranch" name="irbBranch" type="text" class="form-control" value="{{ isset($data->irbBranch) ? $data->irbBranch : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Social Security No</label>
                                    <input id="socsoNo" name="socsoNo" type="text" class="form-control" value="{{ isset($data->socsoNo) ? $data->socsoNo : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">EPF No</label>
                                    <input id="epfNo" name="epfNo" type="text" class="form-control" value="{{ isset($data->epfNo) ? $data->epfNo : "" }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form9" role="button" aria-expanded="false" aria-controls="form9">
                                Spouse & Emergency contact <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form9">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">Marital Status</label>
                                    <select id="marital" name="marital" class="form-control">
                                        <option value="Not Married"{{ isset($data->marital) && $data->marital == 'Not Married'  ? " selected=selected" : "" }}>Not Married</option>
                                        <option value="Married"{{ isset($data->marital) && $data->marital == 'Married'  ? " selected=selected" : "" }}>Married</option>
                                        <option value="Divorced"{{ isset($data->marital) && $data->marital == 'Divorced'  ? " selected=selected" : "" }}>Divorced</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Emergency Contact Person</label>
                                    <input id="contactPerson" name="contactPerson" type="text" class="form-control" value="{{ isset($data->contactPerson) ? $data->contactPerson : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Spouse</label>
                                    <input id="spouse" name="spouse" type="text" class="form-control" value="{{ isset($data->spouse) ? $data->spouse : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Number of Children</label>
                                    <select id="childrenNo" name="childrenNo" class="form-control">
                                        @for ($i=0;$i<11;$i++)
                                            <option value="{{ $i }}"{{ isset($data->childrenNo) && $data->childrenNo == $i ? " selected=selected" : "" }}>{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form10" role="button" aria-expanded="false" aria-controls="form10">
                                Bank Account <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form10">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">Bank Account</label>
                                    <input id="remarks" name="remarks" type="text" class="form-control" value="{{ isset($data->remarks) ? $data->remarks : "" }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form11" role="button" aria-expanded="false" aria-controls="form11">
                                Website <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form11">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">Website</label>
                                    <input id="website" name="website" type="text" class="form-control" value="{{ isset($data->website) ? $data->website : "" }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form12" role="button" aria-expanded="false" aria-controls="form12">
                                Mailing List for Festival <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form12">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">Mail List</label>
                                    <input id="mailList" name="mailList" type="text" class="form-control" value="{{ isset($data->mailList) ? $data->mailList : "" }}"/>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
<style>
.title-link h1 {
    color: blue;
}
.text-left {
  text-align: left !important;
}
</style>
@endsection

@section('script')
<script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>>
<script>
    @if ($method != 'add')
    $(document).ready(function(){
        $(".form-control").prop("disabled", true);
    });
    $('#btn-edit').click( function(e) {
        $(".form-control").prop("disabled", false);
        $('#btn-save').toggle();
        $('#btn-edit').toggle();
        $('#btn-cancel').toggle();
    });
    $('#btn-cancel').click( function(e) {
        $(".form-control").prop("disabled", true);
        $('#btn-save').toggle();
        $('#btn-edit').toggle();
        $('#btn-cancel').toggle();
    });
    @endif
    $('#btn-save').click( function(e) {
        e.preventDefault();
        $('form#form-firm').submit();
    });
    $('#select-page').change(function(e){
        if (this.value != null && this.value != "") {
            let searchParams = new URLSearchParams(window.location.search)
            searchParams.delete('per_page');
            searchParams.append('per_page', this.value);
            window.location='{{ route('setup.staff.index') }}?'+searchParams.toString();
        }
    });
    $("#searchForm").submit( function(eventObj) {
        let searchParams = new URLSearchParams(window.location.search)
        searchParams.delete("_token");
        searchParams.delete("key");
        searchParams.delete("value");
        searchParams.forEach(function(value, key) {
            $("<input />").attr("type", "hidden")
                .attr("name", key)
                .attr("value", value)
                .appendTo("#searchForm");
        });
        return true;
    });
</script>
@endsection
