@extends('layout')
@section('content')
    <div class="mb-3">
        <a href="{{ route('setup.index') }}" class="title-link"><h1 class="h5 d-inline align-middle">&larr; Back to Setup</h1></a><br/>
        <h1 class="h3 d-inline align-middle">{{ $label }}</h1>

        @if ($method == 'add')
        <a href="#" id="btn-save" class="btn btn-outline-success float-end">Save</a>
        <a href="{{ url()->previous() }}" class="btn btn-outline-danger float-end" style="margin-right: 5px;">Cancel</a>
        @else
        <a href="#" id="btn-edit" class="btn btn-warning float-end">Edit</a>
        <a href="#" id="btn-save" class="btn btn-outline-success float-end" style="display: none;">Save</a>
        <a href="#" id="btn-cancel" class="btn btn-outline-danger float-end" style="display: none; margin-right: 5px;">Cancel</a>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form
                        action="{{ $formActionURL }}"
                        method="POST"
                        enctype="multipart/form-data"
                        id="form-firm"
                    >
                        @if ($method == 'edit')
                        <input type="hidden" name="_method" value="PUT" />
                        @endif
                        @csrf
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form1" role="button" aria-expanded="false" aria-controls="form1">
                                Name &amp; Address <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form1">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label class="form-label">Name</label>
                                            <input id="Name" name="Name" type="text" class="form-control" value="{{ isset($data->Name) ? $data->Name : "" }}"/>
                                        </div>
                                        <div class="col">
                                            <label class="form-label">Name in other language</label>
                                            <input id="Note 1" name="Note 1" type="text" class="form-control" value="{{ isset($data->{'Note 1'}) ? $data->{'Note 1'} : "" }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Address 1</label>
                                    <input id="Address1" name="Address1" type="text" class="form-control" value="{{ isset($data->Address1) ? $data->Address1 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Address 2</label>
                                    <input id="Address2" name="Address2" type="text" class="form-control" value="{{ isset($data->Address2) ? $data->Address2 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Address 3</label>
                                    <input id="Address3" name="Address3" type="text" class="form-control" value="{{ isset($data->Address3) ? $data->Address3 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">PostCode</label>
                                    <input id="PostCode" name="PostCode" type="text" class="form-control" value="{{ isset($data->PostCode) ? $data->PostCode : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">City</label>
                                    <input id="City" name="City" type="text" class="form-control" value="{{ isset($data->City) ? $data->City : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">State</label>
                                    <input id="State" name="State" type="text" class="form-control" value="{{ isset($data->State) ? $data->State : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Country</label>
                                    <input id="Country" name="Country" type="text" class="form-control" value="{{ isset($data->Country) ? $data->Country : "" }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form2" role="button" aria-expanded="false" aria-controls="form2">
                                Branch <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form2">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">Branch Name</label>
                                    <input id="BranchName" name="BranchName" type="text" class="form-control" value="{{ isset($data->BranchName) ? $data->BranchName : "" }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form3" role="button" aria-expanded="false" aria-controls="form3">
                                Phone, Fax, Email &amp; Website <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form3">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">Phone Office 1</label>
                                    <input id="Phone-Office1" name="Phone-Office1" type="text" class="form-control" value="{{ isset($data->{'Phone-Office1'}) ? $data->{'Phone-Office1'} : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Phone Office 2</label>
                                    <input id="Phone-Office2" name="Phone-Office2" type="text" class="form-control" value="{{ isset($data->{'Phone-Office2'}) ? $data->{'Phone-Office2'} : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Phone Office 3</label>
                                    <input id="Phone-Office3" name="Phone-Office3" type="text" class="form-control" value="{{ isset($data->{'Phone-Office3'}) ? $data->{'Phone-Office3'} : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Fax 1</label>
                                    <input id="Fax1" name="Fax1" type="text" class="form-control" value="{{ isset($data->Fax1) ? $data->Fax1 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Fax 2</label>
                                    <input id="Fax2" name="Fax2" type="text" class="form-control" value="{{ isset($data->Fax2) ? $data->Fax2 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Email</label>
                                    <input id="EmailAddress" name="EmailAddress" type="email" class="form-control" value="{{ isset($data->EmailAddress) ? $data->EmailAddress : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Website</label>
                                    <input id="Website" name="Website" type="text" class="form-control" value="{{ isset($data->Website) ? $data->Website : "" }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form4" role="button" aria-expanded="false" aria-controls="form4">
                                Partners &amp; Qualification <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form4">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">Label</label>
                                    <input id="Partner0" name="Partner0" type="text" class="form-control" value="{{ isset($data->Partner0) ? $data->Partner0 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label class="form-label">Partner1</label>
                                            <input id="Partner1" name="Partner1" type="text" class="form-control" value="{{ isset($data->Partner1) ? $data->Partner1 : "" }}"/>
                                        </div>
                                        <div class="col">
                                            <label class="form-label">Qualification1</label>
                                            <input id="Qualification1" name="Qualification1" type="text" class="form-control" value="{{ isset($data->Qualification1) ? $data->Qualification1 : "" }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label class="form-label">Partner2</label>
                                            <input id="Partner2" name="Partner2" type="text" class="form-control" value="{{ isset($data->Partner2) ? $data->Partner2 : "" }}"/>
                                        </div>
                                        <div class="col">
                                            <label class="form-label">Qualification2</label>
                                            <input id="Qualification2" name="Qualification2" type="text" class="form-control" value="{{ isset($data->Qualification2) ? $data->Qualification1 : "" }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label class="form-label">Partner3</label>
                                            <input id="Partner3" name="Partner3" type="text" class="form-control" value="{{ isset($data->Partner3) ? $data->Partner3 : "" }}"/>
                                        </div>
                                        <div class="col">
                                            <label class="form-label">Qualification3</label>
                                            <input id="Qualification3" name="Qualification3" type="text" class="form-control" value="{{ isset($data->Qualification3) ? $data->Qualification1 : "" }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label class="form-label">Partner4</label>
                                            <input id="Partner4" name="Partner4" type="text" class="form-control" value="{{ isset($data->Partner4) ? $data->Partner4 : "" }}"/>
                                        </div>
                                        <div class="col">
                                            <label class="form-label">Qualification4</label>
                                            <input id="Qualification4" name="Qualification4" type="text" class="form-control" value="{{ isset($data->Qualification4) ? $data->Qualification1 : "" }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label class="form-label">Partner5</label>
                                            <input id="Partner5" name="Partner5" type="text" class="form-control" value="{{ isset($data->Partner5) ? $data->Partner5 : "" }}"/>
                                        </div>
                                        <div class="col">
                                            <label class="form-label">Qualification5</label>
                                            <input id="Qualification5" name="Qualification5" type="text" class="form-control" value="{{ isset($data->Qualification5) ? $data->Qualification1 : "" }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label class="form-label">Partner6</label>
                                            <input id="Partner6" name="Partner6" type="text" class="form-control" value="{{ isset($data->Partner6) ? $data->Partner6 : "" }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label class="form-label">Partner7</label>
                                            <input id="Partner7" name="Partner7" type="text" class="form-control" value="{{ isset($data->Partner7) ? $data->Partner7 : "" }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label class="form-label">Partner8</label>
                                            <input id="Partner8" name="Partner8" type="text" class="form-control" value="{{ isset($data->Partner8) ? $data->Partner8 : "" }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label class="form-label">Partner9</label>
                                            <input id="Partner9" name="Partner9" type="text" class="form-control" value="{{ isset($data->Partner9) ? $data->Partner9 : "" }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label class="form-label">Partner10</label>
                                            <input id="Partner10" name="Partner10" type="text" class="form-control" value="{{ isset($data->Partner10) ? $data->Partner10 : "" }}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form5" role="button" aria-expanded="false" aria-controls="form5">
                                LA / Associates <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form5">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">Label</label>
                                    <input id="LA0" name="LA0" type="text" class="form-control" value="{{ isset($data->LA0) ? $data->LA0 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">LA1</label>
                                    <input id="LA1" name="LA1" type="text" class="form-control" value="{{ isset($data->LA1) ? $data->LA1 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">LA2</label>
                                    <input id="LA2" name="LA2" type="text" class="form-control" value="{{ isset($data->LA2) ? $data->LA2 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">LA3</label>
                                    <input id="LA3" name="LA3" type="text" class="form-control" value="{{ isset($data->LA3) ? $data->LA3 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">LA4</label>
                                    <input id="LA4" name="LA4" type="text" class="form-control" value="{{ isset($data->LA4) ? $data->LA4 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">LA5</label>
                                    <input id="LA5" name="LA5" type="text" class="form-control" value="{{ isset($data->LA5) ? $data->LA5 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">LA6</label>
                                    <input id="LA6" name="LA6" type="text" class="form-control" value="{{ isset($data->LA6) ? $data->LA6 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">LA7</label>
                                    <input id="LA7" name="LA7" type="text" class="form-control" value="{{ isset($data->LA7) ? $data->LA7 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">LA8</label>
                                    <input id="LA8" name="LA8" type="text" class="form-control" value="{{ isset($data->LA8) ? $data->LA8 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">LA9</label>
                                    <input id="LA9" name="LA9" type="text" class="form-control" value="{{ isset($data->LA9) ? $data->LA9 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">LA10</label>
                                    <input id="LA10" name="LA10" type="text" class="form-control" value="{{ isset($data->LA10) ? $data->LA10 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">LA11</label>
                                    <input id="LA11" name="LA11" type="text" class="form-control" value="{{ isset($data->LA11) ? $data->LA11 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">LA12</label>
                                    <input id="LA12" name="LA12" type="text" class="form-control" value="{{ isset($data->LA12) ? $data->LA12 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">LA13</label>
                                    <input id="LA13" name="LA13" type="text" class="form-control" value="{{ isset($data->LA13) ? $data->LA13 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">LA14</label>
                                    <input id="LA14" name="LA14" type="text" class="form-control" value="{{ isset($data->LA14) ? $data->LA14 : "" }}"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">LA15</label>
                                    <input id="LA15" name="LA15" type="text" class="form-control" value="{{ isset($data->LA15) ? $data->LA15 : "" }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form6" role="button" aria-expanded="false" aria-controls="form6">
                                Working Hours <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form6">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">Work Hour Start</label>
                                    <input id="WorkHourStart" name="WorkHourStart" type="time" class="form-control" value="{{ isset($data->WorkHourStart) ? date('H:i:s', strtotime($data->WorkHourStart)) : "" }}"/>
                                    <small>(e.g 08:00 AM)</small>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Work Hour End</label>
                                    <input id="WorkHourEnd" name="WorkHourEnd" type="time" class="form-control" value="{{ isset($data->WorkHourEnd) ? date('H:i:s', strtotime($data->WorkHourEnd)) : "" }}"/>
                                    <small>(e.g 05:30 PM)</small>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Lunch Break Start</label>
                                    <input id="BreakStart" name="BreakStart" type="time" class="form-control" value="{{ isset($data->BreakStart) ? date('H:i:s', strtotime($data->BreakStart)) : "" }}"/>
                                    <small>(e.g 01:00 PM)</small>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Lunch Break End</label>
                                    <input id="BreakEnd" name="BreakEnd" type="time" class="form-control" value="{{ isset($data->BreakEnd) ? date('H:i:s', strtotime($data->BreakEnd)) : "" }}"/>
                                    <small>(e.g 02:00 PM)</small>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-grid">
                            <a class="btn btn-primary text-left" data-bs-toggle="collapse" href="#form8" role="button" aria-expanded="false" aria-controls="form8">
                                Welcome &amp; footer message <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form8">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <label class="form-label">Welcome message</label>
                                    <textarea id="Message" name="Message" class="form-control">{{ isset($data->Message) ? $data->Message : "" }}</textarea>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Quotation message</label>
                                    <textarea id="QuotationFooter" name="QuotationFooter" class="form-control">{{ isset($data->QuotationFooter) ? $data->QuotationFooter : "" }}</textarea>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Invoice message</label>
                                    <textarea id="BillFooter" name="BillFooter" class="form-control">{{ isset($data->BillFooter) ? $data->BillFooter : "" }}</textarea>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Receipt message</label>
                                    <textarea id="ReceiptFooter" name="ReceiptFooter" class="form-control">{{ isset($data->ReceiptFooter) ? $data->ReceiptFooter : "" }}</textarea>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="mb-3">
                            <a class="btn btn-primary" data-bs-toggle="collapse" href="#form7" role="button" aria-expanded="false" aria-controls="form7">
                                Company Logo <i data-feather="chevrons-down"></i>
                            </a>
                        </div>
                        <div class="collapse" id="form7">
                            <div class="card card-body">
                                <div class="mb-3">
                                    <input id="companyLogo" name="companyLogo" type="text" class="form-control" value="{{ isset($data->companyLogo) }}"/>
                                </div>
                            </div>
                        </div> --}}
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
<style>
.title-link h1 {
    color: blue;
}
.text-left {
  text-align: left !important;
}
/* .zandgar__wizard {
    display: block;
    height: 100%;
    position: relative;
}

.zandgar__wizard .zandgar__step {
    display: none;
}
.zandgar__wizard .zandgar__step.zandgar__step__active {
    display: block;
}

.col-img {
    background: scroll center url('https://images.unsplash.com/photo-1497366754035-f200968a6e72?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80');
    background-size: cover;
    min-height: 100vh;
} */
</style>
@endsection

@section('script')
<script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>>
<script>
    @if ($method != 'add')
    $(document).ready(function(){
        $(".form-control").prop("disabled", true);
    });
    $('#btn-edit').click( function(e) {
        $(".form-control").prop("disabled", false);
        $('#btn-save').toggle();
        $('#btn-edit').toggle();
        $('#btn-cancel').toggle();
    });
    $('#btn-cancel').click( function(e) {
        $(".form-control").prop("disabled", true);
        $('#btn-save').toggle();
        $('#btn-edit').toggle();
        $('#btn-cancel').toggle();
    });
    @endif
    $('#btn-save').click( function(e) {
        e.preventDefault();
        $('form#form-firm').submit();
    });
    $('#select-page').change(function(e){
        if (this.value != null && this.value != "") {
            let searchParams = new URLSearchParams(window.location.search)
            searchParams.delete('per_page');
            searchParams.append('per_page', this.value);
            window.location='{{ route('setup.staff.index') }}?'+searchParams.toString();
        }
    });
    $("#searchForm").submit( function(eventObj) {
        let searchParams = new URLSearchParams(window.location.search)
        searchParams.delete("_token");
        searchParams.delete("key");
        searchParams.delete("value");
        searchParams.forEach(function(value, key) {
            $("<input />").attr("type", "hidden")
                .attr("name", key)
                .attr("value", value)
                .appendTo("#searchForm");
        });
        return true;
    });
</script>
@endsection
