<?php

namespace App\Http\Controllers;

use App\Helpers\LogHelper;
use App\Imports\FileImportCount;
use App\Jobs\DoImport;
use App\Models\BatchImports;
use App\Models\Customer;
use App\Models\File;
use App\Models\Filestatus;
use App\Models\Idtype;
use App\Models\Lawyer;
use App\Models\MatterCode;
use App\Models\ProgramOwner;
use App\Models\Project;
use App\Models\Property;
use App\Models\Staff;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;

class ImportController extends Controller
{
    protected $selectData;

    public function __construct()
    {
        $this->selectData = [
            'branches' => ProgramOwner::all(),
            'fileStatus' => Filestatus::all(),
            'idTypes' => Idtype::all(),
        ];
    }

    public function index($staffId)
    {
        $return = $this->selectData;
        $return['staff_id'] = $staffId;
        $return['partner'] = Staff::where(function ($q) {
            $q->where('status', 'active')->orWhere('status', 'Active');
        })->where('positionTitle', 'Partner')->get();
        $return['la'] = Staff::where(function ($q) {
            $q->where('status', 'active')->orWhere('status', 'Active');
        })->where('positionTitle', 'Legal Assistant')->get();
        $return['clerk'] = Staff::where(function ($q) {
            $q->where('status', 'active')->orWhere('status', 'Active');
        })
        ->whereNotIn('positionTitle', ['Partner', 'Legal Assistant'])
        ->get();
        return view('import.index', $return);
    }

    public function list()
    {
        $data = BatchImports::all();
        return view('import.list', compact('data'));
    }

    public function import(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'staff_id' => 'required', // entered by & updated by
            'idType' => 'required', // for all customer field in file table (except C0)
            'fileNo1_prefix' => 'digits:4', // if this is available, so use this instead generate from latest
            // 'FileNo2' => 'string',
            'Licensee' => 'required', // branch
            'MatterCode' => 'required',
            // 'Checklist1' => 'numeric', // get from matterCode table (if there's no available, so use user input)
            // 'PBilling' => 'numeric', // get from matterCode table (if there's no available, so use user input)
            // 'DateOpenFile' => 'required|date',
            // 'Turnaround' => 'numeric', // get from matterCode table (if there's no available, so use user input)
            'FileStatus' => 'string', // by default : 1 (Active), get from FileStatus table by name and get id
            // 'DateCloseFile' => 'date',
            'PartnerInCharge' => 'required|numeric',
            // 'LAInCharge' => 'numeric',
            'ClerkInCharge' => 'required|numeric',
            // 'Team' => 'string',
            'C0' => 'required|numeric',
            // 'P1' => 'required|numeric',
            // 'FileLoc' => 'string',
            // 'PocketFile' => 'string',
            // 'Box' => 'string',
            'RelatedFile' => 'exists:file,FileNo1',
            // 'remarks' => 'string',
            'exportOptions' => 'required',
            'file' => 'required_if:exportOptions,1|mimes:csv,xls,xlsx',
            'noOfMatters' => 'required_if:exportOptions,2',
            'startFrom' => 'required_if:exportOptions,2',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        if ($request->get('exportOptions') == 2) {
            if ($this->generateFile($request->all())) {
                $lastFileNo1 = str_replace('-', '', File::orderBy('FileNo1', 'desc')->first()->FileNo1);
                $reqStart = str_replace('-', '', $request->get('startFrom'));
                if (is_numeric($reqStart) && $lastFileNo1 < $reqStart) {
                    return redirect()->back()->with('success', 'Files generated');
                } else {
                    return redirect()->back()->with('errors', $validator->errors());
                }
            } else {
                return redirect()->back()->with('errors', $validator->errors());
            }
        }

        // $count = 0;
        $import = new FileImportCount;
        Excel::import($import, $request->file('file'));
        $totalRows = $import->getRowCount() - 1;
        // $excel = (new FastExcel)->import($request->file('file'), function () use ($count) {
        //     $count++;
        // });

        // $file = $request->file('file');
        // $extension = strtolower($file->getClientOriginalExtension());
        // $safeName = 'imports-'.time().'.'.$extension;
        // $file->move(public_path('/uploads/imports'), $safeName);
        // $filename = '/uploads/imports/'.$safeName;

        // $batchImportNo = bcrypt($request->get('staff_id').'-'.date('Y-m-d H:i:s'));
        $batch = BatchImports::create([
            // 'staff_id' => $request->get('staff_id'),
            'staff_id' => \Auth::user()->id,
            'matter' => $request->get('MatterCode'),
            'c0' => $request->get('C0'),
            'file_path' => '',
            'count' => $totalRows,
            'status' => 1,
        ]);

        $request->merge(['batch_id' => $batch->id]);

        // dispatch(new DoImport($request));
        $this->importQueue($request);

        return redirect()->back()->with('success', 'Data importing is in progress now');
    }

    public function generateFile($request)
    {
        // $fileFillable = (new File)->getFillable();
        $lastBI = 1;
        $bi = BatchImports::orderBy('BatchNo', 'desc')->first();
        if ($bi) {
            $lastBI = $bi->BatchNo + 1;
        }
        $lastFileNo1 = str_replace('-', '', $request['startFrom']);
        $sum = $request['noOfMatters'];
        $staffId = $request['staff_id'];
        for ($i=0; $i<$sum; $i++) {
            $request['FileNo1'] = substr($lastFileNo1, 0, 4).'-'.substr($lastFileNo1, 4, 4);
            $request['BatchNo'] = $lastBI;
            $request['DateOpenFile'] = date('Y-m-d');
            File::create($request);
            $lastFileNo1++;
        }
        $batch = BatchImports::create([
            'BatchNo' => $lastBI,
            'staff_id' => $staffId,
            'matter' => $request['MatterCode'],
            'c0' => $request['C0'],
            'file_path' => '',
            'count' => $sum,
            'status' => 1,
        ]);
        return true;
    }

    public function importQueue(Request $request)
    {
        $fileFillable = (new File)->getFillable();

        $fileFields = array_map('strtolower', $fileFillable);
        $fileCustomerFields = preg_filter('/^/', 'c', range(0, 30));
        $filePropertyFields = preg_filter('/^/', 'p', range(1, 5));

        $data = (new FastExcel)
        ->import(
            $request->file('file'),
            function ($importedData) use ($request, $fileFields, $fileCustomerFields, $filePropertyFields) {
                foreach ($importedData as $k1 => $v1) {
                    if ($importedData[$k1] instanceof DateTime) {
                        $importedData[$k1] = $importedData[$k1]->format('Y-m-d H:i:s');
                    } elseif (is_numeric($importedData[$k1])) {
                        $importedData[$k1] = (float) $importedData[$k1];
                    } elseif (empty($v1)) {
                        unset($importedData[$k1]);
                    }
                }
                $insertData = $request->all();
                $skipHeader = [];
                $iKeys = array_keys($importedData);
                $lowerKeys = array_map('strtolower', array_keys($importedData));
                $lowerImportedData = array_change_key_case($importedData);
                foreach ($iKeys as $v) {
                    $cols = explode('_', $v);
                    $mainCol = $cols[0];

                    if (in_array($mainCol, $skipHeader)) {
                        continue;
                    }

                    if (!in_array(strtolower($mainCol), $fileFields)) {
                        continue;
                    }

                    if (in_array(strtolower($mainCol), $fileCustomerFields)) {
                        if (!in_array(strtolower($mainCol).'_name', $lowerKeys) &&
                            !in_array(strtolower($mainCol).'_idno', $lowerKeys)
                        ) {
                            return LogHelper::makeException('Every C / customer must have Name or idNo', 401);
                        }

                        $whereCust = [];
                        if (in_array(strtolower($mainCol).'_idno', $lowerKeys)) {
                            $whereCust['idType'] = $request->get('idType');
                            $whereCust['idNo'] = $lowerImportedData[strtolower($mainCol).'_idno'];
                            if (in_array(strtolower($mainCol).'_name', $lowerKeys)) {
                                $initial = $this->generateInitial($lowerImportedData[strtolower($mainCol).'_name']);
                                $whereCust['name'] = $lowerImportedData[strtolower($mainCol).'_name'];
                                $whereCust['initials'] = $initial;
                            }
                        } else {
                            $idNoTemp = $this->generateInitial($lowerImportedData[strtolower($mainCol).'_name']);
                            $count = Customer::whereRaw("LOWER(idNo) LIKE '".$idNoTemp."%'")->count();
                            if ($count > 2) {
                                $whereCust['idNo'] = $idNoTemp.''.$count;
                            } elseif ($count > 1) {
                                $whereCust['idNo'] = $idNoTemp.'1';
                            } else {
                                $whereCust['idNo'] = $idNoTemp;
                            }
                            $whereCust['idType'] = $request->get('idType');
                            $whereCust['name'] = $lowerImportedData[strtolower($mainCol).'_name'];
                            $whereCust['initials'] = $idNoTemp;
                        }
                        $cPrefixesFields = array_intersect_key(
                            $importedData,
                            array_flip(
                                preg_grep("/^".$mainCol."_/", array_keys($importedData))
                            )
                        );
                        $insertData[$mainCol] = Customer::updateOrCreate(
                            $whereCust,
                            $cPrefixesFields
                        )->customerCode;

                        $skipHeader[] = $mainCol;
                    } elseif (in_array(strtolower($mainCol), $filePropertyFields)) {
                        if (!in_array(strtolower($mainCol).'_propertyid', $lowerKeys)) {
                            return LogHelper::makeException('Every P / property must have PropertyID', 401);
                        }

                        $whereProp = [];
                        $whereProp['PropertyID'] = $lowerImportedData[strtolower($mainCol).'_propertyid'];
                        if ($request->get('P1') != null) {
                            $whereProp['ProjectCode'] = $request->get('P1');
                            $whereProp['ProjectName'] = Project::find($request->get('P1'))->projectName;
                        }

                        $pPrefixesFields = array_intersect_key(
                            $importedData,
                            array_flip(
                                preg_grep("/^".$mainCol."_/", array_keys($importedData))
                            )
                        );
                        $insertData[$mainCol] = Property::updateOrCreate(
                            $whereProp,
                            $pPrefixesFields
                        )->PropertyCode;

                        $skipHeader[] = $mainCol;
                    } else {
                        $insertData[$mainCol] = $importedData[$v];
                    }
                }

                // $check = File::where('FileNo1', 'LIKE', $request->get('fileNo1_prefix').'-%')
                //     ->orderBy('FileNo1', 'desc')->first();
                // if (!$check) {
                //     $insertData['FileNo1'] = $request->get('fileNo1_prefix').'-0001';
                // } else {
                //     $lastNo = (int) explode("-", $check->FileNo1)[1];
                //     $fileno1 = $request->get('fileNo1_prefix').'-'.sprintf('%04s', $lastNo+1);
                //     $insertData['FileNo1'] = $fileno1;
                // }

                $check = File::orderBy('FileNo1', 'desc')->first();
                $lastFileNo1 = str_replace('-', '', $check->FileNo1);
                $lastFileNo1++;
                $insertData['FileNo1'] = substr($lastFileNo1, 0, 4).'-'.substr($lastFileNo1, 4, 4);

                if (isset($lowerImportedData['fileno2'])) {
                    $insertData['FileNo2'] = $lowerImportedData['fileno2'];
                } elseif ($request->has('FileNo2')) {
                    $insertData['FileNo2'] = $request->get('FileNo2');
                } else {
                    $insertData['FileNo2'] = '';
                }

                // Import Batch No
                $insertData['BatchNo'] = $request->get('batch_id');
                // if user not enter it and let the system generate
                // will get from the last one, and increment it (e.g 10000)

                $insertData['C0'] = $request->get('C0');
                $matterData = array_filter(MatterCode::find($request->get('MatterCode'))->toArray());
                $insertData['Checklist1'] = isset(
                    $matterData['SpaCheckListPresetCode']
                ) ? $matterData['SpaCheckListPresetCode'] : 0;
                $insertData['PBilling'] = isset(
                    $matterData['BillingPresetCode']
                ) ? $matterData['BillingPresetCode'] : 0;
                $insertData['Turnaround'] = isset(
                    $matterData['TurnaroundTime']
                ) ? $matterData['TurnaroundTime'] : 0;

                $insertData['P1'] = $request->get('p1');
                $insertData['enteredBy'] = $request->get('staff_id');
                $insertData['updatedBy'] = $request->get('staff_id');
                $insertData['DateOpenFile'] = date('Y-m-d H:i:s');
                $insertData['dateEntered'] = date('Y-m-d H:i:s');
                $insertData['dateUpdated'] = date('Y-m-d H:i:s');
                File::create($insertData);

                BatchImports::find($request->get('batch_id'))->update([
                    'BatchNo' => $request->get('batch_id'),
                    'status' => 1
                ]);
            }
        );
        return response()->json(['success'=>'file imported'], 200);
    }

    /**
     * Generate initials from a name
     *
     * @param string $name
     * @return string
     */
    protected function generateInitial(string $name) : string
    {
        $words = explode(' ', $name);
        if (count($words) >= 2) {
            return strtolower(substr($words[0], 0, 1) . substr(end($words), 0, 1));
        }
        return $this->makeInitialsFromSingleWord($name);
    }

    /**
     * Make initials from a word with no spaces
     *
     * @param string $name
     * @return string
     */
    protected function makeInitialsFromSingleWord(string $name) : string
    {
        preg_match_all('#([A-Z]+)#', $name, $capitals);
        if (count($capitals[1]) >= 2) {
            return substr(implode('', $capitals[1]), 0, 2);
        }
        return strtolower(substr($name, 0, 2));
    }
}
