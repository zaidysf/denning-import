<?php

namespace App\Exports;

use App\Models\File;
use App\Models\Litigationassignedstage;
use App\Models\Spaassignedchecklist;
// use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class FileExport implements FromView
{
    use Exportable;

    private $finalData = [];

    public function __construct($finalData)
    {
        $this->finalData = $finalData;
    }

    public function view(): View
    {
        return view('export.file', [
            'data' => $this->finalData
        ]);
    }
}
