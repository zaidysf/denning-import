<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * Class ProgramOwner
 *
 * @property string $FirmCode
 * @property int $DefaultFirm
 * @property string|null $Qualification1
 * @property string|null $Qualification2
 * @property string|null $Qualification3
 * @property string|null $Qualification4
 * @property string|null $Qualification5
 * @property string|null $Message
 * @property string|null $BillFooter
 * @property string|null $QuotationFooter
 * @property string|null $ReceiptFooter
 * @property int|null $IrdBranch
 * @property int|null $PTG
 * @property string|null $Name
 * @property string|null $BranchName
 * @property string|null $Initials
 * @property string|null $Description1
 * @property string|null $Description2
 * @property string|null $Address1
 * @property string|null $Address2
 * @property string|null $Address3
 * @property string|null $PostCode
 * @property string $City
 * @property string|null $State
 * @property string|null $Country
 * @property string|null $Phone-Office1
 * @property string|null $Phone-Office2
 * @property string|null $Phone-Office3
 * @property string|null $Fax1
 * @property string|null $Fax2
 * @property string|null $EmailAddress
 * @property string|null $Website
 * @property string|null $Partner0
 * @property string|null $Partner1
 * @property string|null $Partner2
 * @property string|null $Partner3
 * @property string|null $Partner4
 * @property string|null $Partner5
 * @property string|null $Partner6
 * @property string|null $Partner7
 * @property string|null $Partner8
 * @property string|null $Partner9
 * @property string|null $Partner10
 * @property string|null $LA0
 * @property string|null $LA1
 * @property string|null $LA2
 * @property string|null $LA3
 * @property string|null $LA4
 * @property string|null $LA5
 * @property string|null $LA6
 * @property string|null $LA7
 * @property string|null $LA8
 * @property string|null $LA9
 * @property string|null $LA10
 * @property string|null $LA11
 * @property string|null $LA12
 * @property string|null $LA13
 * @property string|null $LA14
 * @property string|null $LA15
 * @property string|null $LA16
 * @property Carbon|null $LastUpdated
 * @property string|null $Clerk
 * @property string|null $Note 1
 * @property string $Header1
 * @property string $Footer1
 * @property string $Footer2
 * @property string|null $LawFirmCode
 * @property string $GSTRegNo
 * @property Carbon|null $CommenceDate
 * @property int $Letterhead
 * @property bool $GSTRegistered
 * @property Carbon|null $GSTRegisteredDate
 * @property int $GSTTaxablePeriod
 * @property Carbon|null $GSTDateEffective
 * @property Carbon|null $GSTDateNextFiling
 * @property Carbon|null $GSTDateLastFiling
 * @property bool $SSTRegistered
 * @property Carbon|null $SSTDateEffective
 * @property string $SSTRegNo
 * @property float $ServiceTax
 * @property float $SaleTax
 * @property string $DefaultCurrencySymbol
 * @property string $DefaultTaxLabel
 * @property Carbon|null $dateEntered
 * @property int|null $enteredBy
 * @property Carbon|null $dateUpdated
 * @property int|null $updatedBy
 * @property string|null $remarks
 * @property string|null $AllStaffList
 * @property Carbon|null $WorkHourStart
 * @property Carbon|null $WorkHourEnd
 * @property Carbon|null $BreakStart
 * @property Carbon|null $BreakEnd
 * @property string $companyLogo
 * @property string|null $Phone1Ctr
 * @property string|null $Phone1No
 * @property string|null $Phone2Ctr
 * @property string|null $Phone2No
 * @property string|null $Phone3Ctr
 * @property string|null $Phone3No
 * @property string|null $Fax1Ctr
 * @property string|null $Fax1No
 * @property string|null $Fax2Ctr
 * @property string|null $Fax2No
 *
 * @package App\Models
 */
class ProgramOwner extends Model
{

    protected $table = 'program owner';
    protected $primaryKey = 'FirmCode';
    public $incrementing = false;
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'DefaultFirm' => 'int',
        'IrdBranch' => 'int',
        'PTG' => 'int',
        'Letterhead' => 'int',
        'GSTRegistered' => 'bool',
        'GSTTaxablePeriod' => 'int',
        'SSTRegistered' => 'bool',
        'ServiceTax' => 'float',
        'SaleTax' => 'float',
        'enteredBy' => 'int',
        'updatedBy' => 'int'
    ];

    protected $dates = [
        'LastUpdated',
        'CommenceDate',
        'GSTRegisteredDate',
        'GSTDateEffective',
        'GSTDateNextFiling',
        'GSTDateLastFiling',
        'SSTDateEffective',
        'dateEntered',
        'dateUpdated',
        'WorkHourStart',
        'WorkHourEnd',
        'BreakStart',
        'BreakEnd'
    ];

    protected $fillable = [
        'FirmCode',
        'DefaultFirm',
        'Qualification1',
        'Qualification2',
        'Qualification3',
        'Qualification4',
        'Qualification5',
        'Message',
        'BillFooter',
        'QuotationFooter',
        'ReceiptFooter',
        'IrdBranch',
        'PTG',
        'Name',
        'BranchName',
        'Initials',
        'Description1',
        'Description2',
        'Address1',
        'Address2',
        'Address3',
        'PostCode',
        'City',
        'State',
        'Country',
        'Phone-Office1',
        'Phone-Office2',
        'Phone-Office3',
        'Fax1',
        'Fax2',
        'EmailAddress',
        'Website',
        'Partner0',
        'Partner1',
        'Partner2',
        'Partner3',
        'Partner4',
        'Partner5',
        'Partner6',
        'Partner7',
        'Partner8',
        'Partner9',
        'Partner10',
        'LA0',
        'LA1',
        'LA2',
        'LA3',
        'LA4',
        'LA5',
        'LA6',
        'LA7',
        'LA8',
        'LA9',
        'LA10',
        'LA11',
        'LA12',
        'LA13',
        'LA14',
        'LA15',
        'LA16',
        'LastUpdated',
        'Clerk',
        'Note 1',
        'Header1',
        'Footer1',
        'Footer2',
        'LawFirmCode',
        'GSTRegNo',
        'CommenceDate',
        'Letterhead',
        'GSTRegistered',
        'GSTRegisteredDate',
        'GSTTaxablePeriod',
        'GSTDateEffective',
        'GSTDateNextFiling',
        'GSTDateLastFiling',
        'SSTRegistered',
        'SSTDateEffective',
        'SSTRegNo',
        'ServiceTax',
        'SaleTax',
        'DefaultCurrencySymbol',
        'DefaultTaxLabel',
        'dateEntered',
        'enteredBy',
        'dateUpdated',
        'updatedBy',
        'remarks',
        'AllStaffList',
        'WorkHourStart',
        'WorkHourEnd',
        'BreakStart',
        'BreakEnd',
        'companyLogo',
        'Phone1Ctr',
        'Phone1No',
        'Phone2Ctr',
        'Phone2No',
        'Phone3Ctr',
        'Phone3No',
        'Fax1Ctr',
        'Fax1No',
        'Fax2Ctr',
        'Fax2No'
    ];

    public function sst()
    {
        return $this->belongsTo('App\Models\Ssttaxcode', 'SSTRegistered', 'Ranking');
    }
}
