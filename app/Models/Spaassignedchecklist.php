<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Spaassignedchecklist
 *
 * @property string $FileNo
 * @property string $TaskCode
 * @property int $Ranking
 * @property int|null $AssignedTo
 * @property Carbon|null $StartDate
 * @property Carbon|null $DueDate
 * @property int $TimeEstimate
 * @property bool $Done
 * @property Carbon|null $CompletionDate
 * @property string|null $Remark
 * @property int $ReferToItem
 * @property bool $NotRequired
 * @property Carbon|null $dateEntered
 * @property int|null $enteredBy
 * @property Carbon|null $dateUpdated
 * @property int|null $updatedBy
 * @property string|null $remarks
 *
 * @package App\Models
 */
class Spaassignedchecklist extends Model
{
    protected $table = 'spaassignedchecklist';
    public $incrementing = false;
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'Ranking' => 'int',
        'AssignedTo' => 'int',
        'TimeEstimate' => 'int',
        'Done' => 'bool',
        'ReferToItem' => 'int',
        'NotRequired' => 'bool',
        'enteredBy' => 'int',
        'updatedBy' => 'int'
    ];

    protected $dates = [
        'StartDate',
        'DueDate',
        'CompletionDate',
        'dateEntered',
        'dateUpdated'
    ];

    protected $fillable = [
        'Ranking',
        'AssignedTo',
        'StartDate',
        'DueDate',
        'TimeEstimate',
        'Done',
        'CompletionDate',
        'Remark',
        'ReferToItem',
        'NotRequired',
        'dateEntered',
        'enteredBy',
        'dateUpdated',
        'updatedBy',
        'remarks'
    ];

    public function checklistRef()
    {
        return $this->belongsTo('App\Models\Spachecklist', 'TaskCode', 'TaskCode');
    }

    public function getCodeNameAttribute()
    {
        return $this->FileNo;
    }
}
