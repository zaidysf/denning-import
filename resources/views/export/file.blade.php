<table>
    <thead>
        <tr>
            <th>File No 1</th>
            <th>File No 2</th>
            <th>Date File Open</th>
            <th>Status</th>
            <th>LA In Charge</th>
            <th>Clerk In Charge</th>
            <th>Matter</th>
            <th>Client Name</th>
            <th>Plaintiff1</th>
            <th>Plaintiff2</th>
            <th>Plaintiff3</th>
            <th>Plaintiff4</th>
            <th>Plaintiff5</th>
            <th>Defendant Name</th>
            <th>Defendant Name 2</th>
            <th>Defendant Name 3</th>
            <th>Defendant Name 4</th>
            <th>Defendant Name 5</th>
            <th>Defendant Solicitor</th>
            <th>Accident Date</th>
            <th>JPJ Report</th>
            <th>Sec 96 Notice Date</th>
            <th>Sec 96 Insurer Confirmation Date</th>
            <th>Insurance Company</th>
            <th>Plaintiff Police Report Request Date</th>
            <th>Plaintiff Police Report Received Date</th>
            <th>Defendant Police Report Request Date</th>
            <th>Defendant Police Report Received Date</th>
            <th>Police Photographs Request Date</th>
            <th>Police Photographs Received Date</th>
            <th>Police Sketch plan Request Date</th>
            <th>Police Sketch plan Received Date</th>
            <th>Keputusan Kes Request Date</th>
            <th>Keputusan Kes Received Date</th>
            <th>Other Police Records Request Date</th>
            <th>Other Police Records Received Date</th>
            <th>IMR (Emergency) Request Date</th>
            <th>IMR (Emergency) Received Date</th>
            <th>IMR (Ortho) Request Date</th>
            <th>IMR (Ortho) Received Date</th>
            <th>IMR (Maxi/Optho/Dental) Request Date</th>
            <th>IMR (Maxi/Optho/Dental) Received Date</th>
            <th>IMR (Neuro/Surgery) Request Date</th>
            <th>IMR (Neuro/Surgery) Received Date</th>
            <th>OG Dr Prosthetic Request Date</th>
            <th>OG Dr Prosthetic Received Date</th>
            <th>OG SR (Ortho) Request Date</th>
            <th>OG SR (Ortho) Received Date</th>
            <th>OG SR (Neuro) Request Date</th>
            <th>OG SR (Neuro) Received Date</th>
            @php $checklist = []; @endphp
            @foreach ($data as $v)
                @foreach ($v->checklist_ref as $x)
                    @php
                        $completionDate = "";
                        if (!empty($x->CompletionDate)) {
                            $completionDate = $x->CompletionDate->format('Y-m-d');
                        }
                        $checklist[$x->checklistRef->code_name]['Files'][$v->FileNo1] = $completionDate;
                        $checklist[$x->checklistRef->code_name]['Description'] = $x->checklistRef->Description;
                    @endphp
                    <th>{{ $x->checklistRef->code_name }} - {{ $x->checklistRef->Description }}</th>
                @endforeach
            @endforeach
            {{-- @dd($checklist) --}}
        </tr>
    </thead>
    <tbody>
    @foreach($data as $v)
        <tr>
            <td>{{ $v->FileNo1 }}</td>
            <td>{{ $v->FileNo2 }}</td>
            <td>{{ $v->DateOpenFile }}</td>
            <td>{{ $v->fileStatusRef->status }}</td>
            <td>{{ $v->lAInCharge ? $v->lAInCharge->name : '' }}</td>
            <td>{{ $v->clerkInCharge ? $v->clerkInCharge->name : '' }}</td>
            <td>{{ $v->MatterCode }}</td>
            <td>{{ $v->cust0 ? $v->cust0->name : '' }}</td>
            <td>{{ $v->cust1 ? $v->cust1->name : '' }}</td>
            <td>{{ $v->cust2 ? $v->cust2->name : '' }}</td>
            <td>{{ $v->cust3 ? $v->cust3->name : '' }}</td>
            <td>{{ $v->cust4 ? $v->cust4->name : '' }}</td>
            <td>{{ $v->cust5 ? $v->cust5->name : '' }}</td>
            <td>{{ $v->cust6 ? $v->cust6->name : '' }}</td>
            <td>{{ $v->cust7 ? $v->cust7->name : '' }}</td>
            <td>{{ $v->cust8 ? $v->cust8->name : '' }}</td>
            <td>{{ $v->cust9 ? $v->cust9->name : '' }}</td>
            <td>{{ $v->cust10 ? $v->cust10->name : '' }}</td>
            <td>{{ $v->lawyer2 ? $v->lawyer2->name : '' }}</td>
            <td>{{ $v->Date21 }}</td>
            <td>{{ $v->Date4 }}</td>
            <td>{{ $v->Date3 }}</td>
            <td>{{ $v->Date4 }}</td>
            <td>{{ $v->cust18 ? $v->cust18->name : '' }}</td>
            <td>{{ $v->Date5 }}</td>
            <td>{{ $v->Date6 }}</td>
            <td>{{ $v->Date7 }}</td>
            <td>{{ $v->Date8 }}</td>
            <td>{{ $v->Date9 }}</td>
            <td>{{ $v->Date10 }}</td>
            <td>{{ $v->Date11 }}</td>
            <td>{{ $v->Date12 }}</td>
            <td>{{ $v->Date13 }}</td>
            <td>{{ $v->Date14 }}</td>
            <td>{{ $v->Date27 }}</td>
            <td>{{ $v->Date28 }}</td>
            <td>{{ $v->Date29 }}</td>
            <td>{{ $v->Date30 }}</td>
            <td>{{ $v->Date39 }}</td>
            <td>{{ $v->Date40 }}</td>
            <td>{{ $v->Date41 }}</td>
            <td>{{ $v->Date42 }}</td>
            <td>{{ $v->Date43 }}</td>
            <td>{{ $v->Date44 }}</td>
            <td>{{ $v->Date45 }}</td>
            <td>{{ $v->Date46 }}</td>
            <td>{{ $v->Date47 }}</td>
            <td>{{ $v->Date48 }}</td>
            <td>{{ $v->Date49 }}</td>
            <td>{{ $v->Date50 }}</td>
            @foreach ($checklist as $k => $x)
                @if(array_key_exists($v->FileNo1, $x['Files']))
                    <td>{{ $checklist[$k]['Files'][$v->FileNo1] }}</td>
                @else
                    <td></td>
                @endif
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
