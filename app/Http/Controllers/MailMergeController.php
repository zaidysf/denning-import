<?php

namespace App\Http\Controllers;

use App\Helpers\GeneralHelper;
use App\Models\Bankbranchcode;
use App\Models\Bankcode;
use App\Models\File;
use App\Models\MailMerge;
use App\Models\MatterCode;
use App\Models\Cbomattercategory;
use App\Models\BulkTemplateType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpWord\TemplateProcessor;
use ZipArchive;

class MailMergeController extends Controller
{
    public function __construct()
    {
        //
    }

    public function indexTemplate(Request $req)
    {
        $page = 10;
        if ($req->has('page')) {
            $page = $req->get('page');
        }
        if ($req->has('key')) {
            switch ($req->get('key')) {
                case 'template_id':
                    $data = MailMerge::where('template_id', 'LIKE', '%'.$req->get('value').'%');
                    break;

                case 'category':
                    $category = MatterCode::where('Matter', 'LIKE', '%'.$req->get('value').'%')->get()->pluck('MatterCode');
                    $data = MailMerge::whereIn('category', $category);
                    break;

                case 'type':
                    $type = BulkTemplateType::where('name', 'LIKE', '%'.$req->get('value').'%')->get()->pluck('id');
                    $data = MailMerge::whereIn('type', $type);
                    break;

                case 'bank':
                    $bank = Bankcode::where('BankName', 'LIKE', '%'.$req->get('value').'%')->orWhere('BankCode', 'LIKE', '%'.$req->get('value').'%')->get()->pluck('BankCode');
                    $data = MailMerge::whereIn('bank', $bank);
                    break;

                default:
                    $data = MailMerge::where('name', 'LIKE', '%'.$req->get('value').'%');
                    break;
            }
        } else {
            $data = MailMerge::whereNotNull('id');
        }
        if ($req->has('favourite')) {
            $data = $data->where('favourite', $req->get('favourite'));
        }
        if ($req->has('status')) {
            $data = $data->where('status', $req->get('status'));
        }
        $data = $data->paginate($page);
        $selectStatus = [1 => 'Active', 0 => 'Not Active', 2 => 'Expired'];
        $selectBank = Bankcode::get()->pluck('name', 'BankCode');
        $selectType = BulkTemplateType::get()->pluck('name', 'id');
        $selectCategory = Cbomattercategory::get()->pluck('Category', 'ID');
        $selectColumn = ['name' => 'Name', 'category' => 'Category', 'template_id' => 'Template ID', 'type' => 'Type', 'bank' => 'Bank'];
        $selectPagination = [10 => 10, 25 => 25, 50 => 50, 100 => 100];
        return view('mail-merge.index', compact('data', 'selectStatus', 'selectCategory', 'selectType', 'selectBank', 'selectColumn', 'selectPagination'));
    }

    public function generate(Request $req)
    {
        $allreq = $req->all();
        $allreq['generate'] = true;
        return redirect()->route('mail-merge.template.index', $allreq);
    }

    public function uploadTemplate(Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'name' => 'required|unique:mail_merges',
            'category' => 'required',
            'file' => 'required|mimes:doc,docx',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        if ($req->file()) {
            $fileModel = new MailMerge;
            $fileName = time().'_'.$req->file->getClientOriginalName();
            $filePath = $req->file('file')->storeAs('bulk-templates', $fileName, 'ftp');

            $tempId = 1;
            $first = MailMerge::orderBy('template_id', 'desc')->first();
            if ($first) {
                $tempId += $first->template_id;
            }
            $fileModel->template_id = $tempId;
            $fileModel->name = $req->name;
            $fileModel->file_path = $filePath;
            $fileModel->category = $req->category;
            $fileModel->type = $req->type;
            $fileModel->bank = $req->bank;
            $fileModel->staff_id = \Auth::user()->id;
            $fileModel->favourite = $req->favourite == 'on' ? 1 : 0;
            $fileModel->status = $req->status;
            $fileModel->created_by = \Auth::user()->id;
            $fileModel->updated_by = \Auth::user()->id;
            $fileModel->save();

            return back()
            ->with('success', 'File has been uploaded.')
            ->with('file', $fileName);
        }
    }

    public function update(int $id, Request $req)
    {
        $arrValidate = [
            'name' => 'required|unique:mail_merges,name,'.$id,
            'status' => 'required'
        ];

        if ($req->file()) {
            $arrValidate[] = ['file' => 'required|mimes:doc,docx'];
        }

        $validator = \Validator::make($req->all(), $arrValidate);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $fileModel = MailMerge::find($id);
        if ($req->file()) {
            $fileName = time().'_'.$req->file->getClientOriginalName();
            $filePath = $req->file('file')->storeAs('bulk-templates', $fileName, 'ftp');
            Storage::disk('ftp')->delete($fileModel->file_path);
            $fileModel->file_path = $filePath;
        }

        $fileModel->name = $req->name;
        $fileModel->staff_id = \Auth::user()->id;
        $fileModel->type = $req->type;
        $fileModel->bank = $req->bank;
        $fileModel->category = $req->category;
        $fileModel->favourite = $req->favourite == 'on' ? 1 : 0;
        $fileModel->status = $req->status;
        $fileModel->updated_by = \Auth::user()->id;
        $fileModel->save();

        return back()
        ->with('success', 'Template has been updated.');
    }

    public function download(int $id)
    {
        $fileModel = MailMerge::find($id);
        return Storage::disk('ftp')->download($fileModel->file_path);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {
        $mailMerge = MailMerge::find($id);
        Storage::disk('ftp')->delete($mailMerge->file_path);
        $mailMerge->delete();

        return back()
        ->with('success', 'Template has been deleted.');
    }

    public function generateAll($templateId, $files)
    {
        $exportId = time();
        $template = MailMerge::find($templateId);
        if (count($files) < 1) {
            return back()
            ->with('error', 'This filter has no file result');
        }

        foreach ($files as $v) {
            $this->generateTemplate($template->name, $template->file_path, $v->FileNo1, $exportId);
        }

        $path = storage_path('app/public/uploads/mail-merge/'.$exportId);

        // Merge all pdf files in directory
        shell_exec('pdfunite '.$path.'/*.pdf '.$path.'/full['.str_replace(' ', '-', $template->name).'].pdf');

        $zip_file = $exportId.'.zip';
        $zip = new \ZipArchive();
        $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        // Before ---
        $path = storage_path('app/public/uploads/mail-merge/'.$exportId);

        $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
        foreach ($files as $name => $file) {
            // We're skipping all subfolders
            if (!$file->isDir()) {
                $filePath     = $file->getRealPath();
                $zip->addFile($filePath, basename($filePath));
            }
        }
        $zip->close();

        // Before --
        Storage::deleteDirectory('public/uploads/mail-merge/'.$exportId);
        Storage::delete('public/'.$template->name.'.docx');
        // After ---
        // Storage::disk('ftp')->deleteDirectory('public/uploads/mail-merge/'.$exportId);

        return response()->download($zip_file)->deleteFileAfterSend(true);
    }

    public function generateTemplate($templateName, $templatePath, $fileId, $exportId)
    {
        $file = File::where('FileNo1', $fileId)->first();

        $fileFillable = (new File)->getFillable();
        $fileFields = array_map('strtolower', $fileFillable);
        $fileCustomerFields = preg_filter('/^/', 'C', range(0, 30));
        $filePropertyFields = preg_filter('/^/', 'P', range(1, 5));
        $fileLawyerFields = preg_filter('/^/', 'Lawyer', range(1, 5));

        // before ---
        $ftpfile = Storage::disk('ftp')->get($templatePath);
        Storage::disk('local')->put('public/'.$templateName.'.docx', $ftpfile);
        $word = new TemplateProcessor(storage_path('app/public/'.$templateName.'.docx'));
        // after ---
        // $word = new TemplateProcessor(Storage::get('app/public/'.$templatePath));

        foreach ($word->getVariables() as $v) {
            $cols = explode('_', $v);
            $mainCol = $cols[0];

            if (in_array($mainCol, $fileCustomerFields)) {
                if (isset($file->{'cust'.explode('C', $mainCol)[1]})) {
                    $word->setValue($v, $file->{'cust'.explode('C', $mainCol)[1]}->{$cols[1]});
                } else {
                    $word->setValue($v, '-');
                }
            }
            else if (in_array($mainCol, $filePropertyFields)) {
                if (isset($file->{'prop'.explode('P', $mainCol)[1]})) {
                    $word->setValue($v, $file->{'prop'.explode('P', $mainCol)[1]}->{$cols[1]});
                } else {
                    $word->setValue($v, '-');
                }
            }
            else if (in_array($mainCol, $fileLawyerFields)) {
                if (isset($file->{'lawyer'.explode('Lawyer', $mainCol)[1]})) {
                    $word->setValue($v, $file->{'lawyer'.explode('Lawyer', $mainCol)[1]}->{$cols[1]});
                } else {
                    $word->setValue($v, '-');
                }
            }
            else if (substr($v, 0, 2) == '^^') {
                $vv = str_replace('^^', '$', $v);
                if ($file->{$vv} != null) {
                    $helper = new GeneralHelper;
                    $value = str_replace(',', '.', $file->{$vv});
                    $word->setValue($v, $helper->convertNumber($value));
                } else {
                    $word->setValue($v, '-');
                }
            }
            else if (substr($v, 0, 1) == '^') {
                $word->setValue($v, number_format($file->{substr_replace($v,'$',0,1)}, 2, ".", ","));
            }
            else if (isset($file->{$v})) {
                $word->setValue($v, $file->{$v});
            }
            else {
                $word->setValue($v, '-');
            }
        }
        $full_path = 'uploads/mail-merge/'.$exportId.'/'.$fileId.'_'.str_replace(' ', '-', $templateName).'.docx';
        // Storage::copy($templatePath, $full_path);

        // before ---
        Storage::disk('local')->makeDirectory('public/uploads/mail-merge/'.$exportId);
        $word->saveAs(storage_path('app/public/'.$full_path));
        shell_exec('unoconv -f pdf '.storage_path('app/public/'.$full_path));
        return storage_path('app/public/'.$full_path);
        // after ---
        // Storage::disk('ftp')->makeDirectory('app/public/uploads/mail-merge/'.$exportId);
        // $word->saveAs(Storage::disk('ftp')->get('app/public/'.$full_path));
        // shell_exec('unoconv -f pdf '.Storage::disk('ftp')->get('app/public/'.$full_path));
        // return Storage::disk('ftp')->get('app/public/'.$full_path);
    }
}
