<?php

use App\Http\Controllers\Api\V1\CustomerController;
use App\Http\Controllers\Api\V1\FileController;
use App\Http\Controllers\Api\V1\LawyerController;
use App\Http\Controllers\Api\V1\MatterController;
use App\Http\Controllers\Api\V1\PChecklistController;
use App\Http\Controllers\Api\V1\PropertyController;
use App\Http\Controllers\Api\V1\BatchImportsController;
use App\Http\Controllers\Api\V1\MailMergeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::post('/import', [ImportController::class, 'import'])->name('import');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
