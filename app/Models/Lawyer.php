<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * Class Lawyer
 *
 * @property int $LawyerCode
 * @property string|null $FirmName
 * @property string|null $BranchName
 * @property string|null $Initials
 * @property string|null $Title
 * @property string|null $LawyerName
 * @property string|null $Add1
 * @property string|null $Add2
 * @property string|null $Add3
 * @property string|null $Postcode
 * @property string|null $City
 * @property string|null $State
 * @property string|null $Country
 * @property string|null $Phone1
 * @property string|null $Phone2
 * @property string|null $PhoneMobile
 * @property string|null $Fax
 * @property string|null $Email
 * @property string|null $Website
 * @property string|null $remarks
 * @property string|null $Note1
 * @property string|null $Note2
 * @property Carbon|null $dateEntered
 * @property int|null $enteredBy
 * @property Carbon|null $dateUpdated
 * @property int|null $updatedBy
 * @property bool|null $ShowInList
 * @property string $Status
 * @property string|null $Phone1Ctr
 * @property string|null $Phone1No
 * @property string|null $Phone2Ctr
 * @property string|null $Phone2No
 * @property string|null $Phone3Ctr
 * @property string|null $Phone3No
 * @property string|null $Fax1Ctr
 * @property string|null $Fax1No
 * @property string|null $contactPerson
 *
 * @package App\Models
 */
class Lawyer extends Model
{


    protected $table = 'lawyer';
    protected $primaryKey = 'LawyerCode';
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'enteredBy' => 'int',
        'updatedBy' => 'int',
        'ShowInList' => 'bool'
    ];

    protected $dates = [
        'dateEntered',
        'dateUpdated'
    ];

    protected $fillable = [
        'FirmName',
        'BranchName',
        'Initials',
        'Title',
        'LawyerName',
        'Add1',
        'Add2',
        'Add3',
        'Postcode',
        'City',
        'State',
        'Country',
        'Phone1',
        'Phone2',
        'PhoneMobile',
        'Fax',
        'Email',
        'Website',
        'remarks',
        'Note1',
        'Note2',
        'dateEntered',
        'enteredBy',
        'dateUpdated',
        'updatedBy',
        'ShowInList',
        'Status',
        'Phone1Ctr',
        'Phone1No',
        'Phone2Ctr',
        'Phone2No',
        'Phone3Ctr',
        'Phone3No',
        'Fax1Ctr',
        'Fax1No',
        'contactPerson'
    ];
}
