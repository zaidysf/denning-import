@extends('layout')
@section('content')
<div class="mb-3">
    {{-- <h1 class="h3 d-inline align-middle">Setup</h1> --}}
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="container-fluid">
                    <div class="row">
                        {{-- <div class="col-12 col-lg-6 d-none d-lg-flex col-img">
                        </div> --}}
                        <div class="col-12 col-lg-12 align-self-center">
                            <div class="m-3 m-lg-5">

                                <div class="text-center mt-4 mb-3">
                                    <h1 class="h2">Denning Initial Setup</h1>
                                    <p class="lead">
                                        Navigate between steps in this setup wizard.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-12 align-self-center">
                            <div class="row">
                                {{-- <div class="col-12 col-lg-6 d-none d-lg-flex col-img">
                                </div> --}}
                                <div class="col-3">
                                    <div class="p-3">
                                        <div class="text-center">
                                            <a href="{{ route('setup.firm.index') }}" class="title-link">
                                                <h1 class="h3">
                                                    <i class="align-middle h3" data-feather="info"></i><br/>
                                                    1. Firm Profile (HQ)
                                                </h1>
                                            </a>
                                            <p class="lead">
                                                Enter and update HQ address, contact details, etc
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="p-3">
                                        <div class="text-center">
                                            <a href="{{ route('setup.branch.index') }}" class="title-link">
                                                <h1 class="h3">
                                                    <i class="align-middle h3" data-feather="map-pin"></i><br/>
                                                    2. Branches
                                                </h1>
                                            </a>
                                            <p class="lead">
                                                Enter and update Branch (if any) address, contact details, etc
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="p-3">
                                        <div class="text-center">
                                            <a href="#" class="title-link">
                                                <h1 class="h3">
                                                    <i class="align-middle h3" data-feather="mail"></i><br/>
                                                    3. Letterhead
                                                </h1>
                                            </a>
                                            <p class="lead">
                                                Upload Firm or Branch Letterhead
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="p-3">
                                        <div class="text-center">
                                            <a href="{{ route('setup.staff.index') }}" class="title-link">
                                                <h1 class="h3">
                                                    <i class="align-middle h3" data-feather="users"></i><br/>
                                                    4. Staff or Users
                                                </h1>
                                            </a>
                                            <p class="lead">
                                                Enter & update staff and non-staff users info and employment details
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="p-3">
                                        <div class="text-center">
                                            <a href="{{ route('setup.login-password.index') }}" class="title-link">
                                                <h1 class="h3">
                                                    <i class="align-middle h3" data-feather="log-in"></i><br/>
                                                    5. Login ID &amp; Password
                                                </h1>
                                            </a>
                                            <p class="lead">
                                                Create & reset login ID and password of staff and users
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="p-3">
                                        <div class="text-center">
                                            <a href="#" class="title-link">
                                                <h1 class="h3">
                                                    <i class="align-middle h3" data-feather="lock"></i><br/>
                                                    6. Privileges &amp; Restrictions
                                                </h1>
                                            </a>
                                            <p class="lead">
                                                Set staff and users permission and restriction for access to Denning features, view and edit data
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="p-3">
                                        <div class="text-center">
                                            <a href="{{ route('setup.currency.index') }}" class="title-link">
                                                <h1 class="h3">
                                                    <i class="align-middle h3" data-feather="dollar-sign"></i><br/>
                                                    7. Currency &amp; Tax Setting
                                                </h1>
                                            </a>
                                            <p class="lead">
                                                Enter and update Currency and Tax settings of the country or region in which your HQ and branch are located.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="p-3">
                                        <div class="text-center">
                                            <a href="{{ route('setup.account-bank.index') }}" class="title-link">
                                                <h1 class="h3">
                                                    <i class="align-middle h3" data-feather="book"></i><br/>
                                                    8. Bank Accounts
                                                </h1>
                                            </a>
                                            <p class="lead">
                                                Enter and update your firm banking accounts, e-wallets or prepaid card  for operation
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="p-3">
                                        <div class="text-center">
                                            <a href="{{ route('export.index') }}" class="title-link">
                                                <h1 class="h3">
                                                    <i class="align-middle h3" data-feather="book"></i><br/>
                                                    9. Create Existing Files Range
                                                </h1>
                                            </a>
                                            <p class="lead">
                                                Enter create and reserve file no range for existing matters which will also be used as ledger no
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="p-3">
                                        <div class="text-center">
                                            <a href="#" class="title-link">
                                                <h1 class="h3">
                                                    <i class="align-middle h3" data-feather="credit-card"></i><br/>
                                                    10. Opening Balances (client / file ledgers)
                                                </h1>
                                            </a>
                                            <p class="lead">
                                                Enter the credit balance of your files or matters at the start of using Denning for your firm trust (client), disbursement (billing) and office advances made to clients.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="p-3">
                                        <div class="text-center">
                                            <a href="#" class="title-link">
                                                <h1 class="h3">
                                                    <i class="align-middle h3" data-feather="credit-card"></i><br/>
                                                    11. Opening Balances (office account)
                                                </h1>
                                            </a>
                                            <p class="lead">
                                                Enter the account balances of your office account at the start of using Denning
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="p-3">
                                        <div class="text-center">
                                            <a href="#" class="title-link">
                                                <h1 class="h3">
                                                    <i class="align-middle h3" data-feather="layout"></i><br/>
                                                    12. Outstanding Invoice
                                                </h1>
                                            </a>
                                            <p class="lead">
                                                Enter existing outstanding invoices at the start of using Denning to enable official receipts, fees transfer and revenue to be computed and function properly.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="p-3">
                                        <div class="text-center">
                                            <a href="#" class="title-link">
                                                <h1 class="h3">
                                                    <i class="align-middle h3" data-feather="mail"></i><br/>
                                                    13. Outgoing Email Setting
                                                </h1>
                                            </a>
                                            <p class="lead">
                                                To configure outgoing emails setting used for sending emails and attachments from file folders.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('style')
<style>
.lead {
    font-size: 0.9rem;
    font-weight: 300;
}
.title-link h1 {
    color: blue;
}
/* .zandgar__wizard {
    display: block;
    height: 100%;
    position: relative;
}

.zandgar__wizard .zandgar__step {
    display: none;
}
.zandgar__wizard .zandgar__step.zandgar__step__active {
    display: block;
}

.col-img {
    background: scroll center url('https://images.unsplash.com/photo-1497366754035-f200968a6e72?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80');
    background-size: cover;
    min-height: 100vh;
} */
</style>
@endsection
@section('script')
<script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
{{-- <script src="{{ asset('js/zangdar.min.js') }}"></script>
<script>
    function buildStepsBreadcrumb (wizard, element, steps) {
    const $steps = document.getElementById(element)
        $steps.innerHTML = ''
        for (let label in steps) {
            if (steps.hasOwnProperty(label)) {
                const $li = document.createElement('li')
                const $a = document.createElement('a')
                $li.classList.add('nav-item')
                $a.classList.add('nav-link')
                if (steps[label].active) {
                    $a.classList.add('active')
                }
                $a.setAttribute('href', '#')
                $a.innerText = label
                $a.addEventListener('click', e => {
                    e.preventDefault()
                    wizard.revealStep(label)
                })
                $li.appendChild($a)
                $steps.appendChild($li)
            }
        }
    }

    function onStepChange(wizard, selector) {
        const steps = wizard.getBreadcrumb()
        buildStepsBreadcrumb(wizard, selector, steps)
    }

    const wizard = new window.Zangdar('#wizard', {
    onStepChange: () => {
        onStepChange(wizard, 'steps-native')
    },
    onSubmit(e) {
        e.preventDefault()
        console.log(e.target.elements)
        return false
    }
    })

    onStepChange(wizard, 'steps-native')
</script> --}}
@endsection
