<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\ProgramOwner;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    public function index(Request $req)
    {
        $page = 10;
        if ($req->has('per_page')) {
            $page = $req->get('per_page');
        }
        if ($req->has('key')) {
            switch ($req->get('key')) {
                case 'name':
                    $data = ProgramOwner::where('Name', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'branch':
                    $data = ProgramOwner::where('BranchName', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'city':
                    $data = ProgramOwner::where('City', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'currency':
                    $data = ProgramOwner::where('DefaultCurrencySymbol', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'tax':
                    $data = ProgramOwner::where('DefaultTaxLabel', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'service':
                    $data = ProgramOwner::where('ServiceTax', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                default:
                    $data = ProgramOwner::where('BranchName', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;
            }
        } else {
            $data = ProgramOwner::paginate($page);
        }
        $selectColumn = [
            'name' => 'Name',
            'branch' => 'Branch',
            'city' => 'Town',
            'currency' => 'Currency Symbol',
            'tax' => 'Tax Label',
            'service' => 'Service Tax Rate',
        ];
        $selectPagination = [10 => 10, 25 => 25, 50 => 50, 100 => 100];
        return view('setup.currency.index', compact('data', 'selectColumn', 'selectPagination'));
    }

    public function store(Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'name' => 'required|unique:bulk_template_types',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $model = new ProgramOwner;
        $model->name = $req->name;
        $model->save();

        return back()
        ->with('success', 'Currency setting has been created.')
        ->with('name', $req->name);
    }

    public function edit($id)
    {
        $data = ProgramOwner::find($id);
        $formActionURL = route('setup.currency.update', ['id' => $id]);
        $label = 'Edit Currency - '.$data->BranchName;
        $method = 'edit';
        $currencies = Currency::all();
        return view('setup.currency.add', compact('data', 'formActionURL', 'label', 'method', 'currencies'));
    }

    public function update(int $id, Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $model = ProgramOwner::find($id);
        $model->name = $req->name;
        $model->save();

        return back()
        ->with('success', 'Currency setting has been updated.');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {
        $model = ProgramOwner::find($id);
        $model->delete();

        return back()
        ->with('success', 'Currency setting has been deleted.');
    }
}
