<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Currency
 * 
 * @property string $Code
 * @property string $Name
 * @property string $Symbol
 * @property string $SymbolNative
 * @property int $DecimalDigits
 *
 * @package App\Models
 */
class Currency extends Model
{
	protected $table = 'currency';
	protected $primaryKey = 'Code';
	public $incrementing = false;
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'DecimalDigits' => 'int'
	];

	protected $fillable = [
		'Name',
		'Symbol',
		'SymbolNative',
		'DecimalDigits'
	];
}
