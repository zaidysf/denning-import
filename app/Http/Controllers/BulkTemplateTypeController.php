<?php

namespace App\Http\Controllers;

use App\Models\BulkTemplateType;
use Illuminate\Http\Request;

class BulkTemplateTypeController extends Controller
{
    public function index(Request $req)
    {
        $page = 10;
        if ($req->has('per_page')) {
            $page = $req->get('per_page');
        }
        if ($req->has('key')) {
            switch ($req->get('key')) {
                case 'name':
                    $data = BulkTemplateType::where('name', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                default:
                    $data = BulkTemplateType::where('name', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;
            }
        } else {
            $data = BulkTemplateType::paginate($page);
        }
        $selectColumn = [
            'name' => 'Name',
        ];
        $selectPagination = [10 => 10, 25 => 25, 50 => 50, 100 => 100];
        return view('bulk-template-type.index', compact('data', 'selectColumn', 'selectPagination'));
    }

    public function store(Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'name' => 'required|unique:bulk_template_types',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $model = new BulkTemplateType;
        $model->name = $req->name;
        $model->save();

        return back()
        ->with('success', 'Template type has been created.')
        ->with('name', $req->name);
    }

    public function update(int $id, Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'name' => 'required|unique:bulk_template_types,name,'.$id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $model = BulkTemplateType::find($id);
        $model->name = $req->name;
        $model->save();

        return back()
        ->with('success', 'Template type has been updated.');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {
        $model = BulkTemplateType::find($id);
        $model->delete();

        return back()
        ->with('success', 'Template type has been deleted.');
    }
}
