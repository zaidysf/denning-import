<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Spapresetchecklist;
use Illuminate\Http\Request;

class PChecklistController extends Controller
{
    public function __construct()
    {
        //
    }

    public function search(Request $request)
    {
        $pChecklist = Spapresetchecklist::selectRaw("CONCAT_WS(' - ', PresetCode, Category, Description) as text,
        PresetCode as id")
            ->where('PresetCode', 'LIKE', '%'.$request->input('term', '').'%')
            ->orWhere('Category', 'LIKE', '%'.$request->input('term', '').'%')
            ->orWhere('Description', 'LIKE', '%'.$request->input('term', '').'%')
            ->orderBy('PresetCode', 'ASC')
            ->simplePaginate(10);
        $pChecklist->appends(['term' => $request->input('term', '')]);
        return ['results' => $pChecklist];
    }
}
