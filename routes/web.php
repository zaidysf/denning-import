<?php

use App\Helpers\LogHelper;
use App\Http\Controllers\Api\V1\BatchImportsController;
use App\Http\Controllers\Api\V1\BulkTemplateTypeController as V1BulkTemplateTypeController;
use App\Http\Controllers\Api\V1\CustomerController;
use App\Http\Controllers\Api\V1\FileController as V1FileController;
use App\Http\Controllers\Api\V1\LawyerController;
use App\Http\Controllers\Api\V1\MailMergeController as V1MailMergeController;
use App\Http\Controllers\Api\V1\MatterController;
use App\Http\Controllers\Api\V1\ProjectController;
use App\Http\Controllers\Api\V1\PChecklistController;
use App\Http\Controllers\Api\V1\PropertyController;
use App\Http\Controllers\Api\V1\StaffController as V1StaffController;
use App\Http\Controllers\Setup\BranchController;
use App\Http\Controllers\BulkTemplateTypeController;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\FileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ImportController;
use App\Http\Controllers\MailMergeController;
use App\Http\Controllers\FileManagerController;
use App\Http\Controllers\Setup\AccountBankController;
use App\Http\Controllers\Setup\CurrencyController;
use App\Http\Controllers\Setup\FirmController;
use App\Http\Controllers\Setup\LoginPasswordController;
use App\Http\Controllers\Setup\StaffController;
use App\Models\ProgramOwner;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/dashboard', function () {
    return view('welcome', ['data' => ProgramOwner::where('FirmCode', '>', 0)->orderBy('FirmCode', 'asc')->first()]);
})->middleware(['auth', 'db.dynamic'])->name('dashboard');

// SETUP
Route::get('/setup', function () {
    return view('setup.index');
})->name('setup.index');
Route::get('/setup/staff', [StaffController::class, 'index'])->middleware(['auth', 'db.dynamic'])->name('setup.staff.index');
Route::get('/setup/staff/add', [StaffController::class, 'add'])->middleware(['auth', 'db.dynamic'])->name('setup.staff.add');
Route::get('/setup/staff/{id}', [StaffController::class, 'edit'])->middleware(['auth', 'db.dynamic'])->name('setup.staff.edit');
Route::post('/setup/staff/store', [StaffController::class, 'store'])->middleware(['auth', 'db.dynamic'])->name('setup.staff.store');
Route::put('/setup/staff/{id}', [StaffController::class, 'update'])->middleware(['auth', 'db.dynamic'])->name('setup.staff.update');
Route::delete('/setup/staff/{id}', [StaffController::class, 'destroy'])->middleware(['auth', 'db.dynamic'])->name('setup.staff.destroy');

Route::get('/setup/firm', [FirmController::class, 'index'])->middleware(['auth', 'db.dynamic'])->name('setup.firm.index');
Route::get('/setup/firm/{id}', [FirmController::class, 'edit'])->middleware(['auth', 'db.dynamic'])->name('setup.firm.edit');
Route::post('/setup/firm/store', [FirmController::class, 'store'])->middleware(['auth', 'db.dynamic'])->name('setup.firm.store');
Route::put('/setup/firm/{id}', [FirmController::class, 'update'])->middleware(['auth', 'db.dynamic'])->name('setup.firm.update');
Route::delete('/setup/firm/{id}', [FirmController::class, 'destroy'])->middleware(['auth', 'db.dynamic'])->name('setup.firm.destroy');

Route::get('/setup/branch', [BranchController::class, 'index'])->middleware(['auth', 'db.dynamic'])->name('setup.branch.index');
Route::get('/setup/branch/add', [BranchController::class, 'add'])->middleware(['auth', 'db.dynamic'])->name('setup.branch.add');
Route::get('/setup/branch/{id}', [BranchController::class, 'edit'])->middleware(['auth', 'db.dynamic'])->name('setup.branch.edit');
Route::post('/setup/branch/store', [BranchController::class, 'store'])->middleware(['auth', 'db.dynamic'])->name('setup.branch.store');
Route::put('/setup/branch/{id}', [BranchController::class, 'update'])->middleware(['auth', 'db.dynamic'])->name('setup.branch.update');
Route::delete('/setup/branch/{id}', [BranchController::class, 'destroy'])->middleware(['auth', 'db.dynamic'])->name('setup.branch.destroy');

Route::get('/setup/login-password', [LoginPasswordController::class, 'index'])->middleware(['auth', 'db.dynamic'])->name('setup.login-password.index');
Route::get('/setup/login-password/add', [LoginPasswordController::class, 'add'])->middleware(['auth', 'db.dynamic'])->name('setup.login-password.add');
Route::get('/setup/login-password/{id}', [LoginPasswordController::class, 'edit'])->middleware(['auth', 'db.dynamic'])->name('setup.login-password.edit');
Route::post('/setup/login-password/store', [LoginPasswordController::class, 'store'])->middleware(['auth', 'db.dynamic'])->name('setup.login-password.store');
Route::put('/setup/login-password/{id}', [LoginPasswordController::class, 'update'])->middleware(['auth', 'db.dynamic'])->name('setup.login-password.update');
Route::delete('/setup/login-password/{id}', [LoginPasswordController::class, 'destroy'])->middleware(['auth', 'db.dynamic'])->name('setup.login-password.destroy');

Route::get('/setup/account-bank', [AccountBankController::class, 'index'])->middleware(['auth', 'db.dynamic'])->name('setup.account-bank.index');
Route::get('/setup/account-bank/{id}', [AccountBankController::class, 'edit'])->middleware(['auth', 'db.dynamic'])->name('setup.account-bank.edit');
Route::post('/setup/account-bank/store', [AccountBankController::class, 'store'])->middleware(['auth', 'db.dynamic'])->name('setup.account-bank.store');
Route::put('/setup/account-bank/{id}', [AccountBankController::class, 'update'])->middleware(['auth', 'db.dynamic'])->name('setup.account-bank.update');
Route::delete('/setup/account-bank/{id}', [AccountBankController::class, 'destroy'])->middleware(['auth', 'db.dynamic'])->name('setup.account-bank.destroy');

Route::get('/setup/currency', [CurrencyController::class, 'index'])->middleware(['auth', 'db.dynamic'])->name('setup.currency.index');
Route::get('/setup/currency/{id}', [CurrencyController::class, 'edit'])->middleware(['auth', 'db.dynamic'])->name('setup.currency.edit');
Route::post('/setup/currency/store', [CurrencyController::class, 'store'])->middleware(['auth', 'db.dynamic'])->name('setup.currency.store');
Route::put('/setup/currency/{id}', [CurrencyController::class, 'update'])->middleware(['auth', 'db.dynamic'])->name('setup.currency.update');
Route::delete('/setup/currency/{id}', [CurrencyController::class, 'destroy'])->middleware(['auth', 'db.dynamic'])->name('setup.currency.destroy');

require __DIR__.'/auth.php';

Route::get('/file', [FileController::class, 'index'])
    ->middleware(['auth', 'db.dynamic'])->name('file.index');
Route::get('/list', [ImportController::class, 'list'])->middleware(['auth', 'db.dynamic'])->name('import.list');
Route::get('/import/{staffId}', [ImportController::class, 'index'])->middleware(['auth', 'db.dynamic'])->name('import.index');
Route::post('/import', [ImportController::class, 'import'])->middleware(['auth', 'db.dynamic'])->name('import.post');
Route::get('/export', [ExportController::class, 'index'])->middleware(['auth', 'db.dynamic'])->name('export.index');
Route::post('/export', [ExportController::class, 'export'])->middleware(['auth', 'db.dynamic'])->name('export.post');
Route::post('/mail-merge/template/upload', [MailMergeController::class, 'uploadTemplate'])
    ->middleware(['auth', 'db.dynamic'])->name('mail-merge.template.upload');
Route::get('/mail-merge/template', [MailMergeController::class, 'indexTemplate'])
    ->middleware(['auth', 'db.dynamic'])->name('mail-merge.template.index');
Route::get('/mail-merge/download/{id}', [MailMergeController::class, 'download'])
    ->middleware(['auth', 'db.dynamic'])->name('mail-merge.template.download');
Route::delete('/mail-merge/template/{id}', [MailMergeController::class, 'destroy'])
    ->middleware(['auth', 'db.dynamic'])->name('mail-merge.template.destroy');
Route::put('/mail-merge/template/{id}', [MailMergeController::class, 'update'])
    ->middleware(['auth', 'db.dynamic'])->name('mail-merge.template.update');
Route::post('/mail-merge/template/generate', [MailMergeController::class, 'generate'])
    ->middleware(['auth', 'db.dynamic'])->name('mail-merge.template.generate');
Route::get('/filemanager', [FileManagerController::class, 'index'])->middleware(['auth', 'db.dynamic'])->name('filemanager.index');
Route::get('/bulk-template-type', [BulkTemplateTypeController::class, 'index'])->middleware(['auth', 'db.dynamic'])->name('bulk-template-type.index');
Route::get('/bulk-template-type/{id}', [BulkTemplateTypeController::class, 'edit'])->middleware(['auth', 'db.dynamic'])->name('bulk-template-type.edit');
Route::post('/bulk-template-type/store', [BulkTemplateTypeController::class, 'store'])->middleware(['auth', 'db.dynamic'])->name('bulk-template-type.store');
Route::put('/bulk-template-type/{id}', [BulkTemplateTypeController::class, 'update'])->middleware(['auth', 'db.dynamic'])->name('bulk-template-type.update');
Route::delete('/bulk-template-type/{id}', [BulkTemplateTypeController::class, 'destroy'])->middleware(['auth', 'db.dynamic'])->name('bulk-template-type.destroy');

Route::prefix('mini')->group(function () {
    Route::get('/list', [ImportController::class, 'list'])->middleware(['auth', 'db.dynamic'])->name('mini.import.list');
    Route::get('/import/{staffId}', [ImportController::class, 'index'])
        ->middleware(['auth', 'db.dynamic'])->name('mini.import.index');
    Route::get('/export', [ExportController::class, 'index'])->middleware(['auth', 'db.dynamic'])->name('mini.export.index');
    Route::get('/mail-merge/template', [MailMergeController::class, 'indexTemplate'])
        ->middleware(['auth', 'db.dynamic'])->name('mini.mail-merge.template.index');
    Route::get('/filemanager', [FileManagerController::class, 'index'])->middleware(['auth', 'db.dynamic'])->name('mini.filemanager.index');
    Route::get('/bulk-template-type', [BulkTemplateTypeController::class, 'index'])->middleware(['auth', 'db.dynamic'])->name('mini.bulk-template-type.index');
});


// API
Route::get('api/v1/customer/search', [CustomerController::class, 'search'])->middleware(['auth', 'db.dynamic'])->name('api.customer.search');
Route::get('api/v1/property/search', [PropertyController::class, 'search'])->middleware(['auth', 'db.dynamic'])->name('api.property.search');
Route::get('api/v1/file/search', [V1FileController::class, 'search'])->middleware(['auth', 'db.dynamic'])->name('api.file.search');
Route::get('api/v1/pchecklist/search', [PChecklistController::class, 'search'])->middleware(['auth', 'db.dynamic'])->name('api.pchecklist.search');
Route::get('api/v1/matter/search', [MatterController::class, 'search'])->middleware(['auth', 'db.dynamic'])->name('api.matter.search');
Route::get('api/v1/lawyer/search', [LawyerController::class, 'search'])->middleware(['auth', 'db.dynamic'])->name('api.lawyer.search');
Route::get('api/v1/pn15/search', [BatchImportsController::class, 'search'])->middleware(['auth', 'db.dynamic'])->name('api.pn15.search');
Route::get('api/v1/project/search', [ProjectController::class, 'search'])->middleware(['auth', 'db.dynamic'])->name('api.project.search');
Route::get('api/v1/mail-merge/{id}', [V1MailMergeController::class, 'find'])->middleware(['auth', 'db.dynamic'])->name('api.mail-merge.find');
Route::get('api/v1/mail-merge/search', [V1MailMergeController::class, 'search'])->middleware(['auth', 'db.dynamic'])->name('api.mail-merge.search');
Route::get('api/v1/bulk-template-type/{id}', [V1BulkTemplateTypeController::class, 'find'])->middleware(['auth', 'db.dynamic'])->name('api.bulk-template-type.find');
Route::get('api/v1/bulk-template-type/search', [V1BulkTemplateTypeController::class, 'search'])->middleware(['auth', 'db.dynamic'])->name('api.bulk-template-type.search');
Route::get('api/v1/staff/{id}', [V1StaffController::class, 'find'])->middleware(['auth', 'db.dynamic'])->name('api.staff.find');
Route::get('api/v1/staff/search', [V1StaffController::class, 'search'])->middleware(['auth', 'db.dynamic'])->name('api.staff.search');


Route::get('/debug-sentry', function () {
    return LogHelper::makeException('Test import', 401);
});

Route::get('/debug-sentry2', function () {
    return LogHelper::getExampleRequests('exception');
});
