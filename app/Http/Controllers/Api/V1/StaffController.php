<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Staff;

class StaffController extends Controller
{
    public function __construct()
    {
        //
    }

    public function find($id)
    {
        $data = Staff::find($id);
        return ['data' => $data];
    }
}
