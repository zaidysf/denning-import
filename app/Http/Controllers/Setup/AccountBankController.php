<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Accountbank;
use App\Models\Bankbranchcode;
use App\Models\Bankcode;
use Illuminate\Http\Request;

class AccountBankController extends Controller
{
    public function index(Request $req)
    {
        $page = 10;
        if ($req->has('per_page')) {
            $page = $req->get('per_page');
        }
        if ($req->has('key')) {
            switch ($req->get('key')) {
                case 'BankID':
                    $accountBank = Accountbank::get()->pluck('BankBranchCode');
                    $bank = Bankcode::where('BankCode', 'LIKE', '%'.$req->get('value').'%')->get()->pluck('BankCode');
                    $branch = Bankbranchcode::whereIn('BankCode', $bank)->whereIn('BranchCode', $accountBank)->get()->pluck('BranchCode');
                    $data = Accountbank::whereIn('BankBranchCode', $branch)->paginate($page);
                    break;

                case 'BankAccNo':
                    $data = Accountbank::where('BankAccNo', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'BankAccName':
                    $data = Accountbank::where('BankAccName', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'DateOpen':
                    $data = Accountbank::where('DateOpen', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'DateClose':
                    $data = Accountbank::where('DateClose', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'BankBranchCode':
                    $branch = Bankbranchcode::where('BranchName', 'LIKE', '%'.$req->get('value').'%')->get()->pluck('BranchCode');
                    $data = Accountbank::whereIn('BankBranchCode', $branch)->paginate($page);
                    break;

                default:
                    $data = Accountbank::where('name', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;
            }
        } else {
            $data = Accountbank::paginate($page);
        }
        $selectColumn = [
            'BankID' => 'Bank Name',
            'BankAccNo' => 'Bank Acc No',
            'BankAccName' => 'Bank Acc Name',
            'BankBranchCode' => 'Branch',
            'DateOpen' => 'Opened',
            'DateClose' => 'Closed',
        ];
        $selectPagination = [10 => 10, 25 => 25, 50 => 50, 100 => 100];
        return view('setup.account_bank.index', compact('data', 'selectColumn', 'selectPagination'));
    }

    public function store(Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'name' => 'required|unique:bulk_template_types',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $model = new Staff;
        $model->name = $req->name;
        $model->save();

        return back()
        ->with('success', 'Staff has been created.')
        ->with('name', $req->name);
    }

    public function update(int $id, Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'name' => 'required|unique:bulk_template_types,name,'.$id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $model = Staff::find($id);
        $model->name = $req->name;
        $model->save();

        return back()
        ->with('success', 'Staff has been updated.');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {
        $model = Staff::find($id);
        $model->delete();

        return back()
        ->with('success', 'Staff has been deleted.');
    }
}
