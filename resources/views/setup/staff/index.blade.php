@extends('layout')
@section('content')
    <div class="mb-3">
        <a href="{{ route('setup.index') }}" class="title-link"><h1 class="h5 d-inline align-middle">&larr; Back to Setup</h1></a><br/>
        <h1 class="h3 d-inline align-middle">Staff or Users</h1>
        <a href="{{ route('setup.staff.add') }}" class="btn btn-primary btn-sm" style="margin-left: 10px;"> + Add Staff </a>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form method="GET" action="{{ route('setup.staff.index') }}">
                    @csrf
                    <div class="row">
                        <div class="col mb-2">
                            <select name="key" class="form-control">
                                @foreach ($selectColumn as $k => $v)
                                    <option {{ isset($_GET['key']) ? ($_GET['key'] == $k ? "selected" : "") : "" }} value="{{ $k }}">{{ $v }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <input
                                placeholder="Enter keywords..."
                                name="value"
                                type="text"
                                class="form-control"
                                value="{{ isset($_GET['value']) ? $_GET['value'] : "" }}"/>
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                        <div class="col pull-right text-end mt-2">
                            Per Page
                        </div>
                        <div class="col-1 pull-right text-end">
                            <select id="select-page" name="per_page" class="form-control">
                                @foreach ($selectPagination as $k => $v)
                                    <option {{ isset($_GET['per_page']) ? ($_GET['per_page'] == $k ? "selected" : "") : "" }} value="{{ $k }}">
                                        {{ $v }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    </form>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">&nbsp;</th>
                                <th scope="col">Name</th>
                                <th scope="col">Nickname</th>
                                <th scope="col">Default Branch</th>
                                <th scope="col">Position</th>
                                <th scope="col">Department</th>
                                <th scope="col">Status</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone Mobile</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $v)
                                <tr>
                                    <td>
                                        <a href="{{ route('setup.staff.edit', ['id' => $v->staffCode]) }}" class="btn btn-warning btn-sm">Edit</a>
                                    </td>
                                    <td>{{ $v->name }}</td>
                                    <td>{{ $v->initials }}</td>
                                    <td>{{ $v->branch->BranchName }}</td>
                                    <td>{{ $v->positionTitle }}</td>
                                    <td>{{ $v->department }}</td>
                                    <td>{{ $v->status }}</td>
                                    <td>{{ $v->emailAdd }}</td>
                                    <td>{{ $v->phoneMobile }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @include('pagination', ['paginator' => $data])
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
<style>
.title-link h1 {
    color: blue;
}
/* .zandgar__wizard {
    display: block;
    height: 100%;
    position: relative;
}

.zandgar__wizard .zandgar__step {
    display: none;
}
.zandgar__wizard .zandgar__step.zandgar__step__active {
    display: block;
}

.col-img {
    background: scroll center url('https://images.unsplash.com/photo-1497366754035-f200968a6e72?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80');
    background-size: cover;
    min-height: 100vh;
} */
</style>
@endsection

@section('script')
<script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>>
<script>
    $('#select-page').change(function(e){
        if (this.value != null && this.value != "") {
            let searchParams = new URLSearchParams(window.location.search)
            searchParams.delete('per_page');
            searchParams.append('per_page', this.value);
            window.location='{{ route('setup.staff.index') }}?'+searchParams.toString();
        }
    });
    $("#searchForm").submit( function(eventObj) {
        let searchParams = new URLSearchParams(window.location.search)
        searchParams.delete("_token");
        searchParams.delete("key");
        searchParams.delete("value");
        searchParams.forEach(function(value, key) {
            $("<input />").attr("type", "hidden")
                .attr("name", key)
                .attr("value", value)
                .appendTo("#searchForm");
        });
        return true;
    });
</script>
@endsection
