<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Lawyer;
use App\Models\Property;
use Illuminate\Http\Request;

class LawyerController extends Controller
{
    public function __construct()
    {
        //
    }

    public function search(Request $request)
    {
        $lawyer = Lawyer::selectRaw("CONCAT_WS(' - ', FirmName, BranchName, City) as text, LawyerCode as id")
            ->where('FirmName', 'LIKE', '%'.$request->input('term', '').'%')
            ->orWhere('BranchName', 'LIKE', '%'.$request->input('term', '').'%')
            ->orWhere('City', 'LIKE', '%'.$request->input('term', '').'%')
            ->simplePaginate(10);
        $lawyer->appends(['term' => $request->input('term', '')]);
        return ['results' => $lawyer];
    }
}
