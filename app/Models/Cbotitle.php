<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Cbotitle
 * 
 * @property int $ID
 * @property string $Item
 * @property Carbon|null $dateEntered
 * @property int|null $enteredBy
 * @property Carbon|null $dateUpdated
 * @property int|null $updatedBy
 * @property string|null $remarks
 *
 * @package App\Models
 */
class Cbotitle extends Model
{
	protected $table = 'cbotitle';
	protected $primaryKey = 'ID';
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'enteredBy' => 'int',
		'updatedBy' => 'int'
	];

	protected $dates = [
		'dateEntered',
		'dateUpdated'
	];

	protected $fillable = [
		'Item',
		'dateEntered',
		'enteredBy',
		'dateUpdated',
		'updatedBy',
		'remarks'
	];
}
