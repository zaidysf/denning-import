<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * Class Staff
 *
 * @property int|null $staffCode
 * @property int|null $idType
 * @property string|null $idNo
 * @property string|null $idNoOld
 * @property string|null $name
 * @property string $initials
 * @property string|null $title
 * @property string|null $add1
 * @property string|null $add2
 * @property string|null $add3
 * @property string|null $postCode
 * @property string|null $city
 * @property string|null $state
 * @property string|null $country
 * @property string|null $phoneHome
 * @property string|null $phoneOffice
 * @property string|null $fax
 * @property string|null $phoneMobile
 * @property string|null $emailAdd
 * @property string|null $contactPerson
 * @property string|null $citizenship
 * @property string|null $department
 * @property string|null $positionTitle
 * @property float|null $monthlySalary
 * @property Carbon|null $prevAdjustDate
 * @property string|null $annualLeave
 * @property Carbon|null $dateCommenced
 * @property Carbon|null $dateBirth
 * @property Carbon|null $dateCeased
 * @property string|null $tenureEmployed
 * @property string|null $qualification
 * @property string|null $status
 * @property string|null $marital
 * @property string|null $spouse
 * @property int|null $childrenNo
 * @property string|null $taxFileNo
 * @property int|null $irdBranch
 * @property string|null $socsoNo
 * @property string|null $mailList
 * @property int|null $enteredBy
 * @property Carbon|null $dateEntered
 * @property int|null $updatedBy
 * @property Carbon|null $dateUpdated
 * @property string|null $remarks
 * @property string|null $blank1
 * @property string|null $blank2
 * @property string|null $type
 * @property string|null $id
 * @property string|null $pass
 * @property string $qbID
 * @property string|null $permission
 * @property string|null $epfNo
 * @property bool $IsCloudPreload
 * @property string $IsCloudSharedFolder
 * @property string $SalaryAccount
 * @property string $location
 * @property string|null $DefaultBranch
 * @property string|null $Phone1Ctr
 * @property string|null $Phone1No
 * @property string|null $Phone2Ctr
 * @property string|null $Phone2No
 * @property string|null $Phone3Ctr
 * @property string|null $Phone3No
 * @property string|null $Fax1Ctr
 * @property string|null $Fax1No
 * @property string|null $website
 *
 * @package App\Models
 */
class Staff extends Model
{
    protected $table = 'staff';
    protected $primaryKey = 'staffCode';
    public $incrementing = false;
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'staffCode' => 'int',
        'idType' => 'int',
        'monthlySalary' => 'float',
        'childrenNo' => 'int',
        'irdBranch' => 'int',
        'enteredBy' => 'int',
        'updatedBy' => 'int',
        'IsCloudPreload' => 'bool'
    ];

    protected $dates = [
        'prevAdjustDate',
        'dateCommenced',
        'dateBirth',
        'dateCeased',
        'dateEntered',
        'dateUpdated'
    ];

    protected $fillable = [
        'staffCode',
        'idType',
        'idNo',
        'idNoOld',
        'name',
        'initials',
        'title',
        'add1',
        'add2',
        'add3',
        'postCode',
        'city',
        'state',
        'country',
        'phoneHome',
        'phoneOffice',
        'fax',
        'phoneMobile',
        'emailAdd',
        'contactPerson',
        'citizenship',
        'department',
        'positionTitle',
        'monthlySalary',
        'prevAdjustDate',
        'annualLeave',
        'dateCommenced',
        'dateBirth',
        'dateCeased',
        'tenureEmployed',
        'qualification',
        'status',
        'marital',
        'spouse',
        'childrenNo',
        'taxFileNo',
        'irdBranch',
        'socsoNo',
        'mailList',
        'enteredBy',
        'dateEntered',
        'updatedBy',
        'dateUpdated',
        'remarks',
        'blank1',
        'blank2',
        'type',
        'id',
        'pass',
        'qbID',
        'permission',
        'epfNo',
        'IsCloudPreload',
        'IsCloudSharedFolder',
        'SalaryAccount',
        'location',
        'DefaultBranch',
        'Phone1Ctr',
        'Phone1No',
        'Phone2Ctr',
        'Phone2No',
        'Phone3Ctr',
        'Phone3No',
        'Fax1Ctr',
        'Fax1No',
        'website'
    ];

    public function branch()
    {
        return $this->belongsTo('App\Models\ProgramOwner', 'DefaultBranch', 'FirmCode');
    }
}
