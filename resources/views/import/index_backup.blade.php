@extends('layout')
@section('content')
    <div class="mb-3">
        <h1 class="h3 d-inline align-middle">Import Data</h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('import.post') }}" method="POST" enctype="multipart/form-data" target="_blank">
                        @csrf
                        <input type="hidden" name="staff_id" value="{{ $staff_id }}"/>
                        {{-- <div class="mb-3">
                            <label class="form-label">Matter ID (System Assigned)</label>
                            <input name="fileNo1_prefix" type="text" class="form-control fileNo1"/>
                        </div> --}}
                        <div class="mb-3">
                            <label class="form-label">Primary Client *</label>
                            <div class="row">
                                <div class="col-11">
                                    <select name="C0" class="form-select select2-customer">
                                    </select>
                                </div>
                                <div class="col-1">
                                    <button class="btn btn-sm btn-outline-danger" type="button" id="c0-clear">X</button>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Matter ID 2 (Manual)</label>
                            <input name="FileNo2" type="text" placeholder="Optional" class="form-control"/>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Branch Office *</label>
                            <select name="Licensee" class="form-select select2">
                                @foreach ($branches as $v)
                                    <option value="{{ $v->FirmCode }}">{{ $v->BranchName }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Matter *</label>
                            <div class="row">
                                <div class="col-11">
                                    <select name="MatterCode" class="form-select select2-matter">
                                    </select>
                                </div>
                                <div class="col-1">
                                    <button class="btn btn-sm btn-outline-danger" type="button" id="matterCode-clear">X</button>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">File Status *</label>
                            <select name="FileStatus" class="form-select select2">
                                @foreach ($fileStatus as $v)
                                    <option value="{{ $v->statusCode }}">{{ $v->status }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Partner In Charge *</label>
                            <select name="PartnerInCharge" class="form-select select2">
                                <option value="0">Select</option>
                                @foreach ($partner as $v)
                                    <option value="{{ $v->staffCode }}">{{ $v->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">LA / Associate in Charge</label>
                            <select name="LAInCharge" class="form-select select2">
                                <option value="0">Select</option>
                                @foreach ($la as $v)
                                    <option value="{{ $v->staffCode }}">{{ $v->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Clerk In Charge *</label>
                            <select name="ClerkInCharge" class="form-select select2">
                                <option value="0">Select</option>
                                @foreach ($clerk as $v)
                                    <option value="{{ $v->staffCode }}">{{ $v->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Team</label>
                            <input name="Team" type="text" class="form-control"/>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Related Matter</label>
                            <div class="row">
                                <div class="col-11">
                                    <select name="RelatedFile" class="form-select select2-file">
                                    </select>
                                </div>
                                <div class="col-1">
                                    <button class="btn btn-sm btn-outline-danger" type="button" id="relatedFile-clear">X</button>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Legal Firm 1</label>
                            <div class="row">
                                <div class="col-11">
                                    <select name="Lawyer1" class="form-select select2-lawyer">
                                    </select>
                                </div>
                                <div class="col-1">
                                    <button class="btn btn-sm btn-outline-danger" type="button" id="lawyer1-clear">X</button>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Legal Firm 2</label>
                            <div class="row">
                                <div class="col-11">
                                    <select name="Lawyer2" class="form-select select2-lawyer">
                                    </select>
                                </div>
                                <div class="col-1">
                                    <button class="btn btn-sm btn-outline-danger" type="button" id="lawyer2-clear">X</button>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Legal Firm 3</label>
                            <div class="row">
                                <div class="col-11">
                                    <select name="Lawyer3" class="form-select select2-lawyer">
                                    </select>
                                </div>
                                <div class="col-1">
                                    <button class="btn btn-sm btn-outline-danger" type="button" id="lawyer3-clear">X</button>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Legal Firm 4</label>
                            <div class="row">
                                <div class="col-11">
                                    <select name="Lawyer4" class="form-select select2-lawyer">
                                    </select>
                                </div>
                                <div class="col-1">
                                    <button class="btn btn-sm btn-outline-danger" type="button" id="lawyer4-clear">X</button>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Matter Location *</label>
                            <input name="FileLoc" type="text" class="form-control"/>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Pocket Folder</label>
                            <input name="PocketFile" type="text" class="form-control"/>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Box / Archive</label>
                            <input name="Box" type="text" class="form-control"/>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Remarks</label>
                            <textarea name="remarks" class="form-control"></textarea>
                            {{-- <input name="remarks" type="text" class="form-control"/> --}}
                        </div>
                        <hr/>
                        <div class="mb-3">
                            <label class="form-label">ID Type for imported contacts</label>
                            <select name="idType" class="form-select select2">
                                <option value="0">None</option>
                                @foreach ($idTypes as $v)
                                    <option value="{{ $v->typeCode }}">{{ $v->label }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Project code for Property</label>
                            <div class="row">
                                <div class="col-11">
                                    <select name="P1" class="form-select select2-property">
                                    </select>
                                </div>
                                <div class="col-1">
                                    <button class="btn btn-sm btn-outline-danger" type="button" id="p1-clear">X</button>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Import Options</label>
                            <select id="IdTypeField" name="exportOptions" class="form-select select2">
                                <option value="1">Get External Data</option>
                                <option value="2">Create New Matters Range</option>
                            </select>
                        </div>
                        <div class="mb-3" id="ExternalDataField">
                            {{-- <label class="form-label">Get External Data *</label> --}}
                            <input name="file" type="file" class="form-control" accept=".csv,.xls,.xlsx"/>
                        </div>
                        <div class="mb-3" id="NoMattersField" style="display: none;">
                            <label class="form-label">No. of Matters *</label>
                            <input name="noOfMatters" type="text" class="form-control"/>
                        </div>
                        <div class="mb-3" id="StartFromField" style="display: none;">
                            <label class="form-label">Start From *</label>
                            <input name="startFrom" type="text" class="form-control"/>
                        </div>
                        <hr/>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('style')
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
<style>

</style>
@stop

@section('script')
<script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>
<script src="{{ asset('js/jquery.mask.min.js') }}"></script>

<script>
    $(document).ready(function() {
        $('.select2').select2();
        $('.select2-customer').select2({
            minimumInputLength: 3,
            ajax: {
                delay: 250, // wait 250 milliseconds before triggering the request
                url: "{{ route('api.customer.search') }}",
                dataType: 'json',
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }

                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    var result = data.results;
                    var more = false;
                    if (result.next_page_url != null) {
                        more = true;
                    }

                    return {
                        results: result.data,
                        pagination: {
                            more: more
                        }
                    };
                }
            },
            escapeMarkup: function(markup) {
                return markup;
            }
        });
        $('#c0-clear').click(function() {
            $(".select2-customer").val(null).trigger("change");
        });

        $('.select2-property').select2({
            minimumInputLength: 3,
            ajax: {
                delay: 250, // wait 250 milliseconds before triggering the request
                url: "{{ route('api.property.search') }}",
                dataType: 'json',
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }

                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    var result = data.results;
                    var more = false;
                    if (result.next_page_url != null) {
                        more = true;
                    }

                    return {
                        results: result.data,
                        pagination: {
                            more: more
                        }
                    };
                }
            },
            escapeMarkup: function(markup) {
                return markup;
            }
        });
        $('#p1-clear').click(function() {
            $(".select2-property").val(null).trigger("change");
        });
        $('.select2-file').select2({
            minimumInputLength: 5,
            ajax: {
                delay: 250, // wait 250 milliseconds before triggering the request
                url: "{{ route('api.file.search') }}",
                dataType: 'json',
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }

                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    var result = data.results;
                    var more = false;
                    if (result.next_page_url != null) {
                        more = true;
                    }

                    return {
                        results: result.data,
                        pagination: {
                            more: more
                        }
                    };
                }
            },
            escapeMarkup: function(markup) {
                return markup;
            }
        });
        $('#relatedFile-clear').click(function() {
            $(".select2-file").val(null).trigger("change");
        });
        $('.select2-matter').select2({
            minimumInputLength: 3,
            ajax: {
                delay: 250, // wait 250 milliseconds before triggering the request
                url: "{{ route('api.matter.search') }}",
                dataType: 'json',
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }

                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    var result = data.results;
                    var more = false;
                    if (result.next_page_url != null) {
                        more = true;
                    }

                    return {
                        results: result.data,
                        pagination: {
                            more: more
                        }
                    };
                }
            },
            escapeMarkup: function(markup) {
                return markup;
            }
        });
        $('#matterCode-clear').click(function() {
            $(".select2-matter").val(null).trigger("change");
        });
        $('.select2-lawyer').select2({
            minimumInputLength: 3,
            ajax: {
                delay: 250, // wait 250 milliseconds before triggering the request
                url: "{{ route('api.lawyer.search') }}",
                dataType: 'json',
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }

                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    var result = data.results;
                    var more = false;
                    if (result.next_page_url != null) {
                        more = true;
                    }

                    return {
                        results: result.data,
                        pagination: {
                            more: more
                        }
                    };
                }
            },
            escapeMarkup: function(markup) {
                return markup;
            }
        });
        $('#lawyer1-clear').click(function() {
            $("select[name='Lawyer1']").val(null).trigger("change");
        });
        $('#lawyer2-clear').click(function() {
            $("select[name='Lawyer2']").val(null).trigger("change");
        });
        $('#lawyer3-clear').click(function() {
            $("select[name='Lawyer3']").val(null).trigger("change");
        });
        $('#lawyer4-clear').click(function() {
            $("select[name='Lawyer4']").val(null).trigger("change");
        });
        $('.fileNo1').mask('0000-');
        $('#IdTypeField').on('change', function() {
            if (this.value == 2) {
                $('#ExternalDataField').hide();
                $('#NoMattersField').show();
                $('#StartFromField').show();
            } else {
                $('#ExternalDataField').show();
                $('#NoMattersField').hide();
                $('#StartFromField').hide();
            }
        });
    });
</script>
@stop
