<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Accountbank
 *
 * @property int $BankID
 * @property string $BankAccNo
 * @property string $BankAccName
 * @property string $BankAccBanker
 * @property bool $isALL
 * @property bool $is1
 * @property bool $is2
 * @property bool $is3
 * @property bool $is4
 * @property bool $is5
 * @property Carbon|null $DateOpen
 * @property Carbon|null $DateClose
 * @property int|null $BankBranchCode
 * @property Carbon|null $dateEntered
 * @property int|null $enteredBy
 * @property Carbon|null $dateUpdated
 * @property int|null $updatedBy
 * @property string|null $remarks
 *
 * @package App\Models
 */
class Accountbank extends Model
{
	protected $table = 'accountbank';
	protected $primaryKey = 'BankID';
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'isALL' => 'bool',
		'is1' => 'bool',
		'is2' => 'bool',
		'is3' => 'bool',
		'is4' => 'bool',
		'is5' => 'bool',
		'BankBranchCode' => 'int',
		'enteredBy' => 'int',
		'updatedBy' => 'int'
	];

	protected $dates = [
		'DateOpen',
		'DateClose',
		'dateEntered',
		'dateUpdated'
	];

	protected $fillable = [
		'BankAccNo',
		'BankAccName',
		'BankAccBanker',
		'isALL',
		'is1',
		'is2',
		'is3',
		'is4',
		'is5',
		'DateOpen',
		'DateClose',
		'BankBranchCode',
		'dateEntered',
		'enteredBy',
		'dateUpdated',
		'updatedBy',
		'remarks'
	];

    public function branch()
    {
        return $this->belongsTo('App\Models\Bankbranchcode', 'BankBranchCode', 'BranchCode');
    }
}
