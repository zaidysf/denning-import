<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\ProgramOwner;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    public function index(Request $req)
    {
        $first = ProgramOwner::first()->FirmCode;
        $page = 10;
        if ($req->has('per_page')) {
            $page = $req->get('per_page');
        }
        if ($req->has('key')) {
            switch ($req->get('key')) {
                case 'name':
                    $data = ProgramOwner::where('FirmCode', '!=', $first)->where('Name', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'branch':
                    $data = ProgramOwner::where('FirmCode', '!=', $first)->where('BranchName', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'city':
                    $data = ProgramOwner::where('FirmCode', '!=', $first)->where('City', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                default:
                    $data = ProgramOwner::where('FirmCode', '!=', $first)->where('BranchName', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;
            }
        } else {
            $data = ProgramOwner::where('FirmCode', '!=', $first)->paginate($page);
        }
        $selectColumn = [
            'name' => 'Name',
            'branch' => 'Branch',
            'city' => 'Town',
        ];
        $selectPagination = [10 => 10, 25 => 25, 50 => 50, 100 => 100];
        return view('setup.branch.index', compact('data', 'selectColumn', 'selectPagination'));
    }

    public function add()
    {
        $data = [];
        $formActionURL = route('setup.branch.store');
        $label = 'Add New Branch';
        $method = 'add';
        return view('setup.firm.index', compact('data', 'formActionURL', 'label', 'method'));
    }

    public function store(Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'Name' => 'required',
            'BranchName' => 'required'
        ]);

        // dd($req->all());

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $otherFirm = ProgramOwner::orderBy('FirmCode', 'desc')->first();
        $model = new ProgramOwner;
        $input = $req->all();
        $input['FirmCode'] = ($otherFirm->FirmCode) + 1;
        $input['DefaultFirm'] = $otherFirm->DefaultFirm;
        $model->fill($input)->save();

        $data = $req->all();

        ProgramOwner::create($data);

        return redirect()->route('setup.branch.index')
        ->with('success', 'Branch has been created.');
    }

    public function edit($id)
    {
        $data = ProgramOwner::find($id);
        $formActionURL = route('setup.branch.update', ['id' => $id]);
        $label = 'Edit Branch - '.$data->BranchName;
        $method = 'edit';
        return view('setup.firm.index', compact('data', 'formActionURL', 'label', 'method'));
    }

    public function update(int $id, Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'Name' => 'required',
            'BranchName' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $model = ProgramOwner::find($id);
        $input = $req->all();
        $model->fill($input)->save();

        return redirect()->route('setup.branch.index')
        ->with('success', 'Branch has been updated.');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {
        $model = ProgramOwner::find($id);
        $model->delete();

        return back()
        ->with('success', 'Branch has been deleted.');
    }
}
