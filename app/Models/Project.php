<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Project
 * 
 * @property int $projectCode
 * @property string|null $projectLicense
 * @property string|null $projectname
 * @property string|null $phase
 * @property Carbon|null $validityLicenseBegin
 * @property Carbon|null $validityLicenseEnd
 * @property string|null $AdvSalesPermitNo
 * @property Carbon|null $permitDateBegin
 * @property Carbon|null $permitDateEnd
 * @property string|null $buildingPlanApproval
 * @property Carbon|null $completionDate
 * @property string|null $location
 * @property int|null $encumbrances
 * @property string|null $approvingAuthority
 * @property string|null $landTenure
 * @property Carbon|null $leaseExpDate
 * @property string|null $type1
 * @property int|null $unit1
 * @property float|null $priceMin1
 * @property float|null $priceMax1
 * @property string|null $type2
 * @property int|null $unit2
 * @property float|null $priceMin2
 * @property float|null $priceMax2
 * @property string|null $type3
 * @property int|null $unit3
 * @property float|null $priceMin3
 * @property float|null $priceMax3
 * @property string|null $type4
 * @property int|null $unit4
 * @property float|null $priceMin4
 * @property float|null $priceMax4
 * @property string|null $type5
 * @property int|null $unit5
 * @property float|null $priceMin5
 * @property float|null $priceMax5
 * @property string|null $note1
 * @property string|null $note2
 * @property int|null $developer
 * @property int|null $Proprietor
 * @property string|null $F14AText
 * @property string|null $SPAText
 * @property string|null $PABMText
 * @property string|null $PAEngText
 * @property string|null $MasterTitle
 * @property string|null $HtaAcc
 * @property int|null $HtaBank
 * @property Carbon|null $dateEntered
 * @property int|null $enteredBy
 * @property Carbon|null $dateUpdated
 * @property int|null $updatedBy
 * @property string|null $remarks
 * @property bool|null $ShowInList
 * @property string|null $Remark
 *
 * @package App\Models
 */
class Project extends Model
{
	protected $table = 'project';
	protected $primaryKey = 'projectCode';
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'encumbrances' => 'int',
		'unit1' => 'int',
		'priceMin1' => 'float',
		'priceMax1' => 'float',
		'unit2' => 'int',
		'priceMin2' => 'float',
		'priceMax2' => 'float',
		'unit3' => 'int',
		'priceMin3' => 'float',
		'priceMax3' => 'float',
		'unit4' => 'int',
		'priceMin4' => 'float',
		'priceMax4' => 'float',
		'unit5' => 'int',
		'priceMin5' => 'float',
		'priceMax5' => 'float',
		'developer' => 'int',
		'Proprietor' => 'int',
		'HtaBank' => 'int',
		'enteredBy' => 'int',
		'updatedBy' => 'int',
		'ShowInList' => 'bool'
	];

	protected $dates = [
		'validityLicenseBegin',
		'validityLicenseEnd',
		'permitDateBegin',
		'permitDateEnd',
		'completionDate',
		'leaseExpDate',
		'dateEntered',
		'dateUpdated'
	];

	protected $fillable = [
		'projectLicense',
		'projectname',
		'phase',
		'validityLicenseBegin',
		'validityLicenseEnd',
		'AdvSalesPermitNo',
		'permitDateBegin',
		'permitDateEnd',
		'buildingPlanApproval',
		'completionDate',
		'location',
		'encumbrances',
		'approvingAuthority',
		'landTenure',
		'leaseExpDate',
		'type1',
		'unit1',
		'priceMin1',
		'priceMax1',
		'type2',
		'unit2',
		'priceMin2',
		'priceMax2',
		'type3',
		'unit3',
		'priceMin3',
		'priceMax3',
		'type4',
		'unit4',
		'priceMin4',
		'priceMax4',
		'type5',
		'unit5',
		'priceMin5',
		'priceMax5',
		'note1',
		'note2',
		'developer',
		'Proprietor',
		'F14AText',
		'SPAText',
		'PABMText',
		'PAEngText',
		'MasterTitle',
		'HtaAcc',
		'HtaBank',
		'dateEntered',
		'enteredBy',
		'dateUpdated',
		'updatedBy',
		'remarks',
		'ShowInList',
		'Remark'
	];
}
