<?php
namespace App\Helpers;

use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Log;

class LogHelper
{
    protected static function setScope(array $attributes) {
        if (!empty($attributes)) {
            \Sentry\configureScope(function (\Sentry\State\Scope $scope) use ($attributes): void {
                $scope->setTag('application.name', env('APP_NAME', 'dennings-api'));
                if (isset($attributes['tag'])) {
                    foreach ($attributes['tag'] as $k => $v) {
                        $scope->setTag($k, $v);
                    }
                }
                if (isset($attributes['context'])) {
                    $scope->setContext($attributes['context']['title'], $attributes['context']['data']);
                }
            });
        } else {
            \Sentry\configureScope(function (\Sentry\State\Scope $scope) use ($attributes): void {
                $scope->setTag('application.name', env('APP_NAME', 'dennings-api'));
            });
        }
    }

    public static function makeException(string $message, int $code, array $attributes = [])
    {
        self::setScope($attributes);
        throw new GeneralException($message, $code);
    }

    // logLevel doc = https://laravel.com/docs/8.x/logging#writing-log-messages
    public static function makeLog(string $message, string $logLevel, array $attributes = [])
    {
        self::setScope($attributes);
        Log::{$logLevel}($message);
    }

    public static function getExampleRequests($type = 'log')
    {
        if ($type == 'log') {
            return "LogHelper::makeLog('test message', 'info', [
                'tag' => [
                    'module' => 'example-module'
                ],
                'context' => [
                    'title' => 'character',
                    'data' => [
                        'foo' => 'bar',
                        'john' => 'doe'
                    ]
                ]
            ]);";
        } else {
            return "LogHelper::makeException('test message', 500, [
                'tag' => [
                    'module' => 'example-module'
                ],
                'context' => [
                    'title' => 'character',
                    'data' => [
                        'foo' => 'bar',
                        'john' => 'doe'
                    ]
                ]
            ]);";
        }
    }
}
