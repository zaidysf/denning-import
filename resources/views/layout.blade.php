<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Denning import and export feature">
    <meta name="author" content="zaidysf">
    <meta name="keywords"
        content="denning, import, export, law, lawyer">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Denning Premium</title>

    <link rel="shortcut icon" href="{{ asset('img/logo.jpg') }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="{{ asset('css/app-before.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
    <style>
        #title-right {
            opacity: 1;
            transition: opacity 1s;
        }

        #title-right.hide {
            opacity: 0;
        }
    </style>
    @yield('style')
</head>

<body>
    <div class="wrapper">
        @if (!Request::is('mini/*'))
        <nav id="sidebar" class="sidebar js-sidebar">
            <div class="sidebar-content js-simplebar">
                <a class="sidebar-brand" href="{{ url('/') }}">
                    <span class="align-middle">Denning Premium</span>
                </a>

                <ul class="sidebar-nav">
                    <li class="sidebar-item {{ (Request::is('setup') || Request::is('setup/*') ? 'active' : '') }}">
                        <a class="sidebar-link" href="{{ route('setup.index') }}">
                            <i class="align-middle" data-feather="settings"></i> <span
                                class="align-middle">Initial Setup</span>
                        </a>
                    </li>

                    <li class="sidebar-item {{ (Request::is('file') || Request::is('file/*') ? 'active' : '') }}">
                        <a class="sidebar-link" href="{{ route('file.index') }}">
                            <i class="align-middle" data-feather="sliders"></i> <span
                                class="align-middle">Matter List</span>
                        </a>
                    </li>

                    <li class="sidebar-header">
                        Import Data
                    </li>

                    <li class="sidebar-item {{ (Request::is('import') || Request::is('import/*') ? 'active' : '') }}">
                        <a class="sidebar-link" href="{{ route('import.index', ['staffId' => 3]) }}">
                            <i class="align-middle" data-feather="sliders"></i> <span
                                class="align-middle">Import Matter (Files)</span>
                        </a>
                    </li>

                    <li class="sidebar-item {{ (Request::is('list') || Request::is('list/*') ? 'active' : '') }}">
                        <a class="sidebar-link" href="{{ route('import.list') }}">
                            <i class="align-middle" data-feather="sliders"></i> <span
                                class="align-middle">Import Batch List</span>
                        </a>
                    </li>

                    <li class="sidebar-header">
                        Export Data
                    </li>

                    <li class="sidebar-item {{ (Request::is('export') || Request::is('export/*') ? 'active' : '') }}">
                        <a class="sidebar-link" href="{{ route('export.index') }}">
                            <i class="align-middle" data-feather="sliders"></i> <span
                                class="align-middle">Export Data</span>
                        </a>
                    </li>

                    <li class="sidebar-header">
                        Bulk Mail Merge
                    </li>

                    <li class="sidebar-item {{ (Request::is('export') || Request::is('export/*') ? 'active' : '') }}">
                        <a class="sidebar-link" href="{{ route('export.index') }}">
                            <i class="align-middle" data-feather="sliders"></i> <span
                                class="align-middle">Bulk Generation</span>
                        </a>
                    </li>

                    <li class="sidebar-item {{ (Request::is('mail-merge') || Request::is('mail-merge/*') ? 'active' : '') }}">
                        <a class="sidebar-link" href="{{ route('mail-merge.template.index') }}">
                            <i class="align-middle" data-feather="sliders"></i> <span
                                class="align-middle">Bulk Templates List</span>
                        </a>
                    </li>

                    <li class="sidebar-item {{ (Request::is('bulk-template-type') || Request::is('bulk-template-type/*') ? 'active' : '') }}">
                        <a class="sidebar-link" href="{{ route('bulk-template-type.index') }}">
                            <i class="align-middle" data-feather="sliders"></i> <span
                                class="align-middle">Bulk Template Type</span>
                        </a>
                    </li>

                    {{-- <li class="sidebar-item {{ (Request::is('filemanager') || Request::is('filemanager/*') ? 'active' : '') }}">
                        <a class="sidebar-link" href="{{ route('filemanager.index') }}">
                            <i class="align-middle" data-feather="sliders"></i> <span
                                class="align-middle">Templates Manager</span>
                        </a>
                    </li> --}}
                </ul>
            </div>
        </nav>
        @endif

        <div class="main">
            @if (!Request::is('mini/*'))
            <nav class="navbar navbar-expand navbar-light navbar-bg">
                <a class="sidebar-toggle js-sidebar-toggle">
                    <i class="hamburger align-self-center"></i>
                </a>

                <h3 id="title-right" class="pt-2 hide">Denning Premium</h3>

                <div class="navbar-collapse collapse">
                    <ul class="navbar-nav navbar-align">
                        {{-- <li class="nav-item dropdown">
                            <a class="nav-icon dropdown-toggle" href="#" id="alertsDropdown" data-bs-toggle="dropdown">
                                <div class="position-relative">
                                    <i class="align-middle" data-feather="bell"></i>
                                    <span class="indicator">4</span>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end py-0"
                                aria-labelledby="alertsDropdown">
                                <div class="dropdown-menu-header">
                                    4 New Notifications
                                </div>
                                <div class="list-group">
                                    <a href="#" class="list-group-item">
                                        <div class="row g-0 align-items-center">
                                            <div class="col-2">
                                                <i class="text-danger" data-feather="alert-circle"></i>
                                            </div>
                                            <div class="col-10">
                                                <div class="text-dark">Update completed</div>
                                                <div class="text-muted small mt-1">Restart server 12 to complete the
                                                    update.</div>
                                                <div class="text-muted small mt-1">30m ago</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <div class="row g-0 align-items-center">
                                            <div class="col-2">
                                                <i class="text-warning" data-feather="bell"></i>
                                            </div>
                                            <div class="col-10">
                                                <div class="text-dark">Lorem ipsum</div>
                                                <div class="text-muted small mt-1">Aliquam ex eros, imperdiet vulputate
                                                    hendrerit et.</div>
                                                <div class="text-muted small mt-1">2h ago</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <div class="row g-0 align-items-center">
                                            <div class="col-2">
                                                <i class="text-primary" data-feather="home"></i>
                                            </div>
                                            <div class="col-10">
                                                <div class="text-dark">Login from 192.186.1.8</div>
                                                <div class="text-muted small mt-1">5h ago</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <div class="row g-0 align-items-center">
                                            <div class="col-2">
                                                <i class="text-success" data-feather="user-plus"></i>
                                            </div>
                                            <div class="col-10">
                                                <div class="text-dark">New connection</div>
                                                <div class="text-muted small mt-1">Christina accepted your request.
                                                </div>
                                                <div class="text-muted small mt-1">14h ago</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="dropdown-menu-footer">
                                    <a href="#" class="text-muted">Show all notifications</a>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-icon dropdown-toggle" href="#" id="messagesDropdown"
                                data-bs-toggle="dropdown">
                                <div class="position-relative">
                                    <i class="align-middle" data-feather="message-square"></i>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end py-0"
                                aria-labelledby="messagesDropdown">
                                <div class="dropdown-menu-header">
                                    <div class="position-relative">
                                        4 New Messages
                                    </div>
                                </div>
                                <div class="list-group">
                                    <a href="#" class="list-group-item">
                                        <div class="row g-0 align-items-center">
                                            <div class="col-2">
                                                <img src="img/avatars/avatar-5.jpg"
                                                    class="avatar img-fluid rounded-circle" alt="Vanessa Tucker">
                                            </div>
                                            <div class="col-10 ps-2">
                                                <div class="text-dark">Vanessa Tucker</div>
                                                <div class="text-muted small mt-1">Nam pretium turpis et arcu. Duis arcu
                                                    tortor.</div>
                                                <div class="text-muted small mt-1">15m ago</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <div class="row g-0 align-items-center">
                                            <div class="col-2">
                                                <img src="img/avatars/avatar-2.jpg"
                                                    class="avatar img-fluid rounded-circle" alt="William Harris">
                                            </div>
                                            <div class="col-10 ps-2">
                                                <div class="text-dark">William Harris</div>
                                                <div class="text-muted small mt-1">Curabitur ligula sapien euismod
                                                    vitae.</div>
                                                <div class="text-muted small mt-1">2h ago</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <div class="row g-0 align-items-center">
                                            <div class="col-2">
                                                <img src="img/avatars/avatar-4.jpg"
                                                    class="avatar img-fluid rounded-circle" alt="Christina Mason">
                                            </div>
                                            <div class="col-10 ps-2">
                                                <div class="text-dark">Christina Mason</div>
                                                <div class="text-muted small mt-1">Pellentesque auctor neque nec urna.
                                                </div>
                                                <div class="text-muted small mt-1">4h ago</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <div class="row g-0 align-items-center">
                                            <div class="col-2">
                                                <img src="img/avatars/avatar-3.jpg"
                                                    class="avatar img-fluid rounded-circle" alt="Sharon Lessman">
                                            </div>
                                            <div class="col-10 ps-2">
                                                <div class="text-dark">Sharon Lessman</div>
                                                <div class="text-muted small mt-1">Aenean tellus metus, bibendum sed,
                                                    posuere ac, mattis non.</div>
                                                <div class="text-muted small mt-1">5h ago</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="dropdown-menu-footer">
                                    <a href="#" class="text-muted">Show all messages</a>
                                </div>
                            </div>
                        </li> --}}
                        <li class="nav-item dropdown">
                            <a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#"
                                data-bs-toggle="dropdown">
                                <i class="align-middle" data-feather="settings"></i>
                            </a>

                            <a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#"
                                data-bs-toggle="dropdown">
                                {{-- <img src="{{ url('/img/avatars/avatar.jpg') }}" class="avatar img-fluid rounded me-1"
                                    alt="{{ Auth::user()->name }}" />  --}}
                                <span class="text-dark">{{ Auth::user()->name }}</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a class="dropdown-item" href="#"><i class="align-middle me-1"
                                        data-feather="user"></i> Profile</a>
                                {{-- <a class="dropdown-item" href="#"><i class="align-middle me-1"
                                        data-feather="pie-chart"></i> Analytics</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="pages-settings.html"><i class="align-middle me-1"
                                        data-feather="settings"></i> Settings & Privacy</a>
                                <a class="dropdown-item" href="#"><i class="align-middle me-1"
                                        data-feather="help-circle"></i> Help Center</a> --}}
                                <div class="dropdown-divider"></div>
                                {{-- <a class="dropdown-item" href="#"> --}}
                                    <form method="POST" action="{{ route('logout') }}">
                                        @csrf

                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                            this.closest('form').submit();">
                                            {{ __('Log Out') }}
                                        </a>
                                    </form>
                                {{-- </a> --}}
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
            @endif

            <main class="content">
                <div class="container-fluid p-0">
                    @yield('content')
                </div>
            </main>

            @if (!Request::is('mini/*'))
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row text-muted">
                        <div class="col-6 text-start">
                            <p class="mb-0">
                                <a class="text-muted" href="#" target="_blank">
                                    <strong>Denning</strong></a> &copy;
                            </p>
                        </div>
                        <div class="col-6 text-end">
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a class="text-muted" href="#" target="_blank">Support</a>
                                </li>
                                <li class="list-inline-item">
                                    <a class="text-muted" href="#" target="_blank">Help Center</a>
                                </li>
                                <li class="list-inline-item">
                                    <a class="text-muted" href="#" target="_blank">Privacy</a>
                                </li>
                                <li class="list-inline-item">
                                    <a class="text-muted" href="#" target="_blank">Terms</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
            @endif
        </div>
    </div>

    <script src="{{ asset('js/app-before.js') }}"></script>
    <script>
        var elems = document.querySelectorAll('.sidebar-toggle');
        elems[0].addEventListener('click', fnn, false);
        function fnn() {
            var element = document.getElementById('sidebar');
            var title = document.getElementById('title-right');
            if (!element.classList.contains('collapsed')) {
                console.log('1');
                if(title.classList.contains("hide")){
                    title.classList.remove("hide");
                }
            } else {
                console.log('2');
                if(!title.classList.contains("hide")){
                    title.classList.add("hide");
                }
            }
        }
    </script>
    @yield('script')

</body>

</html>
