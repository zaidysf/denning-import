<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Idtype;
use App\Models\ProgramOwner;
use App\Models\Staff;
use Illuminate\Http\Request;

class LoginPasswordController extends Controller
{
    public function index(Request $req)
    {
        $page = 10;
        if ($req->has('per_page')) {
            $page = $req->get('per_page');
        }
        if ($req->has('key')) {
            switch ($req->get('key')) {
                case 'name':
                    $data = Staff::where('name', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'initials':
                    $data = Staff::where('initials', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'branch':
                    $branch = ProgramOwner::where('BranchName', 'LIKE', '%'.$req->get('value').'%')->get()->pluck('FirmCode');
                    $data = Staff::whereIn('DefaultBranch', $branch)->paginate($page);
                    break;

                case 'position':
                    $data = Staff::where('positionTitle', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'department':
                    $data = Staff::where('department', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'status':
                    $data = Staff::where('status', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'id':
                    $data = Staff::where('id', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                default:
                    $data = Staff::where('id', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;
            }
        } else {
            $data = Staff::paginate($page);
        }
        $selectColumn = [
            'name' => 'Name',
            'initials' => 'Nickname',
            'branch' => 'Branch',
            'position' => 'Position',
            'department' => 'Department',
            'status' => 'Status',
            'id' => 'Login ID',
        ];
        $selectPagination = [10 => 10, 25 => 25, 50 => 50, 100 => 100];
        return view('setup.login_password.index', compact('data', 'selectColumn', 'selectPagination'));
    }

    public function store(Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'name' => 'required|unique:bulk_template_types',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $model = new Staff;
        $model->name = $req->name;
        $model->save();

        return back()
        ->with('success', 'Login ID has been created.')
        ->with('name', $req->name);
    }

    public function edit($id)
    {
        $data = Staff::find($id);
        $formActionURL = route('setup.login-password.update', ['id' => $id]);
        $label = 'Edit Login ID - '.$data->name;
        $method = 'edit';
        $branches = ProgramOwner::all();
        $idTypes = Idtype::all();
        return view('setup.login_password.add', compact('data', 'formActionURL', 'label', 'method', 'branches'));
    }

    public function update(int $id, Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $model = Staff::find($id);
        $model->id = $req->id;
        $model->save();

        return back()
        ->with('success', 'Login ID has been updated.');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {
        $model = Staff::find($id);
        $model->delete();

        return back()
        ->with('success', 'Login ID has been deleted.');
    }
}
