<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Litigationstage
 *
 * @property string $StageCode
 * @property string $ManualCode
 * @property string $Description
 * @property int $TimeEstimate
 * @property string|null $ReferToDate
 * @property string|null $IntegrateWith
 * @property string|null $Category
 * @property int|null $CategoryID
 * @property string $lblChk1
 * @property string $lblChk2
 * @property string $lblCur1
 * @property string $lblCur2
 * @property string $lblCur3
 * @property string $lblCur4
 * @property string $lblCur5
 * @property string $lblText1
 * @property string $lblText2
 * @property string $lblText3
 * @property string $lblText4
 * @property string $lblText5
 * @property string|null $Remark
 * @property int|null $enteredBy
 * @property Carbon|null $dateEntered
 * @property int|null $updatedBy
 * @property string|null $remarks
 * @property Carbon|null $dateUpdated
 *
 * @package App\Models
 */
class Litigationstage extends Model
{
    protected $table = 'litigationstage';
    protected $primaryKey = 'StageCode';
    public $incrementing = false;
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'TimeEstimate' => 'int',
        'CategoryID' => 'int',
        'enteredBy' => 'int',
        'updatedBy' => 'int'
    ];

    protected $dates = [
        'dateEntered',
        'dateUpdated'
    ];

    protected $fillable = [
        'ManualCode',
        'Description',
        'TimeEstimate',
        'ReferToDate',
        'IntegrateWith',
        'Category',
        'CategoryID',
        'lblChk1',
        'lblChk2',
        'lblCur1',
        'lblCur2',
        'lblCur3',
        'lblCur4',
        'lblCur5',
        'lblText1',
        'lblText2',
        'lblText3',
        'lblText4',
        'lblText5',
        'Remark',
        'enteredBy',
        'dateEntered',
        'updatedBy',
        'remarks',
        'dateUpdated'
    ];

    public function getCodeNameAttribute()
    {
        return $this->StageCode;
    }
}
