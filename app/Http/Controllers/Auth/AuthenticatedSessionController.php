<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Validation\ValidationException;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        if ($request->get('email') == 'omargill@test.com' || $request->get('email') == 'megat@test.com') {
            $request->authenticate();

            // if (! Auth::attempt($this->only('email', 'password'), $this->boolean('remember'))) {
            //     RateLimiter::hit($this->throttleKey());

            //     throw ValidationException::withMessages([
            //         'email' => __('auth.failed'),
            //     ]);
            // }
        } else {
            $request->ensureIsNotRateLimited();

            $loginUrl = 'https://denningonline.com.my/denningapi/v1/web/signIn';
            $header = [
                'Content-Type' => 'application/json',
                'webuser-sessionid' => '{334E910C-CC68-4784-9047-0F23D37C9CF9}',
                'webuser-id' => 'online@denning.com.my'
            ];
            // $mac = exec('getmac');
            $browser = $request->header('User-Agent');
            if (str_contains($browser, "Firefox")) {
                $browser = "Mozilla Firefox";
            // "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0"
            } else if (str_contains($browser, "SamsungBrowser")) {
                $browser = "Samsung Internet";
            // "Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-G955F Build/PPR1.180610.011) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.4 Chrome/67.0.3396.87 Mobile Safari/537.36
            } else if (str_contains($browser, "Opera") || str_contains($browser, "OPR")) {
                $browser = "Opera";
            // "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106"
            } else if (str_contains($browser, "Trident")) {
                $browser = "Microsoft Internet Explorer";
            // "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; Zoom 3.6.0; wbx 1.0.0; rv:11.0) like Gecko"
            } else if (str_contains($browser, "Edge")) {
                $browser = "Microsoft Edge";
            // "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge/16.16299"
            } else if (str_contains($browser, "Chrome")) {
                $browser = "Google Chrome or Chromium";
            // "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36"
            } else if (str_contains($browser, "Safari")) {
                $browser = "Apple Safari";
            // "Mozilla/5.0 (iPhone; CPU iPhone OS 11_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1 980x1306"
            } else {
                $browser = "Unknown";
            }
            $body = [
                'ipWAN' => $request->ip(),
                'ipLAN' => $request->ip(),
                'OS' => 'Windows NT 10.0; Win64; x64',
                'device' => 'Browser',
                'deviceName' => $browser,
                // 'MAC' => substr($mac, 0, strpos($mac, " ")),
                'MAC' => $browser,
                'email' => $request->get('email'),
                'password' => $request->get('password')
            ];

            $lastUrl = "";
            $loginReq = json_decode(Http::withHeaders($header)->post($loginUrl, $body), true);
            if ($loginReq['statusCode'] != 250 && $loginReq['statusCode'] != 200) {
                RateLimiter::hit($request->throttleKey());
                throw ValidationException::withMessages([
                    'email' => __('auth.failed'),
                ]);
            }

            $res = $loginReq;
            if ($loginReq['statusCode'] == 250) {
                unset($body['password']);
                $body['activationCode'] = '654321';
                $tacUrl = 'https://denningonline.com.my/denningapi/v1/SMS/newDevice';
                $tacReq = json_decode(Http::withHeaders($header)->post($tacUrl, $body), true);
                if ($tacReq['statusCode'] == 200) {
                    if (isset($tacReq['catPersonal']) && !empty($tacReq['catPersonal'])) {
                        $res = $tacReq['catPersonal'][0];
                        $lastUrl = $tacReq['catPersonal'][0]['APIServer'];
                    } else {
                        $res = $tacReq['catDenning'][0];
                        $lastUrl = $tacReq['catDenning'][0]['APIServer'];
                    }
                } else {
                    RateLimiter::hit($request->throttleKey());
                    throw ValidationException::withMessages([
                        'email' => __('auth.failed'),
                    ]);
                }
            } else {
                $res = $loginReq;
                if (isset($loginReq['catPersonal']) && !empty($loginReq['catPersonal'])) {
                    $res = $loginReq['catPersonal'][0];
                    $lastUrl = $loginReq['catPersonal'][0]['APIServer'];
                } else {
                    $res = $loginReq['catDenning'][0];
                    $lastUrl = $loginReq['catDenning'][0]['APIServer'];
                }
            }


            $body['password'] = $request->get('password');
            $body['sessionID'] = $loginReq['sessionID'];
            $lastReq = json_decode(Http::withHeaders($header)->post($lastUrl.'/v1/web/staffLogin', $body), true);
            if ($lastReq['statusCode'] != 200) {
                RateLimiter::hit($request->throttleKey());
                throw ValidationException::withMessages([
                    'email' => __('auth.failed'),
                ]);
            }

            $checkUser = User::where('email', $request->get('email'))->first();
            if (!$checkUser) {
                $checkUser = new User;
                $checkUser->name = $lastReq['staffProfile']['strName'];
                $checkUser->email = $request->get('email');
                $checkUser->password = Hash::make($request->get('password'));
                $checkUser->denningonline = json_encode($lastReq);
                $checkUser->db_server = $res['DBServer'];
                $checkUser->db_port = $res['DBPort'];
                $checkUser->ftp_server = $res['FtpServer'];
                $checkUser->ftp_port = empty($res['FtpPort']) ? 0 : $res['FtpPort'];
                $checkUser->ftp_dir = $res['FtpDirectory'];
                $checkUser->save();
            } else {
                $checkUser = User::find($checkUser->id);
                $checkUser->password = Hash::make($request->get('password'));
                $checkUser->denningonline = json_encode($lastReq);
                $checkUser->db_server = $res['DBServer'];
                $checkUser->db_port = $res['DBPort'];
                $checkUser->ftp_server = $res['FtpServer'];
                $checkUser->ftp_port =  empty($res['FtpPort']) ? 0 : $res['FtpPort'];
                $checkUser->ftp_dir = $res['FtpDirectory'];
                $checkUser->save();
            }


            Auth::login($checkUser, $request->boolean('remember'));

            // if (! Auth::attempt($this->only('email', 'password'), $this->boolean('remember'))) {
            //     RateLimiter::hit($this->throttleKey());

            //     throw ValidationException::withMessages([
            //         'email' => __('auth.failed'),
            //     ]);
            // }
        }

        RateLimiter::clear($request->throttleKey());

        $request->session()->regenerate();

        // $request->session()->put('denningonline', $lastReq);

        // abort_if(!$request->session()->has('denningonline') ||
        //     !$request->session()->has('DBServer') ||
        //     !$request->session()->has('DBPort'), 404);

        // dd(Config::get('database.connections.mysql'));
        // Config::set("database.connections.mysql.host", $loginReq['catPersonal'][0]['DBServer']);
        // Config::set("database.connections.mysql.port", $loginReq['catPersonal'][0]['DBPort']);
        // Config::set("database.connections.mysql.username", 'Xwj4T4+C86NT6qav');
        // Config::set("database.connections.mysql.password", 'gv4+_W@h)F8hsMU1');
        // Config::set("database.connections.mysql.host", [
        //     "driver" => "mysql",
        //     "host" => $request->session()->get('DBServer'),
        //     "port" => $request->session()->get('DBPort'),
        //     'database' => 'des',
        //     'username' => 'RwlWgyMf2+ao+Ulw',
        //     'password' => 'vZEfCJp+2c32D7p0',
        //     'unix_socket' => '',
        //     'charset' => 'utf8mb4',
        //     'collation' => 'utf8mb4_unicode_ci',
        //     'prefix' => '',
        //     'prefix_indexes' => true,
        //     'strict' => true,
        //     'engine' => null,
        //     'options' => extension_loaded('pdo_mysql') ? array_filter([
        //         PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
        //     ]) : [],
        // ]);
        // DB::purge('mysql');
        // DB::reconnect('mysql');

        // $request->session()->put('DBServer', );
        // $request->session()->put('DBPort', $loginReq['catPersonal'][0]['DBPort']);
        return redirect()->intended(RouteServiceProvider::HOME);
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
