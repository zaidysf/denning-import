<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Bankbranchcode
 *
 * @property string|null $BankCode
 * @property int|null $CacCode
 * @property int $BranchCode
 * @property string|null $BranchName
 * @property string|null $Add1
 * @property string|null $Add2
 * @property string|null $Add3
 * @property string|null $Postcode
 * @property string|null $City
 * @property string|null $State
 * @property string|null $Country
 * @property string|null $Phone1Manager
 * @property string|null $Phone2Credit
 * @property string|null $Phone3Operation
 * @property string|null $Fax
 * @property string|null $Email
 * @property string|null $Manager
 * @property string|null $NewIC
 * @property string|null $CreditOfficer
 * @property string|null $MailList
 * @property string|null $NativeBank
 * @property string|null $Note1
 * @property string|null $Note2
 * @property string|null $Note3
 * @property Carbon|null $dateEntered
 * @property int|null $enteredBy
 * @property Carbon|null $dateUpdated
 * @property int|null $updatedBy
 * @property string|null $remarks
 * @property bool|null $ShowInList
 * @property string|null $Phone1Ctr
 * @property string|null $Phone1No
 * @property string|null $Phone2Ctr
 * @property string|null $Phone2No
 * @property string|null $Phone3Ctr
 * @property string|null $Phone3No
 * @property string|null $Fax1Ctr
 * @property string|null $Fax1No
 *
 * @package App\Models
 */
class Bankbranchcode extends Model
{
	protected $table = 'bankbranchcode';
	protected $primaryKey = 'BranchCode';
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'CacCode' => 'int',
		'enteredBy' => 'int',
		'updatedBy' => 'int',
		'ShowInList' => 'bool'
	];

	protected $dates = [
		'dateEntered',
		'dateUpdated'
	];

	protected $fillable = [
		'BankCode',
		'CacCode',
		'BranchName',
		'Add1',
		'Add2',
		'Add3',
		'Postcode',
		'City',
		'State',
		'Country',
		'Phone1Manager',
		'Phone2Credit',
		'Phone3Operation',
		'Fax',
		'Email',
		'Manager',
		'NewIC',
		'CreditOfficer',
		'MailList',
		'NativeBank',
		'Note1',
		'Note2',
		'Note3',
		'dateEntered',
		'enteredBy',
		'dateUpdated',
		'updatedBy',
		'remarks',
		'ShowInList',
		'Phone1Ctr',
		'Phone1No',
		'Phone2Ctr',
		'Phone2No',
		'Phone3Ctr',
		'Phone3No',
		'Fax1Ctr',
		'Fax1No'
	];

    public function bank()
    {
        return $this->belongsTo('App\Models\Bankcode', 'BankCode', 'BankCode');
    }
}
