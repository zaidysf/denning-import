<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\BulkTemplateType;

class BulkTemplateTypeController extends Controller
{
    public function __construct()
    {
        //
    }

    public function find($id)
    {
        $data = BulkTemplateType::find($id);
        return ['data' => $data];
    }
}
