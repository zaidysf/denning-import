<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\BatchImports;
use Illuminate\Http\Request;

class BatchImportsController extends Controller
{
    public function __construct()
    {
        //
    }

    public function search(Request $request)
    {
        $batchImports = BatchImports::selectRaw("CONCAT_WS(' - ', id, customer.name) AS text, id")
        ->leftJoin('customer', 'batch_imports.C0', '=', 'customer.customerCode')
        ->with(['customer:name'])
        ->where('id', 'LIKE', '%'.$request->input('term', '').'%')
        ->orWhereHas('customer', function ($query) use ($request) {
            $query->where('name', 'like', '%' .$request->input('term', '') . '%');
        })
        ->simplePaginate(10);
        // dd($batchImports->toJson());
        $batchImports->appends(['term' => $request->input('term', '')]);
        return ['results' => $batchImports];
    }
}
