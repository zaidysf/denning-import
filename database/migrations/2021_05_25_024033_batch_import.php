<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BatchImport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batch_imports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('batch_no');
            $table->smallInteger('staff_id');
            $table->integer('matter');
            $table->integer('c0');
            $table->string('file_path');
            $table->integer('count');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batch_imports');
    }
}
