<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Cbotitle;
use App\Models\Idtype;
use App\Models\ProgramOwner;
use App\Models\Staff;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    public function index(Request $req)
    {
        $page = 10;
        if ($req->has('per_page')) {
            $page = $req->get('per_page');
        }
        if ($req->has('key')) {
            switch ($req->get('key')) {
                case 'name':
                    $data = Staff::where('name', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'initials':
                    $data = Staff::where('initials', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'branch':
                    $branch = ProgramOwner::where('BranchName', 'LIKE', '%'.$req->get('value').'%')->get()->pluck('FirmCode');
                    $data = Staff::whereIn('DefaultBranch', $branch)->paginate($page);
                    break;

                case 'position':
                    $data = Staff::where('positionTitle', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'department':
                    $data = Staff::where('department', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'status':
                    $data = Staff::where('status', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'email':
                    $data = Staff::where('emailAdd', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                case 'phoneMobile':
                    $data = Staff::where('phoneMobile', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;

                default:
                    $data = Staff::where('name', 'LIKE', '%'.$req->get('value').'%')->paginate($page);
                    break;
            }
        } else {
            $data = Staff::paginate($page);
        }
        $selectColumn = [
            'name' => 'Name',
            'initials' => 'Nickname',
            'branch' => 'Branch',
            'position' => 'Position',
            'department' => 'Department',
            'status' => 'Status',
            'email' => 'Email',
            'phoneMobile' => 'Phone',
        ];
        $selectPagination = [10 => 10, 25 => 25, 50 => 50, 100 => 100];
        $idTypes = Idtype::all();
        return view('setup.staff.index', compact('data', 'selectColumn', 'selectPagination', 'idTypes'));
    }

    public function add()
    {
        $data = [];
        $formActionURL = route('setup.staff.store');
        $label = 'Add New Staff';
        $method = 'add';
        $branches = ProgramOwner::all();
        $idTypes = Idtype::all();
        $titles = Cbotitle::all();
        return view('setup.staff.add', compact('data', 'formActionURL', 'label', 'method', 'branches', 'idTypes', 'titles'));
    }

    public function store(Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'initials' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $model = new Staff;
        $input = $req->all();
        $input['staffCode'] = Staff::orderBy('staffCode', 'desc')->first()->staffCode + 1;
        $model->fill($input)->save();

        return redirect()->route('setup.staff.index')
        ->with('success', 'Staff has been created.');
    }

    public function edit($id)
    {
        $data = Staff::find($id);
        $formActionURL = route('setup.staff.update', ['id' => $id]);
        $label = 'Edit Staff - '.$data->name;
        $method = 'edit';
        $branches = ProgramOwner::all();
        $idTypes = Idtype::all();
        $titles = Cbotitle::all();
        return view('setup.staff.add', compact('data', 'formActionURL', 'label', 'method', 'idTypes', 'titles', 'branches'));
    }

    public function update(int $id, Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'initials' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $model = Staff::find($id);
        $input = $req->all();
        $model->fill($input)->save();

        return back()
        ->with('success', 'Staff has been updated.');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {
        $model = Staff::find($id);
        $model->delete();

        return back()
        ->with('success', 'Staff has been deleted.');
    }
}
