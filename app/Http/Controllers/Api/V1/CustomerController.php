<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function __construct()
    {
        //
    }

    public function search(Request $request)
    {
        $customer = Customer::selectRaw("CONCAT(name, ' - ', idNo) as text, customerCode as id")
            ->where('name', 'LIKE', '%'.$request->input('term', '').'%')
            ->orWhere('idNo', 'LIKE', '%'.$request->input('term', '').'%')
            ->simplePaginate(10);
        $customer->appends(['term' => $request->input('term', '')]);
        return ['results' => $customer];
    }
}
