@extends('layout')
@section('content')
    <div class="mb-3">
        <h1 class="h3 d-inline align-middle">Export Data &amp; Bulk Mail Merge</h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Bulk Mail Merge</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    {{-- <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">ID</th>
                                                <th scope="col">Name</th>
                                                <th scope="col">Type</th>
                                                <th scope="col">Category</th>
                                                <th scope="col">Uploaded By</th>
                                                <th scope="col">Date created</th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i = 0; @endphp
                                            @foreach ($templates as $v)
                                                @php $i++; @endphp
                                                <tr>
                                                    <th scope="row">{{ $v->template_id }}</th>
                                                    <td>{{ $v->name }}</td>
                                                    <td><a href="/storage/{{ $v->file_path }}">.docx</a></td>
                                                    <td>{{ $v->cat->Category }}</td>
                                                    <td>{{ $v->createdBy->name }}</td>
                                                    <td>{{ $v->created_at }}</td>
                                                    <td>
                                                        <a href="javascript:void(0);" class="btn btn-primary" onclick="generate({{ $v->id }})">
                                                            Generate
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="{{ route('export.post') }}" method="POST" id="exportForm">
                        @csrf
                        <input type="hidden" id="field-mailMerge" name="mailMerge" value="0"/>
                        <input type="hidden" id="field-templateId" name="templateId" value="0"/>
                        <h4>Filter</h4>
                        <h6>(Select one only)</h6>
                        <div class="mb-3 p-3" style="border: 3px solid #CED4DA">
                            <h5>Matter Range</h5>
                            <div class="row">
                                <div class="col">
                                    <input name="start_from" placeholder="Start From (Matter No.)" type="text" class="form-control"/>
                                    <div id="emailHelp" class="form-text">e.g 1000-0001</div>
                                </div>
                                <div class="col">
                                    <input name="sum" placeholder="No. of Matters" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 p-3" style="border: 3px solid #CED4DA">
                            <h5>Single Matter</h5>
                            <div class="row">
                                <div class="col-11">
                                    <select name="file_no_1" class="form-select select2-file">
                                    </select>
                                </div>
                                <div class="col-1">
                                    <button class="btn btn-sm btn-outline-danger" type="button" id="relatedFile-clear">X</button>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 p-3" style="border: 3px solid #CED4DA">
                            <h5>Multiple Matters</h5>
                            <div class="mb-3">
                                <input name="files" type="text" class="form-control" placeholder="1000-3183|1000-3190,1000-4661,6000-0368"/>
                                <div id="emailHelp" class="form-text">Range, simple matter (e.g 1000-3183|1000-3190,1000-4661,6000-0368)</div>
                            </div>
                        </div>
                        <div class="mb-3 p-3" style="border: 3px solid #CED4DA">
                            <h5>Other Filters</h5>
                            <div class="row">
                                <div class="col">
                                    <select name="field" class="form-select" id="FieldOptions">
                                        <option value="0">None</option>
                                        @foreach ($fieldOptions as $k => $v)
                                            <option value="{{ $k }}">{{ $v }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col" id="show">
                                    <div class="row" id="idvalue_C0" style="display: none;">
                                        <div class="col-11">
                                            <select name="value_C0" class="form-select select2-customer">
                                            </select>
                                        </div>
                                        <div class="col-1">
                                            <button class="btn btn-sm btn-outline-danger" type="button" id="c0-clear">X</button>
                                        </div>
                                    </div>
                                    <div class="row" id="idvalue_MatterCode" style="display: none;">
                                        <div class="col-11">
                                            <select name="value_MatterCode" class="form-select select2-matter">
                                            </select>
                                        </div>
                                        <div class="col-1">
                                            <button class="btn btn-sm btn-outline-danger" type="button" id="matterCode-clear">X</button>
                                        </div>
                                    </div>
                                    <div class="row" id="idvalue_PChecklist" style="display: none;">
                                        <div class="col-11">
                                            <select name="value_PChecklist" class="form-select select2-pchecklist">
                                            </select>
                                        </div>
                                        <div class="col-1">
                                            <button class="btn btn-sm btn-outline-danger" type="button" id="pChecklist-clear">X</button>
                                        </div>
                                    </div>
                                    <select name="value_FileStatus" class="form-select" id="idvalue_FileStatus" style="display: none;">
                                        @foreach ($fileStatus as $k => $v)
                                            <option value="{{ $k }}">{{ $v }}</option>
                                        @endforeach
                                    </select>
                                    <div class="row" id="idvalue_DateOpenFile" style="display: none;">
                                        <div class="col">
                                            <input name="value_StartDate" placeholder="2021-12-01" type="text" id="datepicker1" class="form-control"/>
                                        </div>
                                        <div class="col">
                                            <input name="value_EndDate" placeholder="2021-12-30" type="text" id="datepicker2" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="row" id="idvalue_Pn15" style="display: none;">
                                        <div class="col-11">
                                            <select name="value_Pn15" class="form-select select2-pn15">
                                            </select>
                                        </div>
                                        <div class="col-1">
                                            <button class="btn btn-sm btn-outline-danger" type="button" id="pn15-clear">X</button>
                                        </div>
                                    </div>
                                    <div class="row" id="idvalue_project" style="display: none;">
                                        <div class="col-11">
                                            <select name="value_project" class="form-select select2-project">
                                            </select>
                                        </div>
                                        <div class="col-1">
                                            <button class="btn btn-sm btn-outline-danger" type="button" id="project-clear">X</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <button type="submit" class="btn btn-primary">Export to Excel</button>
                        {{-- <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                            Bulk Mail Merge
                        </button> --}}
                        <a href="{{ route('mail-merge.template.generate') }}" id="generate-mail-merge" class="btn btn-primary">
                            Bulk Mail Merge
                        </a>
                        <br/>
                        <small>Maximum limit 3000 records</small>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" /> --}}
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
<style>
.select2 {
    width: 100% !important;
}
</style>
@endsection

@section('script')
<script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
{{-- <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script> --}}
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>
<script src="{{ asset('js/jquery.mask.min.js') }}"></script>
<script>
    function generate(templateId) {
        $('#field-mailMerge').val(1);
        $('#field-templateId').val(templateId);
        $('form#exportForm').submit();
    }
    $(document).ready(function() {
        $("#generate-mail-merge").click(function(e){
            e.preventDefault();
            $('#exportForm').attr('action', '{{ route("mail-merge.template.generate") }}').submit();
        });
        $('#FieldOptions').on('change', function() {
            switch (this.value) {
                case 'MatterCode':
                    $('#idvalue_MatterCode').show();
                    $('#idvalue_PChecklist').hide();
                    $('#idvalue_FileStatus').hide();
                    $('#idvalue_DateOpenFile').hide();
                    $('#idvalue_C0').hide();
                    $('#idvalue_Pn15').hide();
                    $('#idvalue_project').hide();
                    break;
                case 'PChecklist':
                    $('#idvalue_MatterCode').hide();
                    $('#idvalue_PChecklist').show();
                    $('#idvalue_FileStatus').hide();
                    $('#idvalue_DateOpenFile').hide();
                    $('#idvalue_C0').hide();
                    $('#idvalue_Pn15').hide();
                    $('#idvalue_project').hide();
                    break;
                case 'FileStatus':
                    $('#idvalue_MatterCode').hide();
                    $('#idvalue_PChecklist').hide();
                    $('#idvalue_FileStatus').show();
                    $('#idvalue_DateOpenFile').hide();
                    $('#idvalue_C0').hide();
                    $('#idvalue_Pn15').hide();
                    $('#idvalue_project').hide();
                    break;
                case 'DateOpenFile':
                    $('#idvalue_MatterCode').hide();
                    $('#idvalue_PChecklist').hide();
                    $('#idvalue_FileStatus').hide();
                    $('#idvalue_DateOpenFile').show();
                    $('#idvalue_C0').hide();
                    $('#idvalue_Pn15').hide();
                    $('#idvalue_project').hide();
                    break;
                case 'C0':
                    $('#idvalue_MatterCode').hide();
                    $('#idvalue_PChecklist').hide();
                    $('#idvalue_FileStatus').hide();
                    $('#idvalue_DateOpenFile').hide();
                    $('#idvalue_C0').show();
                    $('#idvalue_Pn15').hide();
                    $('#idvalue_project').hide();
                    break;
                case 'Pn15':
                    $('#idvalue_MatterCode').hide();
                    $('#idvalue_PChecklist').hide();
                    $('#idvalue_FileStatus').hide();
                    $('#idvalue_DateOpenFile').hide();
                    $('#idvalue_C0').hide();
                    $('#idvalue_Pn15').show();
                    $('#idvalue_project').hide();
                    break;
                case 'project':
                    $('#idvalue_MatterCode').hide();
                    $('#idvalue_PChecklist').hide();
                    $('#idvalue_FileStatus').hide();
                    $('#idvalue_DateOpenFile').hide();
                    $('#idvalue_C0').hide();
                    $('#idvalue_Pn15').hide();
                    $('#idvalue_project').show();
                    break;
                default:
                    $('#idvalue_MatterCode').hide();
                    $('#idvalue_PChecklist').hide();
                    $('#idvalue_FileStatus').hide();
                    $('#idvalue_DateOpenFile').hide();
                    $('#idvalue_C0').hide();
                    $('#idvalue_Pn15').hide();
                    $('#idvalue_project').hide();
                    break;
            }
        });

        $('#datepicker1').datepicker({ format: 'yyyy-dd-mm' });
        $('#datepicker2').datepicker({ format: 'yyyy-dd-mm' });
        $('.select2').select2();
        $('.select2-customer').select2({
            minimumInputLength: 3,
            ajax: {
                delay: 250, // wait 250 milliseconds before triggering the request
                url: "{{ route('api.customer.search') }}",
                dataType: 'json',
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }

                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    var result = data.results;
                    var more = false;
                    if (result.next_page_url != null) {
                        more = true;
                    }

                    return {
                        results: result.data,
                        pagination: {
                            more: more
                        }
                    };
                }
            },
            escapeMarkup: function(markup) {
                return markup;
            }
        });
        $('.select2-pn15').select2({
            minimumInputLength: 1,
            ajax: {
                delay: 250, // wait 250 milliseconds before triggering the request
                url: "{{ route('api.pn15.search') }}",
                dataType: 'json',
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }

                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    var result = data.results;
                    var more = false;
                    if (result.next_page_url != null) {
                        more = true;
                    }

                    return {
                        results: result.data,
                        pagination: {
                            more: more
                        }
                    };
                }
            },
            escapeMarkup: function(markup) {
                return markup;
            }
        });
        $('#c0-clear').click(function() {
            $(".select2-customer").val(null).trigger("change");
        });
        $('#pn15-clear').click(function() {
            $(".select2-pn15").val(null).trigger("change");
        });
        $('.select2-pchecklist').select2({
            minimumInputLength: 3,
            ajax: {
                delay: 250, // wait 250 milliseconds before triggering the request
                url: "{{ route('api.pchecklist.search') }}",
                dataType: 'json',
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }

                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    var result = data.results;
                    var more = false;
                    if (result.next_page_url != null) {
                        more = true;
                    }

                    return {
                        results: result.data,
                        pagination: {
                            more: more
                        }
                    };
                }
            },
            escapeMarkup: function(markup) {
                return markup;
            }
        });
        $('#pChecklist-clear').click(function() {
            $(".select2-pchecklist").val(null).trigger("change");
        });
        $('.select2-matter').select2({
            minimumInputLength: 3,
            ajax: {
                delay: 250, // wait 250 milliseconds before triggering the request
                url: "{{ route('api.matter.search') }}",
                dataType: 'json',
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }

                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    var result = data.results;
                    var more = false;
                    if (result.next_page_url != null) {
                        more = true;
                    }

                    return {
                        results: result.data,
                        pagination: {
                            more: more
                        }
                    };
                }
            },
            escapeMarkup: function(markup) {
                return markup;
            }
        });
        $('#matterCode-clear').click(function() {
            $(".select2-matter").val(null).trigger("change");
        });
        $('.select2-file').select2({
            minimumInputLength: 3,
            ajax: {
                delay: 250, // wait 250 milliseconds before triggering the request
                url: "{{ route('api.file.search') }}",
                dataType: 'json',
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }

                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    var result = data.results;
                    var more = false;
                    if (result.next_page_url != null) {
                        more = true;
                    }

                    return {
                        results: result.data,
                        pagination: {
                            more: more
                        }
                    };
                }
            },
            escapeMarkup: function(markup) {
                return markup;
            }
        });
        $('#template-clear').click(function() {
            $(".select2-template").val(null).trigger("change");
        });
        $('.select2-template').select2({
            minimumInputLength: 3,
            ajax: {
                delay: 250, // wait 250 milliseconds before triggering the request
                url: "{{ route('api.mail-merge.search') }}",
                dataType: 'json',
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }

                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    var result = data.results;
                    var more = false;
                    if (result.next_page_url != null) {
                        more = true;
                    }

                    return {
                        results: result.data,
                        pagination: {
                            more: more
                        }
                    };
                }
            },
            escapeMarkup: function(markup) {
                return markup;
            }
        });
        $('#relatedFile-clear').click(function() {
            $(".select2-file").val(null).trigger("change");
        });
        $('.select2-project').select2({
            minimumInputLength: 3,
            ajax: {
                delay: 250, // wait 250 milliseconds before triggering the request
                url: "{{ route('api.project.search') }}",
                dataType: 'json',
                data: function (params) {
                    var query = {
                        term: params.term,
                        page: params.page || 1
                    }

                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    var result = data.results;
                    var more = false;
                    if (result.next_page_url != null) {
                        more = true;
                    }

                    return {
                        results: result.data,
                        pagination: {
                            more: more
                        }
                    };
                }
            },
            escapeMarkup: function(markup) {
                return markup;
            }
        });
        $('#project-clear').click(function() {
            $(".select2-project").val(null).trigger("change");
        });
    });
</script>
@endsection
