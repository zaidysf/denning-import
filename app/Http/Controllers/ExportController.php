<?php

namespace App\Http\Controllers;

use App\Exports\FileExport;
use App\Models\File;
use App\Models\Filestatus;
use App\Models\MailMerge;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ExportController extends Controller
{

    public function __construct()
    {
    }

    public function index()
    {
        $fileStatus = Filestatus::pluck('status', 'statusCode');
        $fieldOptions = [
            'C0' => 'Primary Client',
            'MatterCode' => 'Matter Code',
            'PChecklist' => 'Preset Checklist',
            'DateOpenFile' => 'Opened Date',
            'FileStatus' => 'Matter Status',
            'Pn15' => 'Batch No',
            'Project' => 'Project',
            'AllMatters' => 'All Matters',
        ];
        $templates = MailMerge::all();
        return view('export.index', compact('fileStatus', 'fieldOptions', 'templates'));
    }

    public function export(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'start_from' => 'required_without_all:file_no_1,files,field',
            'sum' => 'required_with:start_from',
            'file_no_1' => 'required_without_all:start_from,files,field',
            'files' => 'required_without_all:start_from,file_no_1,field',
            'field' => 'required_without_all:start_from,file_no_1,files',
            'value_MatterCode' => 'required_if:field,MatterCode',
            'value_PChecklist' => 'required_if:field,PChecklist',
            'value_StartDate' => 'required_if:field,DateOpenFile',
            'value_EndDate' => 'required_if:field,DateOpenFile',
            'value_FileStatus' => 'required_if:field,FileStatus',
            'value_Pn15' => 'required_if:field,Pn15',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $mailMerge = 0;
        if ($request->has('mailMerge') && $request->has('templateId')) {
            $mailMerge = $request->get('mailMerge');
            $templateId = $request->get('templateId');
        }

        $filter = [];
        if ($request->filled('start_from')) {
            $filter['file_range']['start_from'] = $request->get('start_from');
            $filter['file_range']['sum'] = $request->get('sum');
        } elseif ($request->filled('file_no_1')) {
            $filter['file_no_1'] = $request->get('file_no_1');
        } elseif ($request->filled('files')) {
            $filter['files'] = $request->get('files');
        } elseif ($request->filled('field')) {
            $filter['field'] = $request->get('field');
            if ($request->get('field') != 'DateOpenFile') {
                $filter['value'] = $request->get('value_'.$request->get('field'));
            } else {
                $filter['value']['start'] = $request->get('value_StartDate');
                $filter['value']['end'] = $request->get('value_EndDate');
            }
        }

        $finalData = $this->filter($filter);

        if ($mailMerge == 1) {
            return (new MailMergeController)->generateAll($templateId, $finalData);
        } else {
            return (new FileExport($finalData))->download('export.xlsx');
        }
    }

    public function filter($filter)
    {
        $maxrows = 3000;

        $data = File::selectRaw("*, cast(replace(FileNo1, '-', '') as UNSIGNED) as FileNo1Text")
        ->orderBy('FileNo1', 'asc');
        if (isset($filter['file_range'])) {
            $data->where('FileNo1', '>=', $filter['file_range']['start_from']);
            if ($filter['file_range']['sum'] > $maxrows) {
                $data->take($maxrows);
            } else {
                $data->take($filter['file_range']['sum']);
            }
        } elseif (isset($filter['file_no_1'])) {
            $data->where('FileNo1', $filter['file_no_1']);
        } elseif (isset($filter['files'])) {
            $files = explode(',', $filter['files']);
            foreach ($files as $v) {
                $fileRange = explode('|', $v);
                if (isset($fileRange[1])) {
                    $file0 = str_replace('-', '', $fileRange[0]);
                    $file1 = str_replace('-', '', $fileRange[1]);
                    $data->orWhereRaw("
                        (cast(replace(FileNo1, '-', '') as UNSIGNED) >= ".$file0." and
                        cast(replace(FileNo1, '-', '') as UNSIGNED) <= ".$file1.")");
                } else {
                    $data->orWhere('FileNo1', $fileRange[0]);
                }
            }
        } elseif (isset($filter['field'])) {
            if ($filter['field'] != 'DateOpenFile') {
                $data->where($filter['field'], $filter['value']);
            } else {
                $data->whereBetween(
                    $filter['field'],
                    [$filter['value']['start'], $filter['value']['end']]
                );
            }
        }

        if (!isset($filter['file_range'])) {
            $data->take($maxrows);
        }

        return $data->get();
    }
}
