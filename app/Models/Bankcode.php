<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Bankcode
 *
 * @property string $BankCode
 * @property string|null $BankName
 * @property int|null $IncorpType
 * @property string|null $CompanyNo
 * @property string|null $RegAdd
 * @property string|null $Phoneoffice
 * @property string|null $Fax
 * @property string|null $Email
 * @property string|null $Website
 * @property string|null $Note1
 * @property string|null $Note2
 * @property string|null $LegalDept
 * @property string|null $LegalDeptHead
 * @property string|null $LegalDeptPhone
 * @property string|null $LegalDeptFax
 * @property Carbon|null $dateEntered
 * @property int|null $enteredBy
 * @property Carbon|null $dateUpdated
 * @property int|null $updatedBy
 * @property string|null $remarks
 * @property int|null $IdTypeBankCode
 * @property string|null $IdType
 * @property string|null $PreviousName1
 * @property string|null $PreviousName2
 * @property string|null $Remark
 * @property string|null $Phone1Ctr
 * @property string|null $Phone1No
 * @property string|null $Phone2Ctr
 * @property string|null $Phone2No
 * @property string|null $Fax1Ctr
 * @property string|null $Fax1No
 * @property string|null $Fax2Ctr
 * @property string|null $Fax2No
 *
 * @package App\Models
 */
class Bankcode extends Model
{
	protected $table = 'bankcode';
	protected $primaryKey = 'BankCode';
	public $incrementing = false;
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'IncorpType' => 'int',
		'enteredBy' => 'int',
		'updatedBy' => 'int',
		'IdTypeBankCode' => 'int'
	];

	protected $dates = [
		'dateEntered',
		'dateUpdated'
	];

	protected $fillable = [
		'BankName',
		'IncorpType',
		'CompanyNo',
		'RegAdd',
		'Phoneoffice',
		'Fax',
		'Email',
		'Website',
		'Note1',
		'Note2',
		'LegalDept',
		'LegalDeptHead',
		'LegalDeptPhone',
		'LegalDeptFax',
		'dateEntered',
		'enteredBy',
		'dateUpdated',
		'updatedBy',
		'remarks',
		'IdTypeBankCode',
		'IdType',
		'PreviousName1',
		'PreviousName2',
		'Remark',
		'Phone1Ctr',
		'Phone1No',
		'Phone2Ctr',
		'Phone2No',
		'Fax1Ctr',
		'Fax1No',
		'Fax2Ctr',
		'Fax2No'
	];

    public function getNameAttribute() {
        return ucwords($this->BankCode . ' - ' . $this->BankName);
    }
}
