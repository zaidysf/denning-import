@extends('layout')
@section('content')
    <div class="mb-3">
        <h1 class="h3 d-inline align-middle">Import Batch List</h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Batch ID</th>
                                <th scope="col">Primary Client</th>
                                <th scope="col">Matter Description</th>
                                <th scope="col">Count</th>
                                <th scope="col">Status</th>
                                <th scope="col">Staff</th>
                                <th scope="col">Date Created</th>
                                <th scope="col">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $v)
                                <tr>
                                    <th scope="row">{{ $v->BatchNo }}</th>
                                    <td>{{ $v->customer->name }}</td>
                                    <td>{{ $v->matter }}</td>
                                    <td>{{ $v->count }}</td>
                                    <td>{{ $v->status_name }}</td>
                                    <td>{{ $v->staff->name }}</td>
                                    <td>{{ date('Y-m-d', strtotime($v->created_at)) }}</td>
                                    <td>
                                        <a
                                            target="_blank"
                                            href="/file?key=BatchNo&value={{ $v->BatchNo }}"
                                            class="btn btn-info btn-sm"
                                        >
                                            Matters
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
